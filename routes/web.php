<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Frontend ///////////////////////////////////////

// Home page
Route::get('/', 'HomePageController@index')->name('home');
//get search data by lacation using ajax
Route::get('/searchbyloc/{id}', 'HomePageController@searchByLoc');
//get search data by category using ajax
Route::get('/searchbycat/{id}', 'HomePageController@searchByCat');

//project listing page
Route::get('/projects/', 'ProjectsController@index')->name('index');
Route::get('/location/{location}/', 'ProjectsController@get_pr_by_loc')->name('index');

// static pages
Route::get('/{url}/', 'PagesController@index')->name('index');
//get search box autocomplete projects and buiders on home pagr
Route::get('autocomplete/{query}','ProjectsController@autocomplete');
//for sent query fron contact us page
Route::post('/contact-us/', 'UserQueriesController@store');

//for sent query fron contact us page
//Route::get('/builder/{name}/', 'BuildersController@index');
//for sent query fron contact us page
//Route::get('/project/{name}/', 'ProjectsController@projectsDetails');
Route::get('/blogs/{id}/', 'BlogsController@index');
Route::get('/search/searchProjects/', 'ProjectsController@searchData');
// Backend ///////////////////////////////////////

Route::get('/admin-panel/admin/', 'Backend\UserController@login')->name('backendLogin');
Route::post('/admin-panel/admin', 'Backend\UserController@doLogin');
Route::get('/admin-panel/logout', 'Backend\UserController@logout');

Route::middleware('backendCheck')->prefix('backend')->group(function(){

Route::get('/index', 'Backend\UserController@index')->name('backendIndex');

	
	//  pages//
 Route::get('/pages', 'Backend\PagesController@index');
 Route::get('/add-page', 'Backend\PagesController@add');
Route::post('/add-page', 'Backend\PagesController@store');
Route::get('/add-home-page', 'Backend\PagesController@addHome');
Route::post('/add-home-page', 'Backend\PagesController@storeHome');
Route::post('/edit-home-page/{id}', 'Backend\PagesController@updateHome');
Route::get('/edit-home-page/{id}', 'Backend\PagesController@editHome');

Route::post('/update-page/{id}', 'Backend\PagesController@update');
Route::get('/update-page/{id}', 'Backend\PagesController@edit');
Route::get('/pages/{id}','Backend\PagesController@updateStatus');


    
   //builder
Route::get('/builders','Backend\BuildersController@index');
Route::get('/add-builder','Backend\BuildersController@add');
Route::get('/builders/{id}','Backend\BuildersController@updateBuilderStatus');
Route::post('/add-builder','Backend\BuildersController@store');
Route::post('/edit-builder/{id}','Backend\BuildersController@store');

Route::get('/builders/{id}','Backend\BuildersController@updateBuilderStatus');
Route::get('/update-builder/{id}', 'Backend\BuildersController@edit');
Route::post('/update-builder/{id}', 'Backend\BuildersController@update');


//category
Route::get('/categories','Backend\CategoriesController@index');
Route::get('/categories/{id}','Backend\CategoriesController@updateStatus');
Route::get('/add-category','Backend\CategoriesController@add');
Route::post('/add-category','Backend\CategoriesController@store');
Route::get('/update-category/{id}','Backend\CategoriesController@edit');
Route::post('/update-category/{id}','Backend\CategoriesController@update');
   
   //Location
 Route::get('/locations','Backend\LocationsController@index');
Route::get('/locations/{id}','Backend\LocationsController@updateStatus');
  Route::get('/add-locations','Backend\LocationsController@add');
   Route::post('/add-locations','Backend\LocationsController@store');
Route::get('/update-location/{id}','Backend\LocationsController@edit');
Route::post('/update-location/{id}','Backend\LocationsController@update');

    //projects
  Route::get('/projects','Backend\ProjectsController@index');
  Route::get('/projects/bid={bid}','Backend\ProjectsController@builderProjects');
  
   Route::get('/add-project-detail/{projectid}','Backend\PropertiesController@addDetails');
   Route::post('/add-project-detail/{projectid}','Backend\PropertiesController@storeDetail');
  
   Route::get('/add-projects','Backend\ProjectsController@add');
    Route::get('/add-projects/{b_id}','Backend\ProjectsController@add');
    Route::post('/add-projects/{b_id}','Backend\ProjectsController@storeBuilderProject');
   Route::post('/add-projects','Backend\ProjectsController@store');
   Route::get('/update-projects/{id}','Backend\ProjectsController@edit');
  Route::get('/projects/{id}','Backend\ProjectsController@updateProjectStatus');
 
   Route::post('/update-projects/{id}','Backend\ProjectsController@update');
  //amenities
      Route::get('/amenities','Backend\AmenitiesController@index');
Route::get('/amenities/{id}','Backend\AmenitiesController@updateStatus');
Route::get('/update-amenity/{id}','Backend\AmenitiesController@add');
Route::get('/update-amenity/{id}','Backend\AmenitiesController@edit');
Route::post('/update-amenity/{id}','Backend\AmenitiesController@update');
  Route::get('/add-amenity','Backend\AmenitiesController@add');
 Route::post('/add-amenity','Backend\AmenitiesController@store');



  Route::get('/add-amenity','Backend\AmenitiesController@add');
 Route::post('/add-amenity','Backend\AmenitiesController@store');
//property type
  Route::get('/prop-type','Backend\PropertyTypesController@index');
Route::get('/prop-type/{id}','Backend\PropertyTypesController@updateStatus');
Route::get('/add-prop-type','Backend\PropertyTypesController@add');
Route::post('/add-prop-type','Backend\PropertyTypesController@store');
Route::get('/update-prop-type/{id}','Backend\PropertyTypesController@edit');
Route::post('/update-prop-type/{id}','Backend\PropertyTypesController@update');
 Route::get('/update-property/{id}','Backend\PropertiesController@edit');
 Route::post('/update-property/{id}','Backend\PropertiesController@update');

//Property
  Route::get('/properties','Backend\PropertiesController@index');
 Route::get('/add-property','Backend\PropertiesController@add');
 Route::post('/add-property','Backend\PropertiesController@store');
 Route::get('/properties/{id}','Backend\PropertiesController@updateStatus');

//Property Prices
 
Route::get('/add-prop-price/{id}/','Backend\PropertyPricesController@index');
Route::post('/add-prop-price/{id}/','Backend\PropertyPricesController@store');
Route::get('/update-price/{id}/','Backend\PropertyPricesController@edit');
Route::post('/update-price/{id}/','Backend\PropertyPricesController@update');

 Route::get('/add-property','Backend\PropertiesController@add');

  Route::get('/ajaxLocBuilder/{id}','Backend\PropertiesController@ajaxLocBuilder');
  Route::get('/ajaxBuilder/{id}','Backend\ProjectsController@ajaxBuilder');
  Route::get('/ajaxProject/{id}','Backend\PropertiesController@ajaxProject');

 Route::get('/ajaxBuilderProject/{id}','Backend\PropertiesController@ajaxBuilderProject');
 Route::get('/deletePropImg/{id}/{propId}','Backend\PropertiesController@deletePropImg');
Route::get('/deletePropf_Img/{id}/{propId}','Backend\PropertiesController@deletePropf_img');

//////Blogs/////
Route::get('/blog-cat','Backend\BlogCategoriesController@index');
Route::get('/add-blog-cat','Backend\BlogCategoriesController@add');
Route::post('/add-blog-cat','Backend\BlogCategoriesController@store');
Route::post('/add-blogs','Backend\BlogsController@store');

Route::get('/add-blogs','Backend\BlogsController@add');
Route::get('/blogs','Backend\BlogsController@index');





////////////////USER PROFILE////////////////
Route::get('/edit-profile/{id}','Backend\UserController@edit');
Route::post('/edit-profile/{id}','Backend\UserController@update');
Route::get('/change-password/{id}','Backend\UserController@editPassword');
Route::post('/change-password/{id}','Backend\UserController@updatePassword');

//////////////////////USER QUERIES LISTING/////////////////
Route::get('/user-queries','Backend\UserQueriesController@showQueries');
Route::get('/delete-queries','Backend\UserQueriesController@deleteQueries');
Route::get('/update-status','Backend\UserQueriesController@updateQueryStatus'); 
Route::get('/download-queries','Backend\UserQueriesController@downloadXl'); 

});  







