<?php

namespace App\Http\Controllers;

use App\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
//use Corcel\Model\Post;

class BuildersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public static function index($keyword)
    {
        
       $name = str_replace("-", " ", $keyword);

        $builder = DB::table('builders')->where('url' ,'=' , $keyword)->first();
        //echo $builder->location_id;die;
        $builderLoc = explode(",", $builder->location_id);
        $builder_location =array();
        if(!empty($builderLoc)){
            foreach($builderLoc as $key=>$val){
                $builder_location[] = DB::table('locations')->where('id' ,'=' , $val )->first();
            }
        }

       // print_r($builder_location);die;
        
        $address=DB::table('users')
                 ->select('phone','address','office_address','email')->first();   
          
        $title = $builder->meta_tag;
        $locData = DB::table('locations')->select('location','id')->get();
        $pageData=DB::table('builders')
                    ->select('og_title','og_image','og_type','og_site_name','og_url','meta_desc')
                    ->where('url','=',$keyword)
                    ->first();    
            $locality = DB::table('localities')->select('locality','id')->get(); 
           
            $locId ='';
            $meta_desc =$builder->meta_desc;
            $selected_tab  = 'projects' ;
            $footerPost=array();
           // $footerPost = $posts = Post::type('post')->newest()->paginate(4);

        return view('builders',compact('name','builder','address','title','locData','locId','locality','meta_desc','selected_tab','builder_location','footerPost','pageData'));
    }


  public static function getProject($builder_id,$loc_id){
    $projectsList=DB::table('projects')
                ->orderBy('created_at', 'Dsc')
                ->where('is_active','=','yes')
                ->where('builder_id','=',$builder_id)
                ->where('location_id','=',$loc_id)
            ->get();
            return $projectsList;

  }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
