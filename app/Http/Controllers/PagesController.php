<?php

namespace App\Http\Controllers;

use App\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
//use Corcel\Model\Post;
use App\Http\Controllers\BuildersController;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 public function index($url)
    {
      
         $footerPost =  array();
         //Post::type('post')->published()->newest()->paginate(4);
     $address=DB::table('users')
                 ->select('phone','address','office_address','email')->first(); 
        $pageExist=DB::table('pages')
                    ->where('url','=',$url)
                    ->first();
       if(!empty($pageExist)){
            $pageData=DB::table('pages')
                    ->where('url','=',$url)
                    ->first();
            $title=$pageData->title;
            $meta_desc=$pageData->meta_desc;
            $selected_tab = $url;
              
             if($url =='contact-us'){
              return view('contact-us',compact('pageData','address','title','meta_desc','selected_tab','footerPost'));
             }

             else{

                return view('pages',compact('pageData','address','title','meta_desc','selected_tab','footerPost'));
                
             }
            
     }
        else{
             /////////////////get project by location redirect on project///////////////
                $location = str_replace('-',' ', $url);   
                $locExist =   DB::table('locations')->where('location','=',$location)->first();
                $projectExist =   DB::table('projects')->where('url','=',$url)->first();
                $builderExist =   DB::table('builders')->where('url','=',$url)->first();
                
                if(!empty($locExist)){
                    return ProjectsController::get_pr_by_loc($url);
                }if(!empty($builderExist)){
                    ///////////// redirect on project detail page//////////////
                    return BuildersController::index($url);
                }if(!empty($projectExist)){
                    ///////////// redirect on project detail page//////////////
                    return ProjectsController::projectsDetails($url);
                }else{
                   
                    return view('layouts.page-not-found');
                }
         }

    }

  
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
