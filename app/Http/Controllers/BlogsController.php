<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //echo "dsd"; die;
        $post = DB::table('blogs')->where('id','=',$id)->first();   
        $footerPost = array();
         //$footerPost = $posts = Post::type('post')->newest()->paginate(4);

        //print_r()
            $address=DB::table('users')
                 ->select('phone','address','office_address','email')->first();   
            $selected_tab = 'blogs';
            $title="Blogs- DSC";
            $pageData = array();
            $meta_desc ='';
         return view('blog-detail',compact('post','selected_tab','address','meta_desc','title','meta_desc','footerPost'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $builders = Builder::create(['name' => $request->newBuilder, 'status' => 'Inactive']);
        return response()->json($builders);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
