<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\PropertyPrice;

class PropertyPricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
    	$sidebarTab = 'Properties';
        $project = DB::table('projects')->where('id', '=',$project_id)
                        ->select('name','id')
                        ->orderBy('name', 'Asc')
                        ->first();

          
        return view('backend.addPropPrice', compact('sidebarTab','project'));
    }


 public function add()
    {
        $sidebarTab = 'Properties';

        return view('backend.addtype', compact('sidebarTab'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //store property price list
  public function store(Request $request)
  { 
        //  echo "test";die;
    	$sidebarTab = 'Properties';
        // print_r($request);die;
       //save property price list array
        $checkExist = DB::table('property_prices')
                            ->where('project_id','=',$request['project_id']);
              if(!empty($checkExist)){
               
                  DB::table('property_prices')->where('project_id','=',$request['project_id'])->delete();

              }
        if($request->hasFile('price_image')){
            $extension=$request->file('price_image')->getClientOriginalExtension();
              $price_image=$request['project_name'].'_price_plan.'.$extension;

               $price_image_path=public_path('upload\project_image\price_plan\\'.$price_image);
                if(is_file($price_image_path)) {
                   unlink($price_image_path);
                }
              $request->file('price_image')->move(public_path('upload/project_image/price_plan'),$price_image);

              $propertyprice = new PropertyPrice;
              $propertyprice->project_id = $request['project_id'];
              $propertyprice->price_plan = $price_image;
              $propertyprice->save();
        }else{
        //for multiple record entry
          $propertypricearray =array();
             for($i=0;$i<count($request['unit_size']);$i++){
                    $propertyprice = new PropertyPrice;
                    $propertyprice->project_id = $request['project_id'];
                    $propertyprice->accomodation_type = $request['accomadation_type'][$i];
                    $propertyprice->unit_size   = $request['unit_size'][$i];
                    $propertyprice->basic_price = $request['basic_price'][$i];
                    $propertyprice->total_price = $request['total_price'][$i];

                    $propertypricearray[] = $propertyprice->attributesToArray();
              }
              ///fetch list
            foreach ($propertypricearray as $list) {
              PropertyPrice::insert($list); 
            }
        }
      return redirect('backend/projects')->with('info','Project price details added Successfully.');
}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($pid)
    {
         $sidebarTab = 'Properties';
        $propPrice = DB::table('property_prices')->where('property_prices.project_id','=',$pid)
                      ->leftjoin('projects', 'property_prices.project_id', '=', 'projects.id')
                      ->select('property_prices.price_plan','property_prices.accomodation_type','property_prices.unit_size','property_prices.basic_price','property_prices.total_price','projects.name as project_name','projects.id as project_id')
                      ->get();
        return view('backend.update-price', compact('sidebarTab', 'propPrice'));
    }


public function update(Request $request,$id)
    {
        $sidebarTab = 'Projects';
        $message='fail';
        $request->validate([
            'prop-type' => 'required',
           
        ]);
     $propertycheck = PropertyType::where('name', $request['prop-type'])->where('id','!=',$id)->first();
        if ($propertycheck === null) {
         // category doesn't exist
          $proptype =PropertyType::find($id);
        $proptype->name = $request['prop-type'];
        $proptype->update();
        return redirect('backend/prop-type')->with('info','success');
    }
    return redirect('backend/prop-type')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 PropertyType::where('id',$id)->update($data);  

}
}