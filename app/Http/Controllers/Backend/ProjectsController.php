<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Project;
use File;
use App\Builder;
use Image;
use ImageOptimizer;
class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sidebarTab='Projects';
       

         $projectList =DB::table('projects')
            ->leftjoin('builders', 'projects.builder_id', '=', 'builders.id')
            ->join('locations', 'projects.location_id', '=', 'locations.id')
            
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.id', 'builders.name AS builder_name','locations.location','projects.location_id')
            ->get();
         
        return view('backend.projects', compact('sidebarTab','projectList'));
    }

    public static function getProPrice($pId)
    {
       $sidebarTab='Projects';
       

         $pricelist =DB::table('property_prices')
            ->where('project_id', '=', $pId)
            ->first();
        $count = count($pricelist);
        //dd($projectList);     
        return $count;
    }

    public static function getPropId($pId)
    {
       $sidebarTab='Projects';
       

         $prop_id =DB::table('properties')
            ->where('project_id', '=', $pId)
            ->first();
        if(!empty($prop_id)){
         $property_id = $prop_id->id; 
        }else{
            $property_id = '';
        }
        return $property_id;
    }

 public function builderProjects($id)
    {
       $sidebarTab='Projects';

        $builderName=Builder::find($id)->name;
         $projectList =DB::table('projects')
            ->leftjoin('builders', 'projects.builder_id', '=', 'builders.id')
            ->join('locations', 'projects.location_id', '=', 'locations.id')
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.id', 'builders.name AS builder_name','locations.location','projects.location_id')
            ->where('builder_id','=',$id)
            ->get();
        //dd($projectList);     
        return view('backend.projects', compact('sidebarTab','projectList','builderName'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add($bid=null)
    {
        $sidebarTab = 'Projects';
        $builderInfo = '';
        $lid='';
        if($bid != '')
        {
            $builderInfo = DB::table('builders')
            ->select('name','id','location_id')
            ->where('id', '=',$bid)
            ->orderBy('name', 'Asc')
            ->get();
         // print_r($builderInfo[0]->location_id);die;
           $lid = $builderInfo[0]->location_id;
          $lidArray =  explode(",",$lid);
          $i=0;
            foreach($lidArray as $locId){
            $locations[$i++] = DB::table('locations')
                ->where('id','=',$locId)
                ->orderBy('location', 'Asc')
                ->first();
            }
             //dd($locations);die;
               // DB::table('locations')->toSql();

        }else{

            $locations = DB::table('locations')
                ->where('is_active','=','yes')
                ->orderBy('location', 'Asc')
                ->get();
                 $builderInfo ='';
       
        }
        
        $categories = DB::table('categories')
        ->where('is_active','=','yes')
        ->orderBy('category', 'Asc')
        ->get(); 
        $builders=DB::table('builders')
        ->where('is_verified','=','1')
        ->orderBy('id','Asc')
        ->get();
  
        return view('backend.addprojects', compact('sidebarTab','locations','lid','bid','builderInfo','categories','builders'));
    }




public static function  getProjLoc($locIds){
        $locIdArray=explode(',',$locIds);
            $loc='';
            $prefix = '';  
          if(!empty($locIdArray)){
              foreach ($locIdArray as $v)
               {
                $location=DB::table('locations')->where('id',$v)->first();
                  $loc.= $prefix.$location->location;
                  $prefix = ",";
                }
          }
          else{

                $location=DB::table('locations')->where('id',$projectList->location_id)->first();
           $loc=$location->location;  
        }
            echo $loc;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
public function storeBuilderProject(Request $request)
{
         
        $error='';
        $l_id = $request['l_id'];
        $sidebarTab='Projects';
        $prefix = '';
        $lid='';
         $logo='';
         $project_image='';
         foreach($l_id as $loc => $val){
                    $lid.=  $prefix.$val;
                    $prefix = ',';
         } 
         if($request->hasFile('logo')){
              $extension= $request->file('logo')
                        ->getClientOriginalExtension();
              $logo=$request['name'].'_logo'.'.'.$extension;
              $request->file('logo')->move(public_path('upload/project_logo'),$logo);
            }
            //check project image
           if($request->hasFile('project_image')){
              $extension=$request->file('project_image')->getClientOriginalExtension();
              $project_image=$request['name'].'.'.$extension;
              $destinationPath = public_path('upload/project_image/thumbnail');
              $img = Image::make($request->file('project_image')->getRealPath());
              $img->resize(100, 100, function ($constraint) {
              $constraint->aspectRatio();
                })->save($destinationPath.'/'.$project_image);     
              $request->file('project_image')->move(public_path('upload/project_image'),$project_image);
            }
             //is_featured
             $is_featured="";
                if(isset($request->is_featured)){
                   $is_featured='yes';
                }
                else{
                    $is_featured='no';
                }

                //parking
             $parking="";
                if(isset($request->parking)){
                   $parking='yes';
                }
                else{
                    $parking='no';
                }
                $project = new Project;
                $project->name = $request->name;
                $project->location_id = $lid;
                $project->builder_id  = $request['b_id'];
                $project->category_id = $request['category'];
                $project->logo=$logo;
                $project->project_image=$project_image;
                $project->is_featured=$is_featured;
                $project->url=$request['url'];
                $project->rera_id=$request['rera_id'];
                $project->bed=$request['bed'];
                $project->bath=$request['bath'];
                $project->min_price=$request['min_price'];
                $project->max_price=$request['max_price'];
                $project->size=$request['size'];
                $project->project_type=$request['project_type'];
                $project->parking=$parking;
                $project->description=$request['description'];
                $project->save();
                   return redirect('backend/builders')->with('info','Project added successfully for selected builder');;
     
     // }
 }

    
public function store(Request $request)
{
        $request->validate([
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
        ]);
        $error='';
        $l_id = $request['l_id'];
        $sidebarTab='Projects';
        $prefix = ''; 
        $lid='';
        foreach($l_id as $loc => $val){
                    $lid.=  $prefix.$val;
                    $prefix = ',';
        } 

        $prefix1 = '';
        $pro_type = $request['project_type'];
        $proType='';
        foreach($pro_type as $key => $val2){
                    $proType.=  $prefix.$val2;
                    $prefix1 = ',';
        } 
         $alreadyExist =  DB::table('projects')
                        ->where('name','=', $request->name )
                        ->first();
        if(!empty($alreadyExist)){
         $error ="Project name already exist for selected location";
         $bid = '';
         $locations = DB::table('locations')
                        ->orderBy('location', 'Asc')
                        ->get();
         $builderInfo = '';
         $categories = DB::table('categories')
                        ->orderBy('category', 'Asc')
                        ->get(); 
            return view('backend.addprojects', compact('sidebarTab','locations','lid','bid','builderInfo','categories','error')); 
        }else{
           $logo ='';
           $project_image='';
            //check project logo
           if($request->hasFile('logo')){
              $extension= $request->file('logo')
                        ->getClientOriginalExtension();
              $logo=$request['name'].'_logo'.'.'.$extension;
              $request->file('logo')->move(public_path('upload/project_logo'),$logo);
            }
            //check project image
           if($request->hasFile('project_image')){
              $extension=$request->file('project_image')->getClientOriginalExtension();
              $project_image=$request['name'].'.'.$extension;
              $destinationPath = public_path('upload/project_image/thumbnail');
              $img = Image::make($request->file('project_image')->getRealPath());
              $img->resize(100, 100, function ($constraint) {
              $constraint->aspectRatio();
                })->save($destinationPath.'/'.$project_image);     
              $request->file('project_image')->move(public_path('upload/project_image'),$project_image);
            }
             //is_featured
             $is_featured="";
                if(isset($request->is_featured)){
                   $is_featured='yes';
                }
                else{
                    $is_featured='no';
                }
               //parking
            
            $project = new Project;
            $project->name = $request->name;
            $project->location_id = $lid;
            $project->builder_id  = $request['b_id'];      
            $project->category_id = $request['category'];
            $project->description=$request['description'];
            $project->logo=$logo;
            $project->project_image=$project_image;
            $project->is_featured=$is_featured;
            $project->url=$request['url'];
            $project->bed=$request['bed'];
            $project->bath=$request['bath'];
            $project->min_price=$request['min_price'];
            $project->max_price=$request['max_price'];
            $project->size=$request['size'];
            $project->project_feature=$request['project_feature'];
            $project->project_type=$proType;
            $project->parking=$request['parking'];
            $project->phase=$request['phase'];
            $project->locality=$request['locality'];
            $project->rera_id=$request['rera_id'];
            $project->save();
         //insert locality
               $chkLocExist =  DB::table('localities')
                        ->where('locality','=', $request['locality'] )
                        ->where('loc_id','=', $lid)
                        ->first();//print_r($chkLocExist->id);die;
                if(empty($chkLocExist)){
                       $lid = DB::table('localities')
                       ->insertGetId([
                       'locality' => $request['locality'] , 
                       'loc_id'=> $lid
                       ]);
                       
                }
            return redirect('backend/projects')->with('info','Recored Added Successfully!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     * edit
     */
public function edit($id)
{
         $sidebarTab = 'Projects';
           $edit =DB::table('projects')
            ->leftjoin('builders', 'projects.builder_id', '=', 'builders.id')
            ->join('locations', 'projects.location_id', '=', 'locations.id')
            ->join('categories', 'projects.category_id', '=', 'categories.id')
            ->select('projects.name','projects.logo','projects.description','projects.project_image', 
            'projects.location_id','projects.is_featured','projects.url','categories.category','projects.category_id','projects.rera_id','locations.location','builders.name As builder_name','projects.bed','projects.bath','projects.min_price','projects.max_price','projects.size','projects.project_type','projects.parking','projects.project_feature','projects.phase','projects.locality','projects.meta_tag','projects.meta_desc','projects.meta_keyword','projects.og_title','projects.og_url','projects.og_type','projects.og_image','projects.og_site_name')->distinct('locations.location')
            ->where('projects.id','=',$id)
            ->first();
            //dd( $edit);
         $var=explode(',', $edit->location_id);
         $locarray=array();
         foreach($var as $x){
             $locarray[]=DB::table('locations')->where('id',$x)->first();
              
         }
        $pro_type=explode(',', $edit->project_type);
         $pTypeArray =array();
         foreach($pro_type as $pType){
             $pTypeArray[]= $pType;
              
         }

        $locations = DB::table('locations')
        ->where('is_active','=','yes')
        ->orderBy('location', 'Asc')
        ->get();
        $category = DB::table('categories')
        ->where('is_active','=','yes')
        ->orderBy('category', 'Asc')
        ->get();
        $builders=DB::table('builders')
        ->where('is_verified','=','1')
        ->orderBy('id','Asc')
        ->get();
       return view('backend.updateprojects', compact('sidebarTab', 'locarray','edit','locations','category','builders','pTypeArray'));
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
public function update(Request $request, $id)
{
         $request->validate([
            'name' => 'required',
            'location' => 'required',
            'logo'=>'image|mimes:jpg,png,jpeg,gif',
       
           ]);
         
            $loc_id='';
               if(in_array('other_loc',$request['location']) && $request['otherLoc'] !='' )  {
                $lid = DB::table('locations')->insertGetId(['location' => $request['otherLoc']]);

                $loc_id = $lid;
            }
            if(!in_array('other_loc',$request['location']) ){
              
                $prefix ='';
                foreach($request['location'] as $loc){
                   $loc_id.=  $prefix.$loc;
                    $prefix = ',';
                }
             }
            
 if($request['location']!= '' ){
   if($loc_id!=null){
          //find by id        
        $project = Project::find($id);
        //logo
         if($request->hasFile('logo')){
            $filename=Project::where('id',$id)->pluck('logo')->first();
            $file_path=public_path('upload\project_logo\\'.$filename);
                if(is_file($file_path)) {
                   unlink($file_path);
                }
                $extension= $request->file('logo')
                            ->getClientOriginalExtension();
                $logo=$request['name'].'_logo'.'.'.$extension;
                $request->file('logo')->move(public_path('upload/project_logo'),$logo);
                $project->logo=$logo;
         }
        //check project_image
        if($request->hasFile('project_image')){
          $filename=Project::where('id',$id)->pluck('project_image')->first();
          $file_path=public_path('upload\project_image\\'.$filename);
            if(is_file($file_path)) {
                unlink($file_path);
            }
            $extension = $request->file('project_image')
                        ->getClientOriginalExtension();
            $project_image=$request['name'].'.'.$extension;
            $destinationPath = public_path('upload/project_image/thumbnail');
              $img = Image::make($request->file('project_image')->getRealPath());
              $img->resize(100, 100, function ($constraint) {
              $constraint->aspectRatio();
                })->save($destinationPath.'/'.$project_image);     
            $request->file('project_image')->move(public_path('upload/project_image'),$project_image);
            $project->project_image=$project_image;
        }
        //is_featured
        $is_featured="";
        if(isset($request->is_featured)){
            $is_featured='yes';
        }else{   
            $is_featured='no';
        }
        $prefix1 = '';
        
        $proType='';
        if(!empty($request['project_type'])){
            $pro_type = $request['project_type'];
            foreach($pro_type as $key => $val2){
                        $proType.=  $prefix1.$val2;
                        $prefix1 = ',';
            } 
        }
         
        //insert locality
        if(!empty($request['locality'])){
         foreach($request['location'] as $loc){
               
               $chkLocExist =  DB::table('localities')
                        ->where('locality','=', $request['locality'] )
                        ->where('loc_id','=', $loc)
                        ->first();//print_r($chkLocExist->id);die;
                if(empty($chkLocExist)){
                       $lid = DB::table('localities')
                       ->insertGetId([
                       'locality' => $request['locality'] , 
                       'loc_id'=> $loc
                       ]);
                       
                }
        }
      }
    $project->name = $request['name'];
    $project->builder_id=$request['b_id'];
    $project->description=$request['description'];
    $project->location_id = $loc_id ;
    $project->category_id=$request['category'];
    $project->is_featured=$is_featured;
    $project->url=$request['url'];
    $project->bed=$request['bed'];
    $project->bath=$request['bath'];
    $project->min_price=$request['min_price'];
    $project->max_price=$request['max_price'];
    $project->size=$request['size'];
    $project->project_feature=$request['project_feature'];
    $project->project_type=$proType;
    $project->parking=$request['parking'];
    $project->phase=$request['phase'];
    $project->locality=$request['locality'];
    $project->rera_id=$request['rera_id'];
    $project->meta_tag=$request['meta_tag'];
    $project->meta_desc=$request['meta_desc'];
    $project->meta_keyword=$request['meta_keyword'];
    $project->og_title=$request['og_title'];
    $project->og_image=$request['og_image'];
    $project->og_type=$request['og_type'];
    $project->og_site_name=$request['og_site_name'];
    $project->og_url=$request['og_url'];
    $project->update();
               
              


    return redirect('backend/projects')->with('info','Record Updated Successfully!');
    }else{
         return redirect('backend/update-projects/'.$id)->with('info','Location is null Not Updated');
    }
 }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

//Get builder in drop down from ajax

    public function ajaxBuilder($id){

        $request = request()->all();
         $builders = DB::table('builders')
         ->select('name', 'id')
         ->whereRaw("find_in_set('".$id."',location_id)")
        ->orderBy('name', 'asc')->get();
       // print_r($builders);
        return response()->json($builders);
}


//status update
    public function updateProjectStatus(){
    $is_active=$_GET['is_active'];
   if($_GET['is_active']=='yes'){
      $is_active='no';

   }
   else if($_GET['is_active']=='no'){
  $is_active='yes';
   }
    $id=$_GET['u_id'];
    $data=array('id'=>$id,'active'=>$is_active);
DB::table('projects')->where('id',$id)->update(['is_active'=>  $is_active]);  

}



}