<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserQuery;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserQueriesExport ;
class UserQueriesController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//fetch user queries list o
  public function index(){
    $sidebarTab = 'Home';
     $queryList= DB::table('user_queries')->take(5)->orderBy('created_at','DESC')->get();
     //print_r($queryList);
     //dd($queryList);
      return view('backend/UserQueries',compact('sidebarTab','queryList'));
  }

//fetch user queries list
  public function showQueries(){
    $sidebarTab = 'Home';
     $queryList= DB::table('user_queries')->orderBy('created_at','Desc')->get();
     //print_r($queryList);
     //dd($queryList);
      return view('backend/UserQueries',compact('sidebarTab','queryList'));
  }
    //status update
  public function updateQueryStatus(){
      $result="";
        $id=explode(",",$_GET['p_id']);
      $s_active='';
     if($_GET['is_active']=='yes'){
        $is_active='no';
     } else if($_GET['is_active']=='no'){
        $is_active='yes';
     }
    
    foreach ($id as $value ) {
     $result=DB::table('user_queries')->where('query_id',$value)->update(['is_active'=>  $is_active]); 
    }
  
    if($result){
      $result="Updated Status Successfully!";
    }else{
     $result="Fail to update status";
    }
    return response()->json($result); 
  }

//////////////////////delete Queries//////////////////////////////////////
  public function deleteQueries(){
    $result='';
    $qid=explode(',',$_GET['p_id']); 
     foreach ($qid as $key ) {
         $result=DB::table('user_queries')->where('query_id',$key)->delete(); 
          if($result){
            $result="Query deleted successfully!";
          }
          else{
            $result="Fail to delete Query!";
          }
       }   

    return response()->json($result);
 }



 public function downloadXl()
    {
          $sidebarTab = 'Home';
          // $queryList= DB::table('user_queries')->orderBy('created_at','Desc')->get();
    
          return   Excel::download(new UserQueriesExport, 'DSC-UserQueries.xlsx');
             //return view('backend/UserQueries',compact('sidebarTab','queryList'));

    }

}