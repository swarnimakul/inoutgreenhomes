<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebarTab = 'Pages';
        $pageList = Page::all();

        return view('backend.pages', compact('sidebarTab', 'pageList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $sidebarTab = 'Pages';
        return view('backend.addpages', compact('sidebarTab'));
    }

     public function addHome()
    {
        $sidebarTab = 'Pages';
        return view('backend.addhomepage', compact('sidebarTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            
        ]);
        $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }
         $pagecheck = Page::where('title', $request['title'])->first();
        if ($pagecheck === null) {
         // title doesn't exist
        
        $staticPage = new Page;
        $staticPage->title = $request['title'];
        $staticPage->url = $request['url'];
        $staticPage->meta_tag = $request['meta_tag'];
        $staticPage->meta_desc = $request['meta_desc'];
        $staticPage->is_active = $is_active;
        $staticPage->type = 'static';
        $staticPage->content = $request['content'];
        $staticPage->og_title=$request['og_title'];
        $staticPage->og_image=$request['og_image'];
        $staticPage->og_type=$request['og_type'];
        $staticPage->og_site_name=$request['og_site_name'];
        $staticPage->og_url=$request['og_url'];
        $staticPage->save();
        return redirect('backend/pages')->with('info','Record Added Successfully!');
    }
    else{
       return redirect()->back()->with('info','Title already exits'); 
    }
    }

 public function storeHome(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            
        ]);
        $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }
      if($request->hasFile('home_header_image')){
        $extension=$request->file('home_header_image')->getClientOriginalExtension();
        $header_image = $request['url'].'.'.$extension;
        $request->file('home_header_image')->move(public_path('upload/header'),$header_image);
      }else{
        $header_image = '';
      }

    $pagecheck = Page::where('title', $request['title'])->first();
        if ($pagecheck === null) {
         // title doesn't exist
    $homePage = new Page;
    $homePage->title        = $request['title'];
    $homePage->url          = $request['url'];
    $homePage->worth_home   = $request['worth_home'];
    $homePage->happy_customer = $request['happy_customer'];
    $homePage->relationship_manager=$request['relationship_manager'];
    $homePage->active_listing = $request['active_listing'];
    $homePage->home_header_image    = $header_image;
    $homePage->affordable_home_content=$request['affordable_home_content'];
    $homePage->goverment_housing_content= $request['goverment_housing_content'];
    $homePage->value_added_services=$request['value_added_services'];
    $homePage->type         = 'home';
    $homePage->meta_tag     = $request['meta_tag'];
    $homePage->meta_desc    = $request['meta_desc'];
    $homePage->og_title=$request['og_title'];
    $homePage->og_type=$request['og_type'];
    $homePage->og_site_name=$request['og_site_name'];
    $homePage->og_image=$request['og_image'];
    $homePage->og_url=$request['og_url'];
    $homePage->is_active    = $is_active;
    $homePage->content      = $request['content'];
    $homePage->rera_registration_text=$request['rera_registration_text'];
    $homePage->featured_text=$request['featured_text'];
    $homePage->top_builder_text=$request['top_builder_text'];
    
    $homePage->save();
        return redirect('backend/pages')->with('info','Page Added Successfully!');
    }
    else{
       return redirect()->back()->with('info','Title already exits'); 
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function show(StaticPage $staticPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Pages';
        
        $Page = Page::find($id);
       /* echo '<pre>';
print_r($Page);
echo '</pre>';)*/
        return view('backend.addpages', compact('sidebarTab', 'Page'));
    }

   public function editHome($id)
    {
         $sidebarTab = 'Pages';
        
        $Page = Page::find($id);
       /* echo '<pre>';
print_r($Page);
echo '</pre>';)*/
        return view('backend.addhomepage', compact('sidebarTab', 'Page'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request,$id)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
           
        ]);
        $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }

    $pagecheck = Page::where('title', $request['title'])
                 ->where('id','!=',$id)
                 ->first();
        if ($pagecheck === null) {
         // title doesn't exist
        $staticPage =Page::find($id);
        $staticPage->title = $request->title;
        $staticPage->url = $request->url;
        $staticPage->meta_tag = $request->meta_tag;
        $staticPage->meta_desc = $request->meta_desc;
        $staticPage->content = $request->content;
        $staticPage->is_active=$is_active;
        $staticPage->og_title=$request['og_title'];
        $staticPage->og_image=$request['og_image'];
        $staticPage->og_type=$request['og_type'];
        $staticPage->og_site_name=$request['og_site_name'];
        $staticPage->og_url=$request['og_url'];
        $staticPage->update();

        return redirect('backend/pages')->with('info','Record Updated Successfully!');
    }
 else{
       return redirect()->back()->with('info','Title already exits'); 
    }
    }

public function updateHome(Request $request,$id)
{
        $request->validate([
            'title' => 'required',
            'url' => 'required',
           
        ]);
        $is_active="";
        if(isset( $request->is_active)){
           $is_active='yes';
        }
        else{
            $is_active='no';
        }

        $old_img=Page::select('home_header_image')
                        ->where('id',$id)
                        ->first();
         $homePage =Page::find($id);  
        if($request->hasFile('home_header_image')){
            $extension=$request->file('home_header_image')->getClientOriginalExtension();
            $header_image = $request['url'].'.'.$extension;
            $file_path=public_path('upload\header\\'.$old_img);
            if(is_file($file_path)) {
              unlink($file_path);
            }
            $request->file('home_header_image')->move(public_path('upload/header'),$header_image);
            $homePage->home_header_image    = $header_image;
        }
    $pagecheck = Page::where('title', $request['title'])
                 ->where('id','!=',$id)
                 ->first();
    if ($pagecheck === null) {
     // title doesn't exist
    $homePage->title        = $request['title'];
    $homePage->url          = $request['url'];
    $homePage->worth_home   = $request['worth_home'];
    $homePage->happy_customer = $request['happy_customer'];
    $homePage->value_added_services=$request['value_added_services'];
    $homePage->relationship_manager=$request['relationship_manager'];
    $homePage->active_listing = $request['active_listing'];
    $homePage->og_url=$request['og_url'];
    $homePage->affordable_home_content=$request['affordable_home_content'];
    $homePage->goverment_housing_content= $request['goverment_housing_content'];
    $homePage->type         = 'home';
    $homePage->meta_tag     = $request['meta_tag'];
    $homePage->meta_desc    = $request['meta_desc'];
    $homePage->og_title=$request['og_title'];
    $homePage->og_image=$request['og_image'];
    $homePage->og_type=$request['og_type'];
    $homePage->og_site_name=$request['og_site_name'];
    $homePage->is_active    = $is_active;
    $homePage->content      = $request['content'];
    $homePage->rera_registration_text=$request['rera_registration_text'];
    $homePage->featured_text=$request['featured_text'];
    $homePage->top_builder_text=$request['top_builder_text'];

    $homePage->update();
     return redirect('backend/pages')->with('info','Record Updated Successfully!');
 }
  else{
       return redirect()->back()->with('info','Title already exits'); 
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticPage $staticPage)
    {
        //
    }

//status update
    public function updateStatus(){
    $status=$_GET['status'];
   if($_GET['status']=='yes'){
      $status='no';

   }
   else if($_GET['status']=='no'){
  $status='yes';
   }
  $data=array('is_active'=>  $status);

    $id=$_GET['u_id'];
 Page::where('id',$id)->update($data);  

//return redirect('backend/pages');
}


//get home page id
public static function getHomePageID(){
     $homePage = Page::where('url','home-page')
                ->pluck('id')
                ->first();
return $homePage;
}

//builder

    public function builder(){
       
        return view('backend.builders');
           
}

}