<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PropertyType;

class PropertyTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Properties';
        $propertyList = PropertyType::all();
      
        return view('backend.propertytype', compact('sidebarTab','propertyList'));
    }


 public function add()
    {
        $sidebarTab = 'Properties';

        return view('backend.addtype', compact('sidebarTab'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    { 
          
    	$sidebarTab = 'Projects';
        $message='fail';
         $request->validate([
            'prop-type' => 'required',
         ]);
        $categorycheck = PropertyType::where('name', $request['prop-type'])->first();
        if ($categorycheck === null) {
         // category doesn't exist
            $message='success';
            $categories = new PropertyType;
            $categories->name = $request['prop-type'];
            $categories->save();
        }else{
            
        }
   return view('backend.addtype', compact('sidebarTab','message'));
    
    
  

}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Properties';
        $propertytype = PropertyType::find($id);
        return view('backend.addtype', compact('sidebarTab', 'propertytype'));
    }


public function update(Request $request,$id)
    {
        $sidebarTab = 'Projects';
        $message='fail';
        $request->validate([
            'prop-type' => 'required',
           
        ]);
     $propertycheck = PropertyType::where('name', $request['prop-type'])->where('id','!=',$id)->first();
        if ($propertycheck === null) {
         // category doesn't exist
          $proptype =PropertyType::find($id);
        $proptype->name = $request['prop-type'];
        $proptype->update();
        return redirect('backend/prop-type')->with('info','success');
    }
    return redirect('backend/prop-type')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 PropertyType::where('id',$id)->update($data);  

}
}