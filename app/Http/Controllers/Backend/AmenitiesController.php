<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Amenity;
use Image;
class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$sidebarTab = 'Properties';
        $amenityList = Amenity::all();
      
        return view('backend.amenities', compact('sidebarTab','amenityList'));
    }

    public function add()
    {
        $sidebarTab = 'Properties';

        return view('backend.addamenity', compact('sidebarTab'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    { 
          
    	$sidebarTab = 'Properties';
        $message='fail';
         $request->validate([
            'amenity' => 'required',
         ]);
        $amenitycheck = Amenity::where('name', $request['amenity'])->first();
        if ($amenitycheck === null) {
         // category doesn't exist

            $message='success';
            $image = '';
            $amenity = new Amenity;

             //check amenity image
              if($request->hasFile('image')){
                $extension = $request->file('image')->getClientOriginalExtension();
                $image   = $request['amenity'].'.'.$extension;
                $destinationPath = public_path('images/amenities/icons');
                $img = Image::make($request->file('image')->getRealPath());
                $img->resize(50, 24, function ($constraint) {
                $constraint->aspectRatio();
                })->save($destinationPath.'/'.$image);
                $request->file('image')->move(public_path('images/amenities'),$image);
              }
            $amenity->name = $request['amenity'];
            $amenity->image = $image;

            $amenity->save();
        }
   return view('backend.addamenity', compact('sidebarTab','message'));
    
    
  

}

/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backend\StaticPage  $staticPage
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
         $sidebarTab = 'Properties';
        $amenity = Amenity::find($id);
        return view('backend.addamenity', compact('sidebarTab', 'amenity'));
    }

public function update(Request $request,$id)
    {
        $sidebarTab = 'Projects';
        $request->validate([
            'amenity' => 'required',
           
        ]);
     $amenitycheck = Amenity::where('name', $request['amenity'])->where('id','!=',$id)->first();

      //check amenity image
             if($request->hasFile('image')){
                    $amenity_image = Amenity::where('id','=',$id)->first();
                    $filename = $amenity_image->image;
                    $file_path1=public_path('images/amenities/icons\\'.$filename);
                    $file_path2=public_path('images/amenities\\'.$filename);
                    if(is_file($file_path1)) {
                         unlink($file_path1);
                    }
                    if(is_file($file_path2)) {
                         unlink($file_path2);
                    }
                $extension = $request->file('image')->getClientOriginalExtension();
                $image   = $request['amenity'].'.'.$extension;
                $destinationPath = public_path('images/amenities/icons');
                $img = Image::make($request->file('image')->getRealPath());
                $img->resize(50, 24, function ($constraint) {
                $constraint->aspectRatio();
                })->save($destinationPath.'/'.$image);
                $request->file('image')->move(public_path('images/amenities'),$image);
              }else{
                    $amenity_image = Amenity::where('id','=',$id)->first();
                    $image = $amenity_image->image;
                    
              }

        if ($amenitycheck === null) {
         // amenity doesn't exist
          $amenity =Amenity::find($id);
        $amenity->name = $request['amenity'];
        $amenity->image = $image;
        $amenity->update();
        return redirect('backend/amenities')->with('info','success');
    }
    return redirect('backend/amenities')->with('info','fail');
    }

//update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 Amenity::where('id',$id)->update($data);  

}


}