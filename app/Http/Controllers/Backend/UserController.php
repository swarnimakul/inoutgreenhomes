<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\User;


class UserController extends Controller
{

  public function index() {
        $sidebarTab = 'Home';
        $getProjectCount=DB::table('projects')->count('id');
        $getBuilderCount=DB::table('builders')->count('id');
        $getAmenityCount=DB::table('amenities')->count('id');
        $getLocationCount=DB::table('locations')->count('id');
           $getPageCount=DB::table('pages')->count('id');
           $getCategoryCount=DB::table('categories')->count('id');
           $getPropertyTypeCount=DB::table('property_types')->count('id');
           $user_Queries= DB::table('user_queries')->take(5)->orderBy('created_at','DESC')->get();
          // return $user_Queries;
        return view('backend.index', compact('sidebarTab','user_Queries','getProjectCount','getBuilderCount','getAmenityCount','getLocationCount','getPageCount','getCategoryCount','getPropertyTypeCount'));
    }
    //edit profile
public function edit($id){
	$sidebarTab = 'Home';
    $user=User::find($id);
    return view('backend.myprofile',compact('user','sidebarTab'));

}

//update user
public function update(Request $request,$id){
	$sidebarTab = 'Home';
    $user=User::find($id);
    $user->name=$request->name;
    $user->email=$request->email;
    $user->phone=$request->phone;
    $user->address=$request->address;
    $user->latitude =$request->lat;
    $user->longitude=$request->lng;
    $user->office_address=$request->office_address;
    $user->update();
    if($user->update()){
return redirect('backend/edit-profile/'.$id)->with('info','success');;
}
else{
return redirect('/edit-profile/'.$id)->with('info','fail');
}

}

//////password change
public function editPassword($id){
	$sidebarTab = 'Home';
	$user=User::find($id);
	return view('backend.changepassword',compact('sidebarTab','user'));
}

//update password
public function updatePassword(Request $request,$id){
   
     $request->validate([
      'password' => 'required|string|min:6|same:cpassword',
        ]);
      
	 $oldpassword=User::find($id)->password;
	//check oldpassword with current password
	if(Hash::check($request->cupassword,$oldpassword)){
		//update password
     User::where('id', $id)
           ->update(['password' => bcrypt($request->password)]);
           //redirect
    return redirect('backend/change-password/'.$id)->with('info','success');;
	}

	//oldpassword not match return with error message
	return redirect('backend/change-password/'.$id)->with('info','fail');

	
}

 

    public function login() {
        
        if ( auth()->check() ) {
            return $this->index();
        }


        return view('backend.login');
    }

    public function doLogin(Request $request) {
        $rememberMe = 1;
        //$rememberMe = ! empty(request('rememberMe')) ? 1 : 0;

        if ( auth()->attempt([
                'email' => $request->email, 
                'password' => $request->password
            ], $rememberMe) ) {

            // Authentication passed...
            return redirect()->route('backendIndex');
        } else {
            $response = [
                'message' => 'User verification failed.',
                'status'  => false
            ];
        }

        return redirect()->route('backendLogin')->withErrors(['msg'=>'Unauthorised user access detected.']);
    }

    public function logout() {
        if( auth()->check() ) {
            auth()->logout();

            // Flushing all session
            Session::flush();
        }

        // redirect to home page
        return redirect()->route('backendLogin');
    }

    public static function getQueriesCount(){
          $getQueriesCount=DB::table('user_queries')->count('query_id');
            echo $getQueriesCount;
    }   

   
}
