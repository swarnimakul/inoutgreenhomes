<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Property;
use App\PropertyImage;
class PropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebarTab = 'Properties';
          $projpertyList =DB::table('properties')
            ->leftjoin('builders', 'properties.builder_id', '=', 'builders.id')
            ->join('locations', 'properties.location_id', '=', 'locations.id')
            ->select( 'properties.name As property_name','properties.id', 'builders.name AS builder_name','locations.location','properties.is_active As is_active','properties.price As price')
            ->get();
        return view('backend.properties', compact('sidebarTab','projpertyList'));
    }

    
    /**
     * add a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function add(Request $request)
    { 
          
        $sidebarTab = 'Properties';
        $locations = DB::table('locations')
                ->where('is_active','=','yes')
                ->orderBy('location', 'Asc')
                ->get();
        $categories = DB::table('categories')
        ->where('is_active','=','yes')
        ->orderBy('category', 'Asc')
        ->get();  
       
        $amenities = DB::table('amenities')
        ->where('is_active','=','yes')
        ->orderBy('name', 'Asc')
        ->get();

        $types = DB::table('property_types')
        ->where('is_active','=','yes')
        ->orderBy('name', 'Asc')
        ->get();

        $projects = DB::table('projects')
        ->orderBy('name', 'Asc')
        ->get();

        $builders = DB::table('builders')
        ->orderBy('name', 'Asc')
        ->get();

        $message='';  


        return view('backend.addproperties', compact('sidebarTab','message','categories','locations','amenities','projects','types','builders'));
    
    }

    public function addDetails($project_id)
    { 
          
        $sidebarTab = 'Properties';
        $project_detail = DB::table('projects')->where('id', '=', $project_id)->first();
        
        $categories = DB::table('categories')->where('id', '=',$project_detail->category_id)->select('id','category')
         ->first();  
       //print_r($categories);die;
        $amenities = DB::table('amenities')
         ->where('is_active','=','yes')
        ->orderBy('name', 'Asc')
        ->get();

        $types = DB::table('property_types')
         ->where('is_active','=','yes')
        ->orderBy('name', 'Asc')
        ->get();
        $locIds = explode(",",$project_detail->location_id);
        $prifix ='';
        $loc = '';
        foreach($locIds as $key=> $val){
            $locations = DB::table('locations')->where('id','=',$val)
                ->orderBy('location', 'Asc')
                ->first();
            $loc .= $prifix.$locations->location;
            $prefix = ',';
        }
        $builders = DB::table('builders')->where('id', '=', $project_detail->builder_id)
                    ->select('id','name')
        ->first();

        $message='';  


        return view('backend.add_project_detail', compact('sidebarTab','project_detail','message','categories','locations','amenities','types','builders','loc'));
    
    }
    /**
     * store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store(Request $request)
    {
        $this->validate($request,[
        'name'=>'required',
        'url'=>'required',
        'broucher'=>'file|mimes:pdf',
         ]);
        $error='';
        $prefix = '';
        $prefix1 = '';
        $amenityIds='';
        $typeIds='';
        $sidebarTab='Properties';
        $property_url=$request['url'];
            
       if(!empty($request['amenity'])){
        $amenities = $request['amenity'];
         foreach($amenities as $key1 => $val1){
                    $amenityIds.=  $prefix.$val1;
                    $prefix = ',';
         }
        } 
        if(!empty($request['type'])){
        $types = $request['type'];
         foreach($types as $key2 => $val2){
                    $typeIds.=  $prefix1.$val2;
                    $prefix1 = ',';
         }
        } 
         $loc_id ='';
         $cat_id='';
         if($request['l_id']=='other_loc' && $request['otherLoc'] !='' ){
                    $chkLocExist =  DB::table('locations')
                        ->where('location','=', $request['otherLoc'] )
                        ->first();
                    //print_r($chkLocExist->id);die;
                     if(empty($chkLocExist)){
                       $lid = DB::table('locations')->insertGetId(['location' => $request['otherLoc']]);
                        $loc_id = $lid;
                     }else{
                           $loc_id = $chkLocExist->id;
                     }
                   
                }else{
                    //$lids = $request['l_id']
                    $prefix3 ='';
                    $loc_id = '';
                    $loc_ids = $request['l_id'];
                    if(!empty($loc_ids)){
                        foreach($loc_ids as $key => $val){
                            $loc_id.=  $prefix3.$val;
                            $prefix3 = ',';
                        }
                    }
                }
        if($request['category']=='other_cat' && $request['otherCat'] !='' ){
                        $chkCatExist =  DB::table('categories')
                        ->where('location','=', $request['otherCat'] )
                        ->first();
                     if(empty($chkCatExist)){
                        $cid = DB::table('categories')->insertGetId(['category' => $request['otherCat']]);
                        $cat_id = $cid;
                    }else{
                        $cat_id = $chkCatExist->id;
                    }
            }else{
               $cat_id = $request['category'];
            }
        $alreadyExist =  DB::table('properties')
                        ->where('name','=', $request->name )
                        ->first();

        if(!empty($alreadyExist)){
           $error ="Property name already exist";
           return redirect('backend/add-property')->with('info','Property name already exists.');
        
          }
         else{

            if($request->hasFile('broucher')){
               $extension=$request->file('broucher')->getClientOriginalExtension();
              $fileName=$property_url.'.'.$extension;
              $request->file('broucher')->move(public_path('upload/brouchers'),$fileName);
            }else{
                $fileName='';
            }
            if($request->hasFile('site_plan')){
               $extension=$request->file('site_plan')->getClientOriginalExtension();
              $site_plan=$property_url.'.'.$extension;
              $request->file('site_plan')->move(public_path('upload/project_image/site_plan'),$site_plan);
            }else{
                $site_plan='';
            }
            //is_featured
             $is_featured="";
                if(isset( $request->is_featured)){
                   $is_featured='yes';
                }
                else{
                    $is_featured='no';
                }
                    //save properties
            $property = new Property;
            $property->name = $request['name'];
            $property->url = $request['url'];
            $property->builder_id  = $request['b_id'];
            $property->project_id = $request['project_id'];
            $property->location_id = $loc_id;
            $property->category_id = $cat_id;
            $property->price = $request['price'];
            $property->type = $typeIds;
            $property->sold = $request['sold'];
            $property->contract = $request['contract'];
            $property->home_area = $request['home_area'];
            $property->lot_area = $request['lot_area'];
            $property->lot_dimension = $request['lot_dimension'];
            $property->garages = $request['garages'];
            $property->rooms = $request['room'];
            $property->bed = $request['bed'];
            $property->bath = $request['bath'];
            $property->description = $request['description'];
            $property->web_url = $request['website'];
            $property->brochure = $fileName; 
            $property->address = $request['address']; 
            $property->latitude = $request['lat']; 
            $property->longitude = $request['lng']; 
            $property->property_amenities = $amenityIds;
            $property->is_featured = $is_featured;
            $property->resale_price=$request['resale_price'];
            $property->builder_price=$request['builder_price'];
            $property->initial_price=$request['initial_price'];
            $property->booking_amount=$request['booking_amount'];
            $property->terrace_comp_amt=$request['terrace_comp_amt'];
            $property->possesion_amt=$request['possesion_amt'];
            $property->booking_payment_time=$request['booking_payment_time'];
            $property->site_plan_image=$site_plan;
            
            $property->save();

           $imgArray = $request->file('images');
           $f_planArray = $request->file('floor_plan');
           $slectId = DB::table('properties')
                        ->where('name', '=' ,$request['name'] )
                        ->select('id')
                        ->first();
                        $i=1;
                        //$j=1;
             //insert gallery images    
            if(!empty($imgArray)){
                  
                foreach($imgArray as $img=>$val){
                    $imgext=$val->getClientOriginalExtension();
                    $imgName=$request['url'].$i++.'_'.time().'.'.$imgext;
                   $val->move(public_path('upload/property_gallery'),$imgName);
                  
                   $insertImg = DB::table('property_images')->insert([
                       'img_name' => $imgName,
                       'property_id' => $slectId->id
                       ]);
                
                }
            }
            //insert floor plan images
            $j =0;
            $f_planArray = $request['floor_plan'];
            
             if(!empty($f_planArray)){
                foreach($f_planArray as $img1=>$val1){
                    $imgext1=$val1->getClientOriginalExtension();
                    $imgName1=$request['url'].$i++.'_'.time().'.'.$imgext1;
                   $val1->move(public_path('upload/floor_plans'),$imgName1);
                   $type=$request['floor_plan_type'][$j++];
                    if($type!= '' && $imgName1!=''){
                      $insertImg1 = DB::table('property_floorplans')->insert([
                       'type'  => $type,
                       'image' => $imgName1,
                       'property_id' => $slectId->id
                       ]);
                    }
                
                }
            }
        
            return redirect('backend/properties')->with('info','Property added Successfully.');
        }
    }

    public function storeDetail(Request $request)
    {
        $this->validate($request,[
        'name'=>'required',
        'url'=>'required',
        'broucher'=>'file|mimes:pdf',
         ]);
        $error='';
        $prefix = '';
        $prefix1 = '';
        $amenityIds='';
        $typeIds='';
        $sidebarTab='Properties';
        $property_url=$request['url'];
            
       if(!empty($request['amenity'])){
        $amenities = $request['amenity'];
         foreach($amenities as $key1 => $val1){
                    $amenityIds.=  $prefix.$val1;
                    $prefix = ',';
         }
        } 
        if(!empty($request['type'])){
        $types = $request['type'];
         foreach($types as $key2 => $val2){
                    $typeIds.=  $prefix1.$val2;
                    $prefix1 = ',';
         }
        } 
        
        $prefix4 ='';
        $nearByplaces ='';
        if( !empty($request['nearBy'])){
        $nearBy = $request['nearBy'];
        foreach($nearBy as $nearByKey => $nearByVal){
                    $nearByplaces.=  $prefix4.$nearByVal;
                    $prefix4 = ',';
         }
        }else{
            $nearByplaces ='';
        }
                  
        $loc_id=  $request['l_id'];
        $cat_id = $request['category'];
            
        $alreadyExist =  DB::table('properties')
                        ->where('name','=', $request->name )
                        ->first();

        if(!empty($alreadyExist)){
           $error ="Property name already exist";
           return redirect('backend/add-property')->with('info','Property name already exists.');
        
          }
         else{

            if($request->hasFile('broucher')){
               $extension=$request->file('broucher')->getClientOriginalExtension();
              $fileName=$property_url.'.'.$extension;
              $request->file('broucher')->move(public_path('upload/brouchers'),$fileName);
            }else{
                $fileName='';
            }
            if($request->hasFile('site_plan')){
               $extension=$request->file('site_plan')->getClientOriginalExtension();
              $site_plan=$property_url.'_site_plan.'.$extension;
              $request->file('site_plan')->move(public_path('upload/project_image/site_plan'),$site_plan);
            }else{
                $site_plan='';
            }
           
                    //save properties
            $property = new Property;
            $property->name = $request['name'];
            $property->url = $request['url'];
            $property->builder_id  = $request['b_id'];
            $property->project_id = $request['project_id'];
            $property->location_id = $loc_id;
            $property->category_id = $cat_id;
            $property->price = $request['price'];
            $property->type = $typeIds;
            $property->sold = $request['sold'];
            $property->contract = $request['contract'];
            $property->home_area = $request['home_area'];
            $property->lot_area = $request['lot_area'];
            $property->lot_dimension = $request['lot_dimension'];
            $property->garages = $request['garages'];
            $property->rooms = $request['room'];
            $property->bed = $request['bed'];
            $property->bath = $request['bath'];
            $property->description = $request['description'];
            $property->web_url = $request['website'];
            $property->brochure = $fileName; 
            $property->address = $request['address']; 
            $property->latitude = $request['lat']; 
            $property->longitude = $request['lng']; 
            $property->property_amenities = $amenityIds;
            $property->is_featured = 'yes';
            $property->rera_id=$request['rera_id'];
            
            $property->resale_price=$request['resale_price'];
            $property->builder_price=$request['builder_price'];
            $property->initial_price=$request['initial_price'];
            $property->booking_amount=$request['booking_amount'];
            $property->terrace_comp_amt=$request['terrace_comp_amt'];
            $property->possesion_amt=$request['possesion_amt'];
            $property->booking_payment_time=$request['booking_payment_time'];
            $property->site_plan_image=$site_plan;
            $property->near_by = $nearByplaces;
            
            $property->save();

           $imgArray = $request->file('images');
           $f_planArray = $request->file('floor_plan');
           $slectId = DB::table('properties')
                        ->where('name', '=' ,$request['name'] )
                        ->select('id')
                        ->first();
                        $i=1;
                        //$j=1;
             //insert gallery images    
            if(!empty($imgArray)){
                  
                foreach($imgArray as $img=>$val){
                    $imgext=$val->getClientOriginalExtension();
                    $imgName=$request['url'].$i++.'_'.time().'.'.$imgext;
                   $val->move(public_path('upload/property_gallery'),$imgName);
                  
                   $insertImg = DB::table('property_images')->insert([
                       'img_name' => $imgName,
                       'property_id' => $slectId->id
                       ]);
                
                }
            }
            //insert floor plan images
            $j =0;
            $f_planArray = $request['floor_plan'];
            
             if(!empty($f_planArray)){
                foreach($f_planArray as $img1=>$val1){
                    $imgext1=$val1->getClientOriginalExtension();
                    $imgName1=$request['url'].$i++.'_'.time().'.'.$imgext1;
                   $val1->move(public_path('upload/floor_plans'),$imgName1);
                   $type=$request['floor_plan_type'][$j++];
                    if($type!= '' && $imgName1!=''){
                      $insertImg1 = DB::table('property_floorplans')->insert([
                       'type'  => $type,
                       'image' => $imgName1,
                       'property_id' => $slectId->id
                       ]);
                    }
                
                }
            }
        
            return redirect('backend/projects')->with('info','Project details added Successfully.');
        }
    }


    public function ajaxProject($id){
       // echo $id;die;
       $ids = explode(",",$id);
       foreach($ids as $key=>$val){
        $projects[] = DB::table('projects')
            ->select('name', 'id')
            ->where("location_id",'=',$val)
            ->orderBy('name', 'asc')->get();
       }
         
        return response()->json($projects);
    }


    public function ajaxLocBuilder($id){
        $ids = explode(",",$id);
         $builders = DB::table('builders')
                ->select('name', 'id')
                ->orderBy('name', 'asc')
                ->where('location_id', 'like', $id);
                if(sizeof($ids) > 1){
                 echo "test";
                 $builders->wherein("location_id",array($id));
                 $builders->orwhere("location_id" , "like" , $id);
                }if(sizeof($ids) == 1){
                
                  $builders->where("location_id",'=',$id);
                  $builders->orwhereRaw("find_in_set('".$id."',location_id)");
                }
               // ->orwhereRaw("find_in_set('".$id."',location_id)")
                $builders = $builders->get();
      
        return response()->json($builders);

    }


    public function ajaxBuilderProject($id){

        $projects = DB::table('projects')
         ->select('name', 'id')
         ->where('builder_id' ,'=',$id)
        ->orderBy('name', 'asc')->get();
        //print_r($builders);
        return response()->json($projects);
    
}


      //update status
public function updateStatus(){
     $is_active=$_GET['status'];
   if($_GET['status']=='yes'){
      $is_active='no';

   }
   else if($_GET['status']=='no'){
  $is_active='yes';
   }
  $data=array('is_active'=>  $is_active);

    $id=$_GET['u_id'];
 Property::where('id',$id)->update($data);  

}
//edit properties

public function edit($id){
        $sidebarTab ='';
        $amenities=DB::table('amenities')->where('is_active', '=', 'yes')->get();
        $propertyList =DB::table('properties')->where('id',$id)
                        ->first();
        $types=DB::table('property_types')->where('is_active', '=', 'yes')->get();
        
        $typeIdArray=explode(',',$propertyList->property_amenities);
        $propertyTypeId=explode(',',$propertyList->type);
        $nearByArray=explode(',',$propertyList->near_by);
        
        $gallery=DB::table('property_images')->where('property_id',$id)->get();
        $floorPlan=DB::table('property_floorplans')->where('property_id',$id)->get();

        $locations = DB::table('locations')
         ->where('is_active','=','yes')
                ->orderBy('location', 'Asc')
                ->get();
        $categories = DB::table('categories')
         ->where('is_active','=','yes')
        ->orderBy('category', 'Asc')
        ->get();  
       
        $amenities = DB::table('amenities')
         ->where('is_active','=','yes')
        ->orderBy('name', 'Asc')
        ->get();

        $builders=DB::table('builders')
                    ->select('name','id')
                    ->get();

        $locIdArray=explode(',',$propertyList->location_id);
       
//print_r($builders);die;
        $projects=DB::table('projects')
                      ->select('name','id','url','rera_id')
                    ->get();

         $projectInfo=DB::table('projects')
                      ->where('id','=',$propertyList->project_id )
                      ->select('name','id','url','rera_id','category_id','builder_id','location_id')
                    ->first();
      $types = DB::table('property_types')
        ->orderBy('name', 'Asc')
        ->get();
 return view('backend/updateprperties',compact('typeIdArray','amenities','types','sidebarTab','propertyTypeId','projects','propertyList','locations','categories','floorPlan','builders','locIdArray','gallery','nearByArray','projectInfo'));        
}


public function deletePropImg($id,$propId){
    $propImg =  DB::table('property_images')
                        ->where('id','=', $id )
                        ->first();
    $file_path=public_path('upload\property_gallery\\'.$propImg->img_name);
    if(is_file($file_path)) {
       unlink($file_path);
    }
   DB::table('property_images')->where('id', '=', $id)->delete();

    $content ='';
    $propImgData =  DB::table('property_images')
                        ->where('property_id','=', $propId )
                        ->get();
    foreach($propImgData as $img){
        $content .="<div style='width:98px;height: 90px; float: left; border: 1px solid black; margin-left: 5px; margin-top: 2px'>
                    <img class='img-responsive' height='88' width='88'    style='margin-left: 5px; margin-top: 5px;' src='../../public/upload/property_gallery\\$img->img_name'>
                    <div><a href='javascript:void(0)' onclick='deleteImage($img->id,$img->property_id)'>Delete</a></div>
                    </div>";
                     }
            //echo $content;
            return response()->json($content);

}

public function deletePropf_Img($id,$propId){
    $propImg =  DB::table('property_floorplans')
                        ->where('id','=', $id )
                        ->first();
    $file_path=public_path('upload\floor_plans\\'.$propImg->image);
    if(is_file($file_path)) {
       unlink($file_path);
    }
   DB::table('property_floorplans')->where('id', '=', $id)->delete();

    $content ='';
    $propImgData =  DB::table('property_floorplans')
                        ->where('property_id','=', $propId )
                        ->get();
    foreach($propImgData as $img){
        $content .="<div style='width:98px;height: 90px; float: left; border: 1px solid black; margin-left: 5px; margin-top: 2px'>
                    <img class='img-responsive' height='70' width='88'    style='margin-left: 5px; margin-top: 5px; width: 88px; height: 70px;' src='../../public/upload/floor_plans\\$img->image'>
                    <div><a href='javascript:void(0)' onclick='deletef_Image($img->id,$img->property_id)'>Delete</a></div>
                    </div>";
                     }
            //echo $content;
            return response()->json($content);

}



///update 
public function update(Request $request,$id){
        $error='';
        $prefix = '';
        $prefix1 = '';
        $amenityIds='';
        $typeIds='';
        $sidebarTab='Properties';
        $property_name=$request['name'];
        if(!empty($request['amenity'])){
        $amenities = $request['amenity'];
         foreach($amenities as $key1 => $val1){
                    $amenityIds.=  $prefix.$val1;
                    $prefix = ',';
         }
        } 
        if(!empty($request['type'])){
        $types = $request['type'];
         foreach($types as $key2 => $val2){
                    $typeIds.=  $prefix1.$val2;
                    $prefix1 = ',';
         }
        } 
        $prefix4 ='';
        $nearByplaces ='';
        if(isset($request['nearBy']) && !empty($request['nearBy'])){
        
        $nearBy = $request['nearBy'];
        
         foreach($nearBy as $nearByKey => $nearByVal){
                    $nearByplaces.=  $prefix4.$nearByVal;
                    $prefix4 = ',';
         }
        } else{
            $nearByplaces ='';
        }
         $loc_id =$request['l_id'];
         $cat_id = $request['category'];
         
       
        if(!empty($alreadyExist)){
           $error ="Property name already exist";
           return redirect('backend/add-property')->with('info','Property name already exists.');
        }
        else{
           // $fileName ='';
            $proBroch = DB::table('properties')->where('id',$id)->select('brochure')->first();
           // print_r($proBroch->brochure);die;
            if($request->hasFile('broucher')){
              $extension=$request->file('broucher')->getClientOriginalExtension();
              $fileName=$request['url'].'.'.$extension;
              $file_path=public_path('upload\brouchers\\'.$proBroch->brochure);
                if(is_file($file_path)) {
                 unlink($file_path);
                }
              $request->file('broucher')->move(public_path('upload/brouchers'),$fileName);
            }else{
              $fileName = $proBroch->brochure;
            }

            $pro_site_plan = DB::table('properties')->where('id',$id)->select('site_plan_image')->first();
            if($request->hasFile('site_plan')){
              $extension=$request->file('site_plan')->getClientOriginalExtension();
              $site_plan=$request['url'].'_site_plan.'.$extension;
              $site_plan_path=public_path('upload\project_image\site_plan\\'.$proBroch->brochure);
                if(is_file($site_plan_path)) {
                 unlink($site_plan_path);
                }
              $request->file('site_plan')->move(public_path('upload/project_image/site_plan'),$site_plan);
            }else{
              $site_plan = $pro_site_plan->site_plan_image;
            }
            //is_featured
             $is_featured="";
                if(isset($request->is_featured)){
                   $is_featured='yes';
                }
                else{
                    $is_featured='no';
                }
            //update properties
            $property = Property::find($id);
            $property->name = $property_name;
            $property->url = $request['url'];
            $property->builder_id  = $request['b_id'];
            $property->project_id = $request['project_id'];
            $property->location_id = $loc_id;
            $property->category_id = $cat_id;
            $property->price = $request['price'];
            $property->type = $typeIds;
            $property->sold = $request['sold'];
            $property->contract = $request['contract'];
            $property->home_area = $request['home_area'];
            $property->lot_area = $request['lot_area'];
            $property->lot_dimension = $request['lot_dimension'];
            $property->garages = $request['garages'];
            $property->rooms = $request['room'];
            $property->bed = $request['bed'];
            $property->bath = $request['bath'];
            $property->description = $request['description'];
            $property->web_url = $request['website'];
            $property->brochure = $fileName; 
            $property->address = $request['address']; 
            $property->latitude = $request['lat']; 
            $property->longitude = $request['lng']; 
            $property->property_amenities = $amenityIds;
            $property->is_featured = 'yes';
            $property->rera_id=$request['rera_id'];
            $property->resale_price=$request['resale_price'];
            $property->builder_price=$request['builder_price'];
            $property->initial_price=$request['initial_price'];
            $property->booking_amount=$request['booking_amount'];
            $property->terrace_comp_amt=$request['terrace_comp_amt'];
            $property->possesion_amt=$request['possesion_amt'];
            $property->booking_payment_time=$request['booking_payment_time'];
            $property->site_plan_image=$site_plan;
            $property->near_by=$nearByplaces;
           $property->update();
           $imgArray = $request->file('images');
           $i=1;
           if(!empty($imgArray)){
                
                   foreach($imgArray as $img=>$val){
                    //print_r($val);die;
                    $imgext=$val->getClientOriginalExtension();
                    $imgName=$request['url'].$i++.'_'.time().'.'.$imgext;
                     $val->move(public_path('upload/property_gallery'),$imgName);
                     $insertImg = DB::table('property_images')->insert([
                           'img_name' => $imgName,
                           'property_id' => $id
                           ]);
                   }
            }
            //update floor plan images
            $j =0;
            $f_planArray = $request['floor_plan'];
            //dd($request['floor_plan_type'][$j++]);
            if(!empty($f_planArray)){
                foreach($f_planArray as $img1=>$val1){
                    $imgext1=$val1->getClientOriginalExtension();
                    $imgName1=$request['url'].$i++.'_'.time().'.'.$imgext1;
                   $val1->move(public_path('upload/floor_plans'),$imgName1);
                   $type=$request['floor_plan_type'][$j++];
                    if($type!= '' && $imgName1!=''){
                      $insertImg1 = DB::table('property_floorplans')->insert([
                       'type'  => $type,
                       'image' => $imgName1,
                       'property_id' => $id
                       ]);
                    }
                
                }
            }
            return redirect('backend/projects')->with('info','Project details updated Successfully.');
        }
         
   }
}