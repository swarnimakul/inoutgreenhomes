<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Corcel\Model\Post;
use Corcel\Model\Taxonomy;
use Corcel\Model\Category;


class HomePageController extends Controller
{
	public function index() {
       
    $trendingPosts=DB::table('blogs')
    ->where('category_id','2')
    ->orderby('id','Asc')->get(); 
      
    $l_cat_post=DB::table('blogs')
    ->where('category_id','1')
    ->orderby('id','Asc')->get(); 
 $footerPost = array();
  //$footerPost =  Post::type('post')->newest()->paginate(4);
//////////////////////////end wp posts/////////////////////////

		    //featured projects
         $projects = DB::table('projects')
            ->select('projects.id','projects.name','projects.project_image','projects.bed','projects.bath','projects.size','projects.min_price','projects.parking','projects.project_feature','projects.url')

             ->where('projects.is_featured','=','yes')
             ->where('projects.is_active','=','yes')
             ->orderby('projects.id','Desc')
             ->take(20)
             ->get();
          ///end featured
          $homePage = DB::table('pages')
             		->where('url','home-page')
             		->first();
          $affHomeContent = $homePage->affordable_home_content;
          $content=$homePage->content;
          $worth_home = 	$homePage->worth_home;
          $happy_customer = $homePage->happy_customer;
          $relationship_manager = $homePage->relationship_manager;
          $active_listing = $homePage->active_listing;
          $govtHousing = $homePage->goverment_housing_content;
          $rera_registration_text=$homePage->rera_registration_text;
          $featured_text=$homePage->featured_text;
          $title=$homePage->title;
          $top_builder_text=$homePage->top_builder_text;
          $meta_desc=$homePage->meta_desc;
          $banner= $homePage->home_header_image;
          $value_added_services=$homePage->value_added_services;
          $og_title=$homePage->og_title;
        $og_image=$homePage->og_image;
        $og_type=$homePage->og_type;
        $og_site_name=$homePage->og_site_name;
        $og_url=$homePage->og_url;

        $builders=DB::table('builders')
        ->select('id','name')
        ->where('top_builder','yes')
        ->orderby('id','Asc')->get(); 
          
         $builderProject =DB::table('builders')
        ->leftjoin('projects','projects.builder_id','=','builders.id') 
        ->leftjoin('locations','locations.id','=','projects.location_id')     
           
        ->select('projects.name','builders.id','builders.name as builder_name','projects.project_image','projects.location_id','projects.min_price','projects.max_price','projects.url','locations.location')
        ->where('projects.is_active','yes')
        ->where('builders.top_builder','yes')
        ->orderby('projects.builder_id','Asc')
       
        ->get();
        $address=DB::table('users')
                 ->select('phone','address','office_address','email')->first();   

		return view('index',compact('projects','worth_home','happy_customer','relationship_manager','active_listing','affHomeContent','govtHousing','builderProject','builders','address','top_builder_text','featured_text','rera_registration_text','content','title','meta_desc','banner','value_added_services','trendingPosts','l_cat_post','og_title','og_image','og_type','og_site_name','og_url','footerPost'));
	}





	//to get ajax data searching by location
	public static function searchByLoc($id){
		$projects = DB::table('projects')
            ->whereRaw("find_in_set('".$id."',location_id)")
            ->where('is_active','=','yes')
            ->where('is_featured','=','yes')
           // ->take(5)
           ->get();
        return view('layouts.featured-ajax',compact('projects'));
  }

	
// serach by category ajax data
   public static function searchByCat($id){
		$projects = DB::table('projects')
             ->where('category_id','=',$id)
              ->where('is_active','=','yes')
            ->where('is_featured','=','yes')
             ->take(20)
             ->get();
        //echo sizeof($projects);die;
       return view('layouts.featured-ajax',compact('projects'));
  
	}

	
}
