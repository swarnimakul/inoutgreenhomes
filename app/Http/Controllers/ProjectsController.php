<?php

namespace App\Http\Controllers;

use App\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
//use Corcel\Model\Post;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
       $locId ='';

       $projectsList=DB::table('projects')
           
           ->leftjoin('locations', 'locations.id', '=', 'projects.location_id')
            ->leftjoin('categories','categories.id','=','projects.category_id')
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.project_image','projects.id','locations.location','projects.description','projects.location_id','projects.min_price','projects.phase','projects.possasion_date','projects.project_type','categories.category','projects.created_at','projects.size','projects.logo','projects.locality','projects.url','projects.project_feature')
              //->take(10)
              ->orderBy('projects.created_at', 'Dsc')
              ->where('projects.is_active','=','yes')
              ->get();

            $address=DB::table('users')
                 ->select('phone','address','office_address','email')->first();   
        $locData = DB::table('locations')->select('location','id')->get();  
        $locality = DB::table('localities')->distinct()->get(['locality']);    
        
        $selected_tab  = 'projects' ;
        $footerPost = array();
        //$posts = Post::type('post')->newest()->paginate(4);
        $pageData=DB::table('pages')
                    ->where('url','=','projects')
                    ->first();
                    $title=$pageData->title;
            $meta_desc=$pageData->meta_desc;
        $builderList=DB::table('builders')
                    ->select('id','name')
                    ->where('is_verified','=','1')
                    ->get(); 
        $dropDownProjects=DB::table('projects')
                    ->select('id','name')
                    ->where('is_active','=','yes')
                    ->get();   
        return view('projects',compact('projectsList','address','title','locData','locId','locality','meta_desc','selected_tab','footerPost','pageData','builderList','dropDownProjects'));


    }

 //get project by laocation
  public static function get_pr_by_loc($loc){

       $location = str_replace('-',' ', $loc);
       $l_title = ucwords($location);
       $selId = DB::table('locations')->select('id','location')->where('location','=',$location)->first();
       $locId = '';
       if(!empty($selId)){
        $locId = $selId->id;
        $projectsList=DB::table('projects')
                   ->leftjoin('locations', 'locations.id', '=', 'projects.location_id')
            ->leftjoin('categories','categories.id','=','projects.category_id')
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.project_image','projects.id','locations.location','projects.description','projects.location_id','projects.min_price','projects.phase','projects.possasion_date','projects.project_type','categories.category','projects.created_at','projects.size','projects.logo','projects.locality','projects.url','projects.project_feature')
              //->take(10)
              ->orderBy('projects.created_at', 'Dsc')
                    ->where('projects.is_active','=','yes')
                    ->whereRaw("find_in_set('".$locId."',location_id)")
                    ->get();

       }

            $address=DB::table('users')
                 ->select('phone','address','office_address','email')->first(); 
            $pageUrl = $loc."-projects"  ;
            $pageData=DB::table('pages')
                    ->where('url','=',$pageUrl)
                    ->first();
                    $title='';
                    $meta_desc='';
          if(!empty($pageData)){
            $title=$pageData->title;
            $meta_desc=$pageData->meta_desc;
          }
            $locData = DB::table('locations')->select('location','id')->get();
            if($locId !=''){
            $locality = DB::table('localities')->select('locality','id')->where('loc_id','=',$locId)->get(); 
            }else{
              $locality = DB::table('localities')->select('locality','id')->get(); 
            }   
       
        $selected_tab  = 'projects' ;
        $footerPost = array();//
        //$posts = Post::type('post')->newest()->paginate(4);
          $builderList=DB::table('builders')
                    ->select('id','name')
                    ->where('location_id','=',$locId)
                    ->where('is_verified','=','1')
                    ->get(); 
        $dropDownProjects=DB::table('projects')
                    ->select('id','name')
                    ->where('location_id','=',$locId)
                    ->where('is_active','=','yes')
                    ->get();   
        return view('projects',compact('projectsList','address','title','locData','locId','locality','meta_desc','selected_tab','footerPost','pageData','builderList','dropDownProjects'));


  }

  

  public function autocomplete(Request $request)
    {
        $query = $request->get('query','');        

        $project['Project'] = DB::table('projects')->select('name')->where('name','LIKE','%'.$query.'%')->where('is_active','=','yes')->get();        
        $builder['Builder'] = DB::table('builders')->select('name')->where('name','LIKE','%'.$query.'%')->where('is_verified','=','1')->get();        
       //echo "<pre>"; print_r(array_merge($a1,$a2)); echo "</pre>";
        //die;
        $data = array_merge($builder,$project);
       
          foreach( $data as $key1=>$value1){
                           foreach($value1 as$key=>$value){
                               $dataItems[$key1][$key] = $key1.": ". $value->name;
       
                            }
                         
                           }
                        //echo "<pre>"; print_r($dataItems); echo "</pre>";
        //die;
       // $data = array_merge($builder,$project);
        return response()->json($dataItems);
       
    }

public function autocompleteGetURL(Request $request){
   $query = $request->get('name');  
  // echo $query;
   $project = DB::table('projects')
   ->select('url')->where('name',$query)
   ->first();
   if(empty( $project)){
  $project = DB::table('builders')
   ->select('url')->where('name',$query)
   ->first();
   }
  return response()->json($project);
}


    ///fetch projects details
 public static function  projectsDetails($url){

  //echo $url;die;
   $selected_tab  = 'projects' ;
   $name = str_replace("-", " ", $url);
         
 $address =DB::table('users')->select('phone','address','office_address','email')->first(); 
 $projectInfo = DB::table('projects')->where('url','=',$url)->first();
 $priceInfo = array();
 if(!empty($projectInfo)){
 $priceInfo = DB::table('property_prices')->where('project_id','=',$projectInfo->id)->get();           
}
 $sizeOfpriceInfo = sizeof($priceInfo); 
 //echo $priceInfo[0]->price_plan; die;
 /////////////////////////Project details from property table///////////////////////
 $project_id ='';
 if(!empty($projectInfo)){
   $project_id = $projectInfo->id;
 }
 $projectDetail = DB::table('properties')->where('project_id','=',$project_id)->first();
 $nearByArr = array();
 $amenities =array();
 if(!empty($projectDetail)){
  $nearByArr = explode(",",$projectDetail->near_by);
 
 $floorPlan = DB::table('property_floorplans')->where('property_id','=',$projectDetail->id)           ->get();
 $gallery = DB::table('property_images')->where('property_id','=',$projectDetail->id)           ->get();

/////////////////get amenities/////////////////////////////////////////
      if($projectDetail->property_amenities !=''){
       $amenity = explode(',',$projectDetail->property_amenities);
         if(!empty($amenity)){
            foreach($amenity as $ameVal => $val){
                $amenityArr = DB::table('amenities')->where('id',$val)->first();
                if(!empty($amenityArr)){
                  $amenities[] = $amenityArr->name;
                }
            }
          }
      }
///////////////////////////end amenities//////////////////////////
       
      //print_r($amenities);die;
      
 }
 ////////////////////////////End project details//////////////////////////////
 //////////////builder Information///////////////
 $p_builder_id ='';
 $p_location_id = '';
 if(!empty($projectInfo)){
   $p_builder_id = $projectInfo->builder_id;
   $p_location_id = $projectInfo->location_id;
   $locIdArray=explode(',',$p_location_id);
            $loc='';
            $prefix = '';  
          if(!empty($locIdArray)){
              foreach ($locIdArray as $v)
               {
                $location=DB::table('locations')->where('id',$v)->first();
                  $loc.= $prefix.$location->location;
                  $prefix = ",";
                }
          }
 
 $builderInfo = DB::table('builders')->where('id','=',$p_builder_id)
                ->select('id','name','url','description')
                ->first();   
//////////////////end puilder Info/////////////////////
  if(!empty($builderInfo)){
    $builder_url = str_replace(" ", "-", $builderInfo->name);
                  
   
   $popularProjects = DB::table('projects')
  ->where('projects.is_active','=','yes')
  ->where('projects.builder_id','=',$builderInfo->id)
                     ->leftjoin('properties','projects.id','=','properties.project_id')
                     ->select('projects.name','projects.id','projects.project_image','projects.url','properties.price as pro_price')
                     ->where('projects.is_active','=','yes')
                     ->where('projects.id','!=',$projectInfo->id)  
                     ->get(); 

  }
              //echo "<pre>";print_r($amenities);echo "</pre>"; die; 
 }else{
  $builderInfo =array();
  $popularProjects =array();
 }

  $footerPost = array();
  //$posts = Post::type('post')->newest()->paginate(4);

  return view('projectsdetails',compact('projectInfo','selected_tab','address','builderInfo','builder_url','popularProjects','projectDetail','floorPlan','gallery','loc','nearByArr','amenities','priceInfo','sizeOfpriceInfo','footerPost'));

 }


 public static function  getProjLoc($locIds){
        $locIdArray=explode(',',$locIds);
            $loc='';
            $prefix = '';  
          if(!empty($locIdArray)){
              foreach ($locIdArray as $v)
               {
                $location=DB::table('locations')->where('id',$v)->first();
                  $loc.= $prefix.$location->location;
                  $prefix = ",";
                }
          }
          else{

                $location=DB::table('locations')->where('id',$projectList->location_id)->first();
           $loc=$location->location;  
        }
            echo $loc;
    }

///get amnities for project id ad $pId////////////////////
 public static function  getAmenities($pId){
   $pro_amenities = DB::table('properties')->select('property_amenities')->where('project_id',$pId)->first();
   $amenities = array();
/////////////////get amenities/////////////////////////////////////////
      if($pro_amenities !=''){
       $amenity = explode(',',$pro_amenities->property_amenities);
         if(!empty($amenity)){
            foreach($amenity as $ameVal => $val){
              if($ameVal < 6){
                $amenityArr = DB::table('amenities')->where('id',$val)->first();
                if(!empty($amenityArr)){
                  $amenities[] = $amenityArr->name;
                }
              }
            }
          }
      }
///////////////////////////end amenities//////////////////////////
       return $amenities;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchData(Request $request){
       //echo $request['type'];die;
        $projectsList=DB::table('projects')
                   ->leftjoin('locations', 'locations.id', '=', 'projects.location_id')
            ->leftjoin('categories','categories.id','=','projects.category_id')
            ->select('projects.is_active AS status', 'projects.name As project_name','projects.project_image','projects.id','locations.location','projects.description','projects.location_id','projects.min_price','projects.phase','projects.possasion_date','projects.project_type','categories.category','projects.created_at','projects.size','projects.logo','projects.locality','projects.url','projects.project_feature')
             // ->take(10)
              ->where('projects.is_active','=','yes');
              if(isset($request['location']) && $request['location']!=''){
                $projectsList= $projectsList
                               ->whereRaw("find_in_set('".$request['location']."',projects.location_id)");
                      
              }if(isset($request['category']) && $request['category']!=''){
                $projectsList= $projectsList
                               ->where('projects.category_id' ,'=', $request['category']);
              }if(isset($request['type']) && $request['type']!=''){
                $projectsList= $projectsList
                              ->where('projects.project_type' ,'=', $request['type']);
              }if(isset($request['phase']) && $request['phase']!=''){
                //echo $request['phase']; die();
                $projectsList= $projectsList
                             ->where('projects.phase' ,'=', $request['phase']);
              }if(isset($request['project']) && $request['project']!=''){
                //echo $request['phase']; die();
                $projectsList= $projectsList
                             ->where('projects.id' ,'=', $request['project']);
              }if(isset($request['builder']) && $request['builder']!=''){
                //echo $request['phase']; die();
                $projectsList= $projectsList
                             ->where('projects.builder_id' ,'=', $request['builder']);
              }if(isset($request['locality']) && $request['locality']!=''){
                 $locality =explode(",",$request['locality']);
               
                 $projectsList= $projectsList
                                ->whereIn('projects.locality',$locality);
               }if(isset($request['bedroom']) && $request['bedroom']!=''){
                $bedroom =explode(",",$request['bedroom']);
                $projectsList= $projectsList
                              ->whereIn('projects.bed',$bedroom);
               }if(isset($request['amount']) && $request['amount']!=''){
                $InrAmount =explode("-",$request['amount']);
                $InrAmount1 = explode(" ", $InrAmount[0]);
                $InrAmount2 = explode(" ", $InrAmount[1]);
                //print_r($InrAmount2);
                 $minAmount = $InrAmount1[1];
                 $maxAmount = $InrAmount2[2];
                $projectsList= $projectsList
                             ->where('projects.min_price', '>',$minAmount )
                               ->where('projects.max_price', '<',$maxAmount )
                               ->orderBy('projects.min_price', 'Asc');
              
               }if(isset($request['area']) && $request['area']!=''){
                $area =explode("-",$request['area']);
                  $min_area = $area[0];
                 $max_area = $area[1];
                 //print_r($area);die;
                $projectsList= $projectsList
                              ->whereBetween('projects.size', array($min_area,$max_area));
                            
               }
                $projectsList= $projectsList->get();

           
       
        return view('layouts.search-projects',compact('projectsList'));


  }

}
