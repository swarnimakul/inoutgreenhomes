<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Builder extends Model
{
    public static function BuildersFromState($stateId) {
    	$builders = \DB::table('builders')
    		->join('builder_state', 'builders.id', '=', 'builder_state.builder_id')
    		->select('builders.id', 'builders.name')
    		->where('builder_state.state_id', filter_var($stateId, FILTER_SANITIZE_NUMBER_INT))
    		->get();

    	return $builders;
    }
}
