<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\UserQuery;
class UserQueriesExport implements FromCollection
{
    public function collection()
    {
        return UserQuery::select('name','email','phone','query','created_at')->get();
    }
}