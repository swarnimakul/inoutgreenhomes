@include('layouts.master')
<style type="text/css">/* Outer */
.popup {
width:100%;
height:100%;
display:none;
position:fixed;
top:0px;
left:0px;
background:rgba(0,0,0,0.75);
z-index:1031;
}
/* Inner */
.popup-inner {
max-width:700px;
width:90%;
padding:40px;
position:absolute;
top:50%;
left:50%;
-webkit-transform:translate(-50%, -50%);
transform:translate(-50%, -50%);
box-shadow:0px 2px 6px rgba(0,0,0,1);
border-radius:3px;
background:#fff;
z-index:1031;
}
/* Close Button */
.popup-close {
width:30px;
height:30px;
padding-top:4px;
display:inline-block;
position:absolute;
top:0px;
right:0px;
transition:ease 0.25s all;
-webkit-transform:translate(50%, -50%);
transform:translate(50%, -50%);
border-radius:1000px;
background:rgba(0,0,0,0.8);
font-family:Arial, Sans-Serif;
font-size:20px;
text-align:center;
line-height:100%;
color:#fff;
}
.popup-close:hover {
-webkit-transform:translate(50%, -50%) rotate(180deg);
transform:translate(50%, -50%) rotate(180deg);
background:rgba(0,0,0,1);
text-decoration:none;
}
    /* Outer */
.popup {
width:100%;
height:100%;
display:none;
position:fixed;
top:0px;
left:0px;
background:rgba(0,0,0,0.75);
z-index:1031;
}
/* Inner */
.popup-inner {
max-width:700px;
width:90%;
padding:40px;
position:absolute;
top:50%;
left:50%;
-webkit-transform:translate(-50%, -50%);
transform:translate(-50%, -50%);
box-shadow:0px 2px 6px rgba(0,0,0,1);
border-radius:3px;
background:#fff;
z-index:1031;
}
/* Close Button */
.popup-close {
width:30px;
height:30px;
padding-top:4px;
display:inline-block;
position:absolute;
top:0px;
right:0px;
transition:ease 0.25s all;
-webkit-transform:translate(50%, -50%);
transform:translate(50%, -50%);
border-radius:1000px;
background:rgba(0,0,0,0.8);
font-family:Arial, Sans-Serif;
font-size:20px;
text-align:center;
line-height:100%;
color:#fff;
}
.popup-close:hover {
-webkit-transform:translate(50%, -50%) rotate(180deg);
transform:translate(50%, -50%) rotate(180deg);
background:rgba(0,0,0,1);
text-decoration:none;
}
</style>
@include('layouts.header')
<section class="seacrh-content mar-top100 container-fluid">
  <div class="container project-content">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 text-center com-marg"> <img src="{{asset('public/upload/builder_logo/')}}/{{$builder->logo}}" class="img-thumbnail"/> </div>
          <div class="divider"></div>
          
          <div class="clearfix">&nbsp;</div>
          @include('layouts.left-contact-form')
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-9">
        @if(session('info')=='fail')
                         <div class="alert alert-danger">Message Not Send</div>
                         @endif
                         @if(session('info')=='success')
                          <div class="alert alert-success">Thank you for your Interest. We will contact you Soon.</div>
                         @endif
        <h3 class="mar-top0">{{$builder->name}}</h3>
        
        <p>{!!$builder->description!!}</p>
       <div class="divider"></div>
       @if(!empty($builder_location))
       @foreach($builder_location as $locations)

        <h4 class="project-head">{{$builder->name}} in {{$locations->location}}</h4>   
        <div class="clearfix">&nbsp;</div>
        @php $projectsList = BuilderController::getProject($builder->id,$locations->id); 
         
        @endphp
        @if(sizeof($projectsList) == 0)
            <div class="project-cont" style="">
                <div class="srpWrap">
                    <div class="col-md-3 col-sm-4 col-xs-12 com-marg">
                    <div class="row">
                        <h2>Coming Soon...</h2>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!empty($projectsList))
        @foreach($projectsList as $projects)
        <div class="project-cont">
          <div class="srpWrap">
            <div class="col-md-3 col-sm-4 col-xs-12 com-marg">
              <div class="row">
                <div class="project-pic"> <img src="{{url('/public/upload/project_image'.'/'.$projects->project_image)}}" alt=""/>
                  <div class="project-title">{{$projects->name}}</div>
                </div>
              </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
              <div class="row">
                <h4 class="pro-title"><a href="{{ URL::to($projects->url) }}/">{{$projects->name}}</a><span class="fr font13">From <i class="fa fa-rupee"></i> {{$projects->min_price}}  to <i class="fa fa-rupee"></i> {{$projects->max_price}}</span></h4>
                <p>{{$projects->locality}}</p>
                <p>{!!substr($projects->description,0,200)!!}...</p>
                <div class="col-md-12 col-sm-12 col-xs-12"> <a href="#" data-popup-open="popup-{{$projects->id}}" class="contact_but mar-top0 pull-right">Contact Us</a> 
                        
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="popup" data-popup="popup-{{$projects->id}}">
                                            <div class="popup-inner">
                                                    @include('layouts.left-contact-form')
                                            <p><a data-popup-close="popup-{{$projects->id}}" href="#">Close</a></p>
                                            <a class="popup-close" data-popup-close="popup-{{$projects->id}}" href="#">x</a>
                                           </div>
                                        </div>
        @endforeach
        @endif

        
        
        @endforeach
        @endif
 
      </div>
    </div>
  </div>
  
  <div class="clearfix com-marg"></div>               
</section>
 


@include('layouts.footer')
