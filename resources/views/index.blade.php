@include('layouts.master')
<style type="text/css">
p{margin:0 0 10px ;}
.rc-anchor-normal .rc-anchor-content { width: 85px !important;}
#contact_form_float.floating-form {max-width: 295px !important;top:0px !important;}
#contact_form_float.floating-form label {margin: 5px -5px 5px -5px !important;}
#contact_form_float .floating-form-heading {margin-bottom: 5px !important; margin-top:-5px !important; font-size:15px !important;}
#contact_form_float.floating-form {
    background: #fff!important;
}
button, input[type=button], input[type=reset], input[type=submit], select {margin-bottom: 5px;width: 100%;}
</style>
<link href="{{asset('public/css/css-floating-form.css')}}" rel="stylesheet" media='all'/>
@include('layouts.floating-contact-form')
 
 <style>
.typeahead{
 top: 257px; left: 257.5px; display: block;width:560px;
}
.lat_nws p{
    text-align:justify;
}

</style>
<header>
 <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand logo" href="{{ URL::to('/') }}"><img src="{{asset('public/images/logo.png')}}" alt=""/></a> </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="{{ URL::to('/') }}/">Home</a></li>
              <li><a href="{{ URL::to('/about-us/')}}/">About Us</a></li>
              <li class="dropdown menu-list">
               <a href="javascript:void(0)" class="dropbtn">In Out Green</a>
               <div class="drop-content">
                <a  href="{{ URL::to('/inout-green-homes/') }}/">IOG Homes</a>
                <a href="{{ URL::to('/inout-green-interiors/') }}/">IOG Interiors</a>
                </div> 
              </li>  
              <li><a href="{{ URL::to('/green-consultants/')}}/">Green Consultants</a></li>
              <li><a href="{{ URL::to('/blogs/') }}/">Blogs</a></li>
              <li><a href="{{ URL::to('/contact-us') }}/">Contact Us</a></li>
              
            </ul>
          </div>
          <!--/.nav-collapse --> 
        </nav>
      </div>
    </div>
  </div>
</header> 
<section class="banner"> <img src="{{URL::to('')}}/public/upload/header/{{$banner}}" width="100%" alt=""/>
  <div class="overlay">
    <div class="container">
      <div class="row search-box">
        <h2>Find Your Green Home</h2>
        <div class="tab-box">
          <ul class="nav nav-tabs nav-fill" role="tablist">
            <li id="residential"><a href="#" onclick="searchByCat(2,'Residential')"  class="">Residential</a></li>
            <li id="Commercial"><a href="#" onclick="searchByCat(1,'Commercial')"  class="">Commercial</a></li>
            </ul>
          <ul  class="tabs-content-wrap res-table clearfix mrg-top10">
              <input placeholder="Enter a location, builder or Project" name="search_text" value="" type="text" id="search_text" class="search">
              <div class="clearfix"></div>
              <div class="city-link"> 
                <a href="#" onclick="searchByLoc(7,'Delhi')">Delhi</a>
                <a href="#" onclick="searchByLoc(2,'Noida')">Noida</a>
                <a href="#" onclick="searchByLoc(4,'Gurugram')" class="hidden-xs">Gurugram</a> 
                <a href="#" onclick="searchByLoc(25,'Bangalore')">Bangalore</a>
                <a href="#" onclick="searchByLoc(21,'Jaipur')" class="hidden-xs">Jaipur</a>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-xs dropdown-toggle city-link-dropdown-menu" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span> </button>
                  <ul class="dropdown-menu pull-right drop-menu" role="menu">
                    <li><a href="#" onclick="searchByLoc(28,'Faridabad')">Faridabad</a></li>
                    <li><a href="#" onclick="searchByLoc(6,'Pune')">Pune</a></li>
                    <li><a href="#" onclick="searchByLoc(29,'B')">Mumbai</a></li>
                    <li><a href="#" onclick="searchByLoc(26,'Chennai')">Chennai</a></li>
                    <li><a href="#" onclick="searchByLoc(6,'Hyderabad')">Hyderabad</a></li>
                  </ul>
                </div>
              </div>
          </ul>
      </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <section class="features">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 ml50">
          <div class="homes"> <img src="{{asset('public/images/icon1.png')}}" alt=""/> <span>{!!$worth_home!!}<br>
            </span> </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
          <div class="homes"> <img src="{{asset('public/images/icon2.png')}}" alt=""/> <span>{!!$happy_customer!!}<br>
           </span> </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
          <div class="homes"> <img src="{{asset('public/images/icon3.png')}}" alt=""/> <span>{!!$relationship_manager!!}
            </span> </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
          <div class="homes"> <img src="{{asset('public/images/icon4.png')}}" alt=""/> <span>{!!$active_listing!!}
            </span> </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="rera_regis">
          <h1>RERA Registration</h1>
          <p>{{$rera_registration_text}}</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="feat_pro" rel="Residential">
          <h2 id="location">Featured Projects</h2>
          <p>{{$featured_text}} 
            <div id="demo">
            <div id="owl-demo6" class="owl-carousel">
             @foreach($projects as $project)
             <div class="item">
                <div class="iten-cont"> <img src="{{asset('public/upload/project_image/')}}/{{$project->project_image}}" alt=""/>
                  <div class="item-cont-top">
                    <div class="row">
                      <div class="col-md-7 col-sm-6 col-xs-12">
                        <h3 class="head-title">{{$project->name}}</h3>
                        <p><i class="fa-home fa"></i> {{$project->project_feature}}</p>
                      </div>
                      <div class="col-md-5 col-sm-6 col-xs-12">
                        <h4 class="price">@if(!empty($project->min_price))
                          {{$project->min_price}}
                          @else
                          0
                          @endif  </h4>
                      </div>
                    </div>
                  </div>
                  <div class="item-cont-link"> <a href="{{ URL::to($project->url) }}/"> More Details</a></div>
                  <div class="item-cont-bottom">
                    <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-6 com-marg font10 no-rpad"><i class="fa fa-area-chart"></i>
                      @if(!empty($project->size))
                        {{$project->size}}
                      @else
                      -
                      @endif</div>
                      <div class="col-md-3 col-sm-6 col-xs-6 com-marg font10"><i class="fa-bed fa"></i>@if(!empty($project->bed)) 
                        {{$project->bed}}
                      @else
                      0
                      @endif
                      </div>
                      <div class="col-md-2 no-lpad col-sm-6 com-marg col-xs-6 font10"><i class="fa-bathtub fa"></i> @if(!empty($project->bath))
                        {{$project->bath}}
                      @else
                      0
                      @endif
                      </div>
                      <div class="col-md-3 col-sm-6 col-xs-6 com-marg font10"><i class="fa-car fa"></i> @if(!empty($project->parking) && $project->parking!= 'no')
                      {{$project->parking}} 
                      @else
                      0
                      @endif</div>
                    </div>
                  </div>
                  <div class="item-cont-overlay"></div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sch_tab">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="Carousel" class="carousel slide">
          {!!$govtHousing !!} 
</section>
<section class="home_cat">
   {!!$affHomeContent!!}
</section>

<section class="top_build">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 top_build2">
        <h3>Top Builders</h3>
        
      </div>
    </div>
    <div class="row">
      <div class="tabs_wrapper">
        <div class="col-md-3 col-sm-12 col-lg-3">
          <ul class="tabs">
             @foreach($builders as $key=>$builder)
            @if($key==0)
            <li class="active" rel="tab{{$builder->id}}">{{$builder->name}}</li>
            @else
            <li  rel="tab{{$builder->id}}">{{$builder->name}}</li>
           @endif
            @endforeach
            <!--<li rel="tab4">TATA Housing</li> <li rel="tab6">Viridian Group</li> <li rel="tab8">Delhi Heights</li>--> 
          </ul>
        </div>
        <div class="col-md-9 col-sm-12 col-lg-9">
          <div class="tab_container">
               @foreach($builders as $key=>$builder)
           @if($key==0)
            <h3 class="d_active tab_drawer_heading" rel="tab{{$builder->id}}">{{$builder->name}}</h3>
             @else
             <h3 class="tab_drawer_heading" rel="tab{{$builder->id}}">{{$builder->name}}</h3>
            @endif
            <div id="tab{{$builder->id}}" class="tab_content">
              <h4 class="hide">{{$builder->name}}</h4>
              <div class="row">
                     <div id="" class="owl-demo5 owl-carousel">
                       @foreach($builderProject as $projects)
                           @if($builder->id==$projects->id)
                         <div class="item project_detail">
                           
                             <a href="{{ URL::to($projects->url) }}/">
                                @if($projects->project_image!='')
                                 <img class="img-responsive thumbnail h270" src="{{URL::to('')}}/public/upload/project_image/{{$projects->project_image}}" width="397" height="250"  alt=""/>
                                 @else
                                 <img width="397" height="250"   src="{{URL::to('')}}/public/upload/project_image/no-image.png"   alt=""/>
                                  @endif
                                 <div class="project_detail-inner">
                                     <h3>{{$projects->name}}</h3>
                                     <p>{{$projects->location}}</p>
                                     <div class="clearfix"></div>
                                      <button class="price-btn"> @if(!empty($projects->min_price))<i class="fa fa-rupee"></i>  {{$projects->min_price}} 
                                       @endif
                       @if(!empty($projects->max_price)) - <i class="fa fa-rupee"></i> {{$projects->max_price}}
                        @endif
                        @if(empty($projects->max_price)&&empty($projects->min_price))
                          Not Available
                         @endif  </button >
                                 </div>
                             </a>
                             
                         </div>
                         @endif
                         @endforeach
                                             
                        
                     </div>
                </div> 
            </div>
            @endforeach
         
          </div>
        </div>
          
        <!-- .tab_container -->
      </div>
    </div>
  
</section>
<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> <img src="{{asset('public/images/about.png')}}" class="about_img" alt=""/> </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> <span>About</span>
        <h3 class="h3about">In Out Green Homes</h3>
        {!!$content!!}<div class="read_more"><a href="{{ URL::to('/about-us') }}">Read More <img src="{{asset('public/images/arrow.png')}}" alt=""/></a></div>
      </div>
    </div>
  </div>
</section>
<section class="value_add">
  <div class="val_add">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h3>Value Added Services</h3>
          <ul>
            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Real Estate Advisor</span></a></li>

            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/2.png')}}" alt=""/></p>
              <span class="hidden-xs">Property Management</span></a></li>

            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/3.png')}}" alt=""/></p>
              <span class="hidden-xs">Property  <br>Leasing</span></a></li>
              
            <li class="v1 m1"><a href=""> 
              <p><img src="{{asset('public/images/6.png')}}" alt=""/></p>
              <span class="hidden-xs">Warehouses</span></a></li>
            
            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/4.png')}}" alt=""/></p>
              <span class="hidden-xs">Drone Videos</span></a></li>

            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/10.png')}}" alt=""/></p>
              <span class="hidden-xs">InOut GREEN Interiors</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="mobile-value_add">
  <div class="mobile-val_add">
    <div class="container">
      <div class="row">
        <h3>Value Added Services</h3>
        <ul>
          <li class=""><a href=""> <img src="{{asset('public/images/1.png')}}" alt=""/> <span class="title">Real Estate Advisor</span></a></li>
          <li class=""><a href=""> <img src="{{asset('public/images/2.png')}}" alt=""/> <span class="title">Property Management</span></a></li>
          <li class=" "><a href=""> <img src="{{asset('public/images/3.png')}}" alt=""/> <span class="title">Property Leasing</span></a></li>
          <li class=""><a href=""> <img src="{{asset('public/images/6.png')}}" alt=""/> <span class="title">Warehouses</span></a></li>
          <li class=""><a href=""> <img src="{{asset('public/images/4.png')}}" alt=""/> <span class="title">Drone Videos</span></a></li>
          <li class=" "><a href=""> <img src="{{asset('public/images/10.png')}}" alt=""/> <span class="title">In Out Green Interiors</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="nws_tab">
<div class="container">
<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
    <h3>Latest Real Estate News</h3>
    @foreach($l_cat_post as $latestCatPosts)
    <div class="lat_nws">
      <h5><a href="{{ URL::to('/blogs/') }}/{{$latestCatPosts->id}}/">{!!substr(strip_tags($latestCatPosts->title),0,45)!!}... </a></h5>
     <span>{{$latestCatPosts->created_at}}</span>
      <div class="nws_cont">
        <div class="nws_img">
          @if($latestCatPosts->img != '')
          <img src="{{$latestCatPosts->img()}}" style="width:61px; height: 49px;" />
           @else
               <img src="{{asset('public/images/no-image.png')}}"  style="width: 61px; height: 49px; "alt=""/>
          @endif
        </div>
        <p>{!!substr(strip_tags($latestCatPosts->description),0,110)!!}... <a href="{{ URL::to('/blogs/') }}/{{$latestCatPosts->id}}/">Read More</a></p>
      </div>
    </div>
    @endforeach
   
   
  </div>
  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 com-marg">
    <h3>Trending News</h3>
    @foreach($trendingPosts as $trendingCatPosts)
    <div class="lat_nws">
      <h5><a href="{{ URL::to('/blogs/') }}/{{$trendingCatPosts->id}}/"> {{$trendingCatPosts->title}}</a></h5>
      <span>{{$trendingCatPosts->created_at}}</span>
      <div class="nws_cont">
        <div class="nws_img">
          @if($trendingCatPosts->img !='')
          <img src="{{$trendingCatPosts->img}}" style="width:61px; height: 49px;" />
          @else
               <img src="{{asset('public/images/no-image.png')}}"  style="width: 61px; height: 49px; "alt=""/>
          @endif
        </div>
        <p>{!!substr(strip_tags($trendingCatPosts->description),0,100)!!}...<a href="{{ URL::to('/blogs/') }}/{{$trendingCatPosts->id}}/">Read More</a></p>
      </div>
    </div>
  @endforeach
 </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div class="t_soc"> <a class="twitter-timeline" href="https://twitter.com/">Tweets by IOG Homes</a>
      <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
     <div class="f_soc">
      <div class="fb-page" data-href="https://www.facebook.com/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/">Facebook</a></blockquote>
      </div>
    </div>
    <div class="o_soc">
      <iframe src="https://www.youtube.com" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    
      </div>

  </div>
</div>
</section>
<section  class="num_tab num">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"> <span>Talk to Us. We hear you patiently.</span> <a href="tel:+91 {{$address->phone}}">(+91) {{$address->phone}}</a> </div>
    </div>
  </div>
</section>
<footer>
  <div class="footer1">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Top Cities Project</h4>
          <ul>
          <li><a href="{{ URL::to('/new-delhi/') }}/">New Delhi</a></li>
    <li><a href="{{ URL::to('/noida/') }}/">Noida</a></li>
    <li><a href="{{ URL::to('/greater-noida/') }}/">Greater Noida</a></li>
    <li><a href="{{ URL::to('/gurugram/') }}/">Gurugram</a></li>
    <li><a href="{{ URL::to('/bangalore/') }}/">Bangalore</a></li>
    <li><a href="{{ URL::to('/pune/') }}/">Pune</a></li>
    <li><a href="{{ URL::to('/mumbai/') }}/">Mumbai</a></li>

          </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Quick Links</h4>
          <ul>
      <li><a href="{{ URL::to('/') }}/">Home</a></li>
    <li><a href="{{ URL::to('/about-us/') }}/">About Us</a></li>
    <li><a href="https://www.inoutgreen.com/">In Out Green Interiors</a></li>
    <li><a href="{{ URL::to('/green-building-consultants/') }}/">Green Building Consultants</a></li>
    <li><a href="http://www.inoutgreenhomes.com/blogs/">Blogs</a></li>
    <li><a href="{{ URL::to('/contact-us/') }}/">Contact us</a></li>
    <li><a href="{{ URL::to('/we-are-hiring/') }}/">We Are Hiring</a></li>
</ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft ">
          <h4>Recent Posts</h4>
          @foreach($l_cat_post as $newposts)
          <p>
            @if($newposts->img != '')
            <img src="{{$newposts->img}}"  style="width: 60px; height: 49px; "alt=""/>
            @else
               <img src="{{asset('public/images/no-image.png')}}"  style="width: 60px; height: 49px; "alt=""/>
            @endif
             <a href="{{ URL::to('/blogs/') }}/{{$newposts->id}}/">{!!substr($newposts->description,0,50)!!}</a></p>
          @endforeach
         
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Contact Us</h4>
            <p>
   <i class="fa fa-map-marker"></i> <span>Delhi Office:<br/>
            {{substr($address->office_address,0,32)}}<br/>
            {{substr($address->office_address,33)}} </span>
     
       <br><i class="fa fa-map-marker"></i>  <span> Noida Office:<br/>
           {{substr($address->address,0,32)}}<br/>
            {{substr($address->address,33)}}</span>  </p>
          <p><i class="fa fa-phone"></i> <a href="tel:+91 {{$address->phone}}">+91 {{$address->phone}}</a></p>
          <p><i class="fa fa-envelope"></i> <a href="mailto:{{$address->email}}">{{$address->email}}</a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="footer2">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ft"> <span>Copyright &copy; 2020 
          <a href="{{ URL::to('/') }}">In Out Green Homes</a>. All Rights Reserved</span> </div>
       <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ft">
<ul>
    <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
    <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
    <li><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
    <li><a href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
    <li><a href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
    
</ul>
</div>
      </div>
    </div>
  </div>
</footer>
<script src="{{asset('public/js/jquery.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/jquery-ui.js')}}"></script>
<script src="{{asset('public/js/bootstrap-select.js')}}"></script>
<script src="{{asset('public/js/typeahead.js')}}"></script>  


<!-- Import Owl carosel js-->
<script src="{{asset('public/js/owl.carousel.js')}}"></script>
<!-- logo slider-->
<script>
$(document).ready(function() {
    @if(session('info') != '')
      $("#contact_results").show();
      $("#contact_body").hide();
    @endif 
  //alert(screen.width);
/*if (screen.width <= 2000) {*/
  //$("#contact_form_float").hide();
  //$(".contact-opener").show();
    var _scroll = true, _timer = false, _floatbox = $("#contact_form_float"), _floatbox_opener = $(".contact-opener") ;
   
   @if(session('info') == '')
   _floatbox.css("left", "-296px"); //initial contact form position
   @endif   
    //Contact form Opener button
    _floatbox_opener.click(function(){
       if (_floatbox.hasClass('visiable')){
            _floatbox.animate({"left":"-296px"}, {duration: 300}).removeClass('visiable');
            
             $("#contact_body").show();
              $("#contact_results").hide();
        }else{
           _floatbox.animate({"left":"0px"},  {duration: 300}).addClass('visiable');

        }
    
    });
    $('#Carousel').carousel({
        interval: 5000
    })    
});
</script>

<!-- logo slider -->
<!-- facebook -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!-- facebook -->

<script type="text/javascript">
  $(document).ready(function () {

    $("#residential").click(function() {
    $('html, body').animate({
        scrollTop: $("#location").offset().top
    }, 2000);
    });
    $("#Commercial").click(function() {
    $('html, body').animate({
        scrollTop: $("#location").offset().top
    }, 2000);
    });
    $("#Delhi-Awas-Yojna").click(function() {
    $('html, body').animate({
        scrollTop: $("#location").offset().top
    }, 2000);
    });

    $('#search_text').typeahead({
            source: function (query, result) {
                $.ajax({
                    url: "{{url('/')}}/autocomplete/"+query,
                    dataType: "json",
                    type: "get",
                    success: function (data) {
                        result($.map(data, function (item) {
                           return item;
                        }));
                    }
                });
            },
            select: function () {
               var searchVal = this.$menu.find('.active').data('value');
               var builderurl='';
               var valArray = searchVal.split(': ');
               $('#search_text').val(valArray[1]);
               var keyword = valArray[1];
                   //keyword = keyword.replace(/\s/g , "-");
                  // keyword = keyword.toLowerCase();
               /*if(valArray[0] == 'Builder') {
                var builderurl = "{{url('/')}}/builder/";
               }*/
                //if(valArray[0] == 'Project') {
                var builderurl = "{{url('/')}}/";
               //}
               //keyword=keyword.toUpperCase();
               //alert(keyword);
                $.ajax({
         
     url:"{{url('/')}}/autocomplete-get-url/"+keyword,
     type: 'get',
     data: {name:keyword},
     dataType: 'json',
     success:function(response){
      
     // var len = response.length;
      //     console.log(response['url']);

      if(response['url']!=''){
            var projects_url = response['url'];
            url=builderurl+projects_url+"/";
             window.location = url;
      
      }
 
     }

    });  
                
    return false;          
                
              
            }
        });

    });
  
    </script>
<script >
function searchByLoc(id,lName){
    var imgId  = imgId;
     var Url = "{{url('/')}}/searchByLoc/"+id;
     $.ajax({
        url: Url,
        type: "get",
        success: function(data){
         $("#demo").html(data);
         $("#location").html(lName+ " Projects");
      
         }
       });
}

function searchByCat(id,catName){
     
    // var imgId  = imgId;
     var Url = "{{url('/')}}/searchByCat/"+id;
     
      $.ajax({
        url: Url,
        type: "get",
        beforeSend: function()
        {
                $("#demo").html("<img src='{{asset('public/images/loading.gif')}}''>");
        },
        success: function(data){
          
          $("#demo").html(data);
          $("#location").html(catName+ " Projects");
     
        }
      });     
  }


</script>
<!-- builder -->
<script>
 // tabbed content
    http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
    
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();    
    
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
    
    /*$(".tabs").css("margin-top", function(){ 
       return ($(".tab_container").outerHeight() - $(".tabs").outerHeight() ) / 2;
    });*/
    });
    $(".tab_container").css("min-height", function(){ 
      return $(".tabs").outerHeight() + 50;
    });
  /* if in drawer mode */
  $(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
    
    $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
    
    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
  
  
  /* Extra class "tab_last" 
     to add border to bottom side
     of last tab 
  $('ul.tabs li').last().addClass("tab_last");*/
  </script>
<script>
            function switch_tabs($obj){
            //$('.tabs-content-wrap').hide();
            $('.nav-tabs a').removeClass('active');
            var id = $obj.attr('href');
            $(id).show().css('display', 'block');
            $obj.addClass('active');
            }
            $('.nav-tabs a').click(function(){
            switch_tabs($(this));
            return false;
            });
            switch_tabs($('.nav-tabs a.active'));
            </script>
<!-- builder /-->
        <script>
            $(document).ready(function() {
              $("#owl-demo6").owlCarousel({
                navigation: true,
                navigationText: [
                     "<i class='fa fa-chevron-left icon-grey'></i>",
                     "<i class='fa fa-chevron-right icon-grey'></i>"
                ],
              autoPlay: 8000,
              items : 3,
              itemsDesktop : [1199,3],
              itemsDesktopSmall : [979,3]
              });
              $(".owl-pagination").remove();

              $(".owl-demo5").owlCarousel({
                navigation: true,
                navigationText: [
                     "<i class='fa fa-chevron-left icon-grey'></i>",
                     "<i class='fa fa-chevron-right icon-grey'></i>"
                ],
              autoPlay: 8000,
              items : 2,
              itemsDesktop : [1199,2],
              itemsDesktopSmall : [979,1]
              });
              $(".owl-pagination").remove();
            });
    </script>
     <script>
$(document).ready(function(){
    $("#button").click(function(){
        $(".disclamer").slideUp(function(){
           $("#show").css("display","block");
           $("#show").show(1000);
        });
       
        
    });
    $("#show").click(function(){
        $(".disclamer").slideDown();
         $("#show").css("display","none");
    });
});
</script>
   <!-- disclaimer -->  
<a id="show" class="" style="display: none">
    <i class="  fa fa-chevron-circle-up fa-ange-b"></i> 
  </a>
 <div class="alert col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-xs disclamer">
 
    <div class="icon-wrap">
 <i class="fa fa-info-circle"></i></div>
  <span id="button" class=" close-dis">
    <i class="fa fa-chevron-circle-down" style="color:#999999;
    font-size: 46px;"></i> 
  </span>
<!-- <span class=" close-dis" onclick="this.parentElement.style.display='none';">
    <span aria-hidden="true" style="font-size: 32px;">&times;</span>
 </span>-->
  <div class="d-ib">  </div>  
</div> 
<!-- close -->     
</body>
</html>
