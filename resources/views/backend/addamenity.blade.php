@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />

<!--Header-part to commit-->

  @include('backend.layouts.header')
  

<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="{{ URL::to('backend/projects').'/' }}" class="tip-bottom">Projects</a> <a href="{{ URL::to('backend/amenities') .'/'}}" class="tip-bottom">Amenities</a> <a href="add-category" class="current">{{!empty($amenity)?'Update Amenity':'Add Amenity'}}</a> </div>
  <h1></h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{!empty($amenity)?'Update Amenity':'Add Amenity'}}</h5>
        </div>
       @if(!empty($message))
        @if($message=='fail')
        <div class="alert alert-danger">Amenity already Exists</div>
        @endif
         @if($message=='success')
        <div class="alert alert-success">Amenity added successfully!</div>
        @endif
      @endif
        <div class="widget-content nopadding">
             @include('backend.layouts.errors')
         <form  method="post" action="" enctype="multipart/form-data"  >
          {{ csrf_field() }}
          <div class="form-horizontal">
          <div class="control-group">
              <label class="control-label">Amenity:</label>
              <div class="controls">
               <input class="span4" type="text" name="amenity" value="{{!empty($amenity)?$amenity->name:''}}" requird >
              </div>
          </div>
          <div class="control-group">
              <label class="control-label">Image:</label>
              <div class="controls">
                <input type="file" name="image">
              
               @if(!empty($amenity) ) 
               <img src="{{asset('public/images/amenities/')}}/{{$amenity->image}}" />
               @endif
              </div>
          </div>
          <div class="form-actions">
             <label class="control-label"></label>
              <button type="submit" class="btn btn-success">{{!empty($amenity)?'Update':'Save'}}</button>
          </div>
          </div>
          </form>
        </div>
      </div>
</div>
 
</div>

</div></div></div>
<<!--Footer-part-->
@include('backend.layouts.footer')
