@include('backend.layouts.master')

<!--Header-part to commit-->

  @include('backend.layouts.header')
  <link rel="stylesheet" href="{{asset('public/backend/css/colorpicker.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/datepicker.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />

<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/pages').'/' }}" class="tip-bottom">Pages</a> <a href="" class="current">{{ ! empty($Blog->title) ? $Blog->title : 'Add New Blog' }}</a> </div>
  <h1>{{ ! empty($Blog->title) ? $Blog->title : 'Add Blog' }}</h1>
</div>
 @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif

<form  method="post" action="" >
  {{ csrf_field() }}
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{ ! empty($Blog->title) ? $Blog->title : 'Add New Blog' }}</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                  <input type="text" class="span11" name="title" value="{{ ! empty($Blog->title) ? $Blog->title : '' }}" required>
                </div>
              </div>
           
            <!-- og Type -->
              <div class="control-group">
              <label class="control-label">Description</label>
              <div class="controls">
                
                <textarea class="span11" name="og_type" value="" >{{ ! empty($Blog->description) ? $Blog->description : '' }}</textarea>
             
              </div>
            </div>
            <!-- og image -->
              <div class="control-group">
              <label class="control-label">Image </label>
              <div class="controls">
                
                <textarea class="span11" name="og_image" value="" >{{ ! empty($Blog->image) ? $Blog->image : '' }}</textarea>
             
              </div>
            </div>
         
       <!-- close -->   
          </div>
        </div>
      </div>

 
    
 
  </div>
  <div class="row-fluid">
     <div class="control-group">
              <label>
                  <div class="checker" ><span>
                    <div class="checker" ><span>
                      
                     
           <input type="checkbox" name="is_active" style="opacity: 0;"   {{! empty($Blog->is_active) ? (($Blog->is_active=='yes')?'checked':'') : '' }}>
                    </span></div>
                </span>
                </div>
                  Active/Deactive</label>
                <label>
             </label></div>
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="content" value="" >{{!empty($Blog->content) ? $Blog->content : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
  </div>
    <div class="form-actions">
              <button type="submit" class="btn btn-success">{{!empty($Blog)?'Update':'Save'}}</button>
            </div>
</div>




</form>
</div></div></div>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
<<!--Footer-part-->
@include('backend.layouts.footer')
