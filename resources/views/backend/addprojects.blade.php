
@include('backend.layouts.master')
<style type="text/css">
  .img{
    width: 100px;
    border: 1px solid green;
    border-radius: 5px;
  }

</style>
<script type="text/javascript">
  function addCat(){
    var newCat = $('#category').val();
    //alert (newCat);
     if(newCat == 'other_cat'){
      $("#othercatDiv").show();
        $("#otherCat").attr("placeholder", "Add new category").focus();
     }
     else{
      $("#othercatDiv").hide();
      
     }
    }
  </script>
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--Header-part-->

@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/projects').'/' }}" >Projects</a> <a href="{{ URL::to('backend/add-projects').'/' }}" class="current"> Add Project </a> </div>
  <h1>Add New Project  </h1>
</div>
<div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus-sign"></i> </span>
            <h5>Add</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"  >
                {{ csrf_field() }}
                @include('backend.layouts.errors')
                @if(isset($error) && $error != '')
                <div class="alert alert-danger" role="alert">
                  {{$error}}
                </div>
                 @endif
             
             @if($bid !='')
             <div class="control-group">
              <label class="control-label">Builder</label>
            
             <div class="controls">
                <input type="text" class="span4" name="b_name" value="{{$builderInfo[0]->name}}" readonly="">
                <input type="hidden" class="span4" name="b_id"  value="{{$builderInfo[0]->id}}" readonly="">
              </div>
             </div>
             @else
          <div class="control-group">
              <label class="control-label">Builder</label>
            
             <div class="controls">
                <select name="b_id"  class="span4" >
                    <option value="" selected>-Select Builder-</option>
                  @foreach($builders as $builder)
                 
                  <option value="{{$builder->id}}" >{{$builder->name}}</option>
       
                  @endforeach
                </select>
             </div>
           </div>
             
             @endif

             <div class="control-group" id="selectCat">
                 <label class="control-label">Location</label>
              <div class="controls">
                  @if($bid == '')
                  <select name="l_id[]" id="location"  class="span4" multiple="" >
                     <option value="" >-Select Location-</option>
                    @foreach($locations as $loc)
                       <option value="{{$loc->id}}" >{{$loc->location}}</option>
                    @endforeach
                   </select>
                  @else
                    <select name="l_id[]" id="location" "  class="span4"  multiple="">
                      <option value="">-Select Location-</option>
                     @foreach($locations as $key=>$val)
                         <option value="{{$val->id}}" selected >{{$val->location}}</option>
                      @endforeach
                    </select>
                   @endif
              </div>
              </div>


              <div class="control-group" id="selectCat">
                 <label class="control-label">Category</label>
              <div class="controls">
                <select name="category" class="span4" id="category"  >  <option value=" ">-Select Category-</option>
                  
                  @if(!empty($categories))
                  @foreach($categories as $cat)
                      @if(!empty($edit)&& $edit->category==$cat->category)
                   <option value="{{$edit->category_id}} " selected>{{$cat->category}}</option>
                   @endif
                    @if(!empty($edit)&& $edit->category!=$cat->category)
                  <option value="{{$cat->id}} ">{{$cat->category}}</option>
                 @endif
                   @if(empty($edit))
                  <option value="{{$cat->id}} ">{{$cat->category}}</option>
                  @endif
                  @endforeach
                  @endif
              
                    
                </select>
              </div>
              </div>
              
               
            <div class="control-group">
                 <label class="control-label">Project Name</label>
              <div class="controls">
                <input class="span4" type="text" name="name" value="" requird >
             </div>
            </div>
            <!-- url -->
             <div class="control-group">
              <label class="control-label">Url :</label>
              <div class="controls">
                <input type="text" name="url" required="" class="span4"  placeholder="Url(ex:project-name)" />
              </div>
            </div>
        <!-- close -->
         <!-- rera_id -->
             <div class="control-group">
              <label class="control-label">Rera Id :</label>
              <div class="controls">
                <input type="text" name="rera_id"  class="span4" placeholder="Rera Id" />
              </div>
            </div>
            <!-- close -->

            <!-- Minimum price -->
          <div class="control-group">
            <label class="control-label">Min. Price</label>
              <div class="controls">
                <input type="text" placeholder="Enter Minimum Price"  class="span4" name="min_price" />
              </div>
            </div> 

            <!-- close -->
              <!-- Maximum price -->
          <div class="control-group">
            <label class="control-label">Max. Price</label>
              <div class="controls">
                <input type="text" placeholder="Enter Maximum Price"  class="span4" name="max_price" />
              </div>
            </div> 

            <!-- close -->
            <!-- bed -->
            <div class="control-group">
              <label class="control-label">Bedroom</label>
              <div class="controls">
                <input type="text" placeholder="No. Of Bedrooms" name="bed"  value="" class=" span4">
                
              </div>
            </div>


            <!-- bed -->
            <!-- bath -->
           <div class="control-group">
              <label class="control-label">Bathroom</label>
              <div class="controls">
                <input type="text" placeholder="No. Of Bathrooms" name="bath"  value="" class=" span4">
                
              </div>
            </div>

            <!-- close -->
            <!-- size -->
            <div class="control-group">
              <label class="control-label">Size</label>
              <div class="controls">
                <input type="text" placeholder="Size " name="size"  value="" class=" span4">
                 
              </div>
            </div>
              <div class="control-group">
            <label class="control-label">Project feature</label>
              <div class="controls">
                 <input type="text" name="project_feature" placeholder="Ex: 2BHK 3Bhk"  class="span4" />
            
              </div>
            </div> 
            <!-- close -->
           <div class="control-group">
            <label class="control-label">Project type</label>
              <div class="controls">
                <select  name ="project_type[]" class="span4" multiple="">
                <option value="Apartment">Apartment</option>
                <option value="Residential Plot">Residential Plot</option>
                <option value="Independent Floor">Independent Floor</option>
                <option value="Villa">Villa</option>
                <option value="Studio">Studio</option>
                <option value="Row House">Row House</option>
                <option value="Independent House">Independent House</option>
                <option value="Farm House">Farm House</option>
                <option value="Town Suites">Town Suites</option>
                <option value="Township">Township</option>
                <option value="Bungalow">Bungalow</option>
                <option value="Retail Shop">Retail Shop</option>
                 <option value="Office Space">Office Space</option>
                 <option value="Plots">Plots</option>
                
            </select> 
              </div>
            </div> 
            <div class="control-group">
            <label class="control-label">Project Phase</label>
              <div class="controls">
                 <select name="phase" class="span4">
                  <option value="Launch">Launch</option>
                  <option value="Under Construction">Under Construction</option>
                  <option value="Ready to Move">Ready to Move</option>
                   <option value="Depend draw">Depend draw</option>
                 <option value="Upcoming Projects">Upcoming Projects</option>
                 <option value="Pre Launch">Pre Launch</option>
                 
                 </select>
              </div>
            </div> 
             <div class="control-group">
            <label class="control-label">Locality</label>
              <div class="controls">
                 <input type="text" name="locality"  class="span4" />
            
              </div>
            </div> 
              <!--  Parking -->
              <div class="control-group">
          <label class="control-label">Parking</label>
               <div class="controls">
                <input type="text" name="parking"  class="span4" />
                </div>
            </div> 
            <!-- close -->
             <!-- Logo -->
              <div class="control-group">
                <label class="control-label"> Project Logo</label>
                <div class="controls">
   <input type="file" id="image" value="" name="logo"  onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview" src=""
    >
                </div>
              </div>
              <!-- project logo -->

              <!-- projects image -->
                <div class="control-group">
                <label class="control-label"> Project Image</label>
                <div class="controls">
           <input type="file" id="image" value="" name="project_image"  onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])"/>
            <img class="img" id="preview2" src="">

                </div>
              </div>
              <!-- close -->

              
        
        <!-- featured open -->
            <div class="control-group">
              <label class="control-label">Featured</label>
                <div class="controls">
                <input type="checkbox" name="is_featured"  class="span11">
                </div>
            </div>
<!-- featured close -->
               <div class="widget-box">
      <div class="widget-title"> 
        <h5>Description</h5>
      </div>
      <div class="widget-content">
             <div class="control-group"  >
                <div class="">
                     <textarea cols="3" id="textarea" name="description" required=""></textarea>
               
                </div>
              </div>
            </div>
          </div>
            <div class="form-actions">
              <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Save</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
       </div>
      </div>
 

      
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    
<!--Footer-part-->
@include('backend.layouts.footer')
