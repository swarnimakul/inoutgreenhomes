@include('backend.layouts.master')

<!--Header-part to commit-->

@include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 
<!--sidebar-menu-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/pages').'/' }}" class="tip-bottom">Pages</a> <a href="" class="current">{{ ! empty($Page->title) ? $Page->title : 'Add Home Page' }}</a> </div>
  <h1>{{ ! empty($Page->title) ? $Page->title : 'Add Home Page' }}</h1>
</div>

 @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
<form  method="post" action="" enctype="multipart/form-data"  >
  {{ csrf_field() }}
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{ ! empty($Page->title) ? $Page->title : 'Add Home Page' }}</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Title</label>
                <div class="controls">
                  <input type="text" class="span11" name="title" value="{{ ! empty($Page->title) ? $Page->title : '' }}" required>
                </div>
              </div>
            <div class="control-group">
                <label class="control-label">Url</label>
                <div class="controls">
                  <input type="text" class="span11" value="{{ ! empty($Page->url) ? $Page->url : '' }}" name="url" required 
                  {{ ! empty($Page->url)?'readonly':''}}>
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">Worth Home Sold:</label>
              <div class="controls">
               <input type="text" class="span11" value="{{ ! empty($Page->worth_home) ? $Page->worth_home : '' }}" name="worth_home" >
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Happy Customers:</label>
              <div class="controls">
               <input type="text" class="span11" value="{{ ! empty($Page->happy_customer) ? $Page->happy_customer : '' }}" name="happy_customer" >
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Relationship Managers:</label>
              <div class="controls">
               <input type="text" class="span11" value="{{ ! empty($Page->relationship_manager) ? $Page->relationship_manager : '' }}" name="relationship_manager" >
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Active Listing:</label>
              <div class="controls">
               <input type="text" class="span11" value="{{ ! empty($Page->active_listing) ? $Page->active_listing : '' }}" name="active_listing" >
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Header Image:</label>
              <div class="controls">
               <input type="file" class="span11" value="" name="home_header_image" >
             </div>
         @if (isset($Page->home_header_image) && $Page->home_header_image !='')
              <div class="controls">
               <img src="{{asset('public/upload/header/')}}\{{$Page->home_header_image}}"  style="height: 100px; width: 300px;">
              </div>
              @endif
            </div>
            <div class="control-group">
              <label class="control-label">Meta Keywords :</label>
              <div class="controls">
                
                <textarea class="span11" name="meta_tag" value="" >{{ ! empty($Page->meta_tag) ? $Page->meta_tag : '' }}</textarea>
             
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Meta Description
:</label>
              <div class="controls">
               <textarea class="span11" name="meta_desc" value=""  >{{ ! empty($Page->meta_desc) ? $Page->meta_desc : '' }}</textarea>
              </div>
            </div>
                 <!-- OG Section -->

           <!-- og title -->
             <div class="control-group">
              <label class="control-label">OG Title </label>
              <div class="controls">
                
                <textarea class="span11" name="og_title" value="" >{{ ! empty($Page->og_title) ? $Page->og_title : '' }}</textarea>
             
              </div>
            </div>
            <!-- og site name -->
              <div class="control-group">
              <label class="control-label">OG Site Name</label>
              <div class="controls">
                
                <textarea class="span11" name="og_site_name" value="" >{{ ! empty($Page->og_site_name) ? $Page->og_site_name : '' }}</textarea>
             
              </div>
            </div>
            <!-- og Type -->
              <div class="control-group">
              <label class="control-label">OG Type </label>
              <div class="controls">
                
                <textarea class="span11" name="og_type" value="" >{{ ! empty($Page->og_type) ? $Page->og_type : '' }}</textarea>
             
              </div>
            </div>
            <!-- og image -->
              <div class="control-group">
              <label class="control-label">OG Image </label>
              <div class="controls">
                
                <textarea class="span11" name="og_image" value="" >{{ ! empty($Page->og_image) ? $Page->og_image : '' }}</textarea>
             
              </div>
            </div>
          <!-- og url -->
               <div class="control-group">
              <label class="control-label">OG Url </label>
              <div class="controls">
                
                <textarea class="span11" name="og_url" value="" >{{ ! empty($Page->og_url) ? $Page->og_url : '' }}</textarea>
             
              </div>
            </div>
       <!-- close -->  
            <!-- RERA Registration Text-->
            <div class="control-group">
              <label class="control-label">RERA Registration Text :</label>
              <div class="controls">
                
                <textarea class="span11" name="rera_registration_text" value="" >{{ ! empty($Page->rera_registration_text) ? $Page->rera_registration_text : '' }}</textarea>
             
              </div>
            </div>

            <!-- close -->
             <!-- Featured Text-->
            <div class="control-group">
              <label class="control-label">Featured Text :</label>
              <div class="controls">
                
                <textarea class="span11" name="featured_text" value="" >{{ ! empty($Page->featured_text) ? $Page->featured_text : '' }}</textarea>
             
              </div>
            </div>

            <!-- close -->
            <!-- Top builder Text-->
            <div class="control-group">
              <label class="control-label">Top Builder Text :</label>
              <div class="controls">
                
                <textarea class="span11" name="top_builder_text" value="" >{{ ! empty($Page->top_builder_text) ? $Page->top_builder_text : '' }}</textarea>
             
              </div>
            </div>

            <!-- close -->
           </div>
        </div>
      </div>
  </div>
     <div class="control-group">
          <label>
             <div class="checker" >
               <span>
                  <input type="checkbox" name="is_active" style="opacity: 0;"   {{! empty($Page->is_active) ? (($Page->is_active=='yes')?'checked':'') : '' }}>
               </span>
             </div>
             Active/Deactive
          </label>
      </div>
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="content" value="" >{{ ! empty($Page->content) ? $Page->content : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Affordable Home Content</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea1" name="affordable_home_content" value="" >{{ ! empty($Page->affordable_home_content) ? $Page->affordable_home_content : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Goverment Housing Scheme</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea2" name="goverment_housing_content" value="" >{{ ! empty($Page->goverment_housing_content) ? $Page->goverment_housing_content : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
    <div class="widget-box">
      <div class="widget-title"> 
        <h5>Value added services</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
                  <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea3" name="value_added_services" value="" >{{ ! empty($Page->value_added_services) ? $Page->value_added_services : '' }}</textarea>
            </div>
         
        </div>
      </div>
    </div>
  </div>
    <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
</div>




</form>
</div></div></div>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
<script>CKEDITOR.replace('textarea1');</script>

<script>CKEDITOR.replace('textarea2');</script>
<script>CKEDITOR.replace('textarea3');</script>
<<!--Footer-part-->
@include('backend.layouts.footer')
