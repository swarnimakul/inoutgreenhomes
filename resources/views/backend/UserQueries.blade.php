@include('backend.layouts.master')

<!--Header-part-->


@include('backend.layouts.header')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  
  
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/').'/user-queries' }}" class="current">Queries</a> </div>
    <h1>Queries List</h1>
  </div>
  
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
          
          </div>
          <div class="widget-content nopadding">
            <table id="userQueries" class="table table-bordered   with-check">
              <thead>
                 <tr>
              <td colspan="4">
                <button id='deleteall' class='btn btn-danger btn-mini tip' title='Delete'>Delete </button>
              
                <button id='active' class='btn btn-success btn-mini tip' title="Active">Active </button>
              
                <button id='deactive' class='btn btn-danger btn-mini tip' title="Deactive">Deactive </button>

                
              </td>

              <td style="border: 0" >
                 <a href="{{url('/')}}/backend/download-queries"><span class="label label-info" style="height: 20px;">Download Queries</span></a>
              </td>
            </tr>
                <tr>
                   <th><input type="checkbox" id="title-table-checkbox" name="title-table-checkbox" /></th>
                  <th>User Name</th>
                   <th>Query</th>
                   <th>Posted On</th>
                   <th>Action</th>
                </tr>
              </thead>
              <tbody>

        @if(count($queryList)>0)  
           @foreach($queryList as $query)
          
           <tr class="gradeU">
            <td><input type="checkbox" name="id[]" id="id" value="{{$query->query_id}}" /></td>
             <td>
               <b>{{$query->name}}</b>
               <br>
               <a href="mailto:{{$query->email}}"><i class=" icon-envelope-alt"></i> {{$query->email}}</a>
               <br>
               <a href="tel:+91 {{$query->phone}}"><i class="icon-phone"></i> +91 {{$query->phone}}</a>
             </td>
             <td>
               {{$query->query}}
             </td>
             <td> {{$query->created_at}}</td>
         <td >
                 @if($query->is_active == 'no')
                    <button class="btn btn-danger btn-mini tip" onclick="updateQueryStatus('{{$query->is_active}}',{{$query->query_id}})" title="Update Status" >Deavtive</button>
                    @endif
                     @if($query->is_active == 'yes')
                   
                  <button class="btn btn-success btn-mini tip" onclick="updateQueryStatus('{{$query->is_active}}',{{$query->query_id}})" title="Update Status">Active</button>

                    @endif
             |  <button class="btn btn-danger btn-mini tip"
              onclick="deleteQueries({{$query->query_id}})"  id="delete" title="Delete">Delete</button>
              </td>
                 
          </tr>
          @endforeach
          @else

           <tr class="gradeU">
             <td >No Data</td>
           </tr>
            @endif

              </tbody>
           
                   
            </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<script type="text/javascript">
  //active/deactive single row
  function updateQueryStatus(is_active,u_id){
    var is_active = is_active;
    var p_id=u_id;
    var msg='';
     var url="{{url('/')}}/backend/update-status";
    if(is_active=='yes'){
    msg=' Deactive ';
  }
  else{
    msg=' Activate ';
  }
if(confirm("Are you sure to "+ msg)){
    $.ajax({
      url:url,
      type: "GET",
      data: {is_active:is_active,p_id:p_id},
      success: function(value){
       // alert(value);
        location.reload();
      }
    });
  }
  }

</script> 

<script type="text/javascript">
  //active multiple rows
  $(document).ready(function(){
  $("#active").click(function(){
    var is_active='no';
    var ids='';
     ids=$('#id:checked').map(function() {return this.value;}).get().join(',');
     var check=$('#id:checked').val();
     if(check!=null){
     if(confirm("Are you sure to Active Status ")){
    $.ajax({
      url:"{{url('/backend/')}}/update-status",
      type: "GET",
      data: {p_id:ids,is_active:is_active},
      success: function(value){
       location.reload();
         alert(value);
      }
    });
  }
    }
    else{
      if($(".alert")){
    $(".alert").remove();
  }
      $(".container-fluid").before("<div class='alert alert-danger'>Atleast One Row Select to Delete</div>");
    }
  });




 });
  //deactive multiple rows
  $(document).ready(function(){
  $("#deactive").click(function(){
    var is_active='yes';
    var ids='';
     ids=$('#id:checked').map(function() {return this.value;}).get().join(',');
     var check=$('#id:checked').val();
     if(check!=null){
     if(confirm("Are you sure to Deactive Status ")){
    $.ajax({
      url:"{{url('/backend/')}}/update-status",
      type: "GET",
      data: {p_id:ids,is_active:is_active},
      success: function(value){
       location.reload();
         alert(value);
      }
    });
  }
    }
    else{
      if($(".alert")){
    $(".alert").remove();
  }
      $(".container-fluid").before("<div class='alert alert-danger'>Atleast One Row Select to Delete</div>");
    }
  });




 });
</script>
<script type="text/javascript">

 $(document).ready(function(){

  $('#userQueries').dataTable( {
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "sDom": '<""l>t<"F"fp>',
      "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 0 ] }
       ]
  });
 
  $("#deleteall").click(function(){
    var ids='';

     ids=$('#id:checked').map(function() {return this.value;}).get().join(',');
     var check=$('#id:checked').val();
     if(check!=null){
     if(confirm("Are you sure to Delete , Data permanently deleted from database ")){
    $.ajax({
      url:"{{url('/backend/')}}/delete-queries",
      type: "GET",
      data: {p_id:ids},
      success: function(value){
       location.reload();
         alert(value);
      }
    });
  }
    }
    else{
      if($(".alert")){
    $(".alert").remove();
  }
      $(".container-fluid").before("<div class='alert alert-danger'>Atleast One Row Select to Delete</div>");
    }
  });




 });
</script>
<script type="text/javascript">
  function deleteQueries(p_id){
    var p_id=p_id;
     if(confirm("Are you sure to Delete , Data permanently deleted from database ")){
    $.ajax({
      url:"{{url('/backend/')}}/delete-queries",
      type: "GET",
      data: {p_id:p_id},
      success: function(value){
       location.reload();
         alert(value);
      }
    });
  }
}




 
</script>
<script type="text/javascript">
  
</script>
@include('backend.layouts.footer')


