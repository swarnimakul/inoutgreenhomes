@include('backend.layouts.master')
<style type="text/css">
  .img{
    width: 50px;
    border: 1px solid green;
    border-radius: 5px;
  }
   .remove-btn{
    font-size:18px;
    color: #c75c59;
 
  }
#geocomplete { width: 200px}

   .map_canvas { 
       width: 800px; 
        height: 380px; 
        margin: 10px 20px 10px 0;

      }
      .margin{
    margin-left: 400px;
  }
</style>
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
 
<style type="text/css">
  a{
    cursor: pointer;
  }
</style>
<!--Header-part-->
<script>
function preview_images() 
{
 var total_file=document.getElementById("images").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<img style='margin-left:5px;margin-top:5px;' class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"' width='80' height='80'>");
 }
}
</script>
<script type="text/javascript">
   function addCat(){
    var newCat = $('#category').val();
    //alert (newCat);
     if(newCat == 'other_cat'){
      $("#othercatDiv").show();
        $("#otherCat").attr("placeholder", "Add new category").focus();
     }
     else{
      $("#othercatDiv").hide();
      
     }
    }
    function addLocation(){
    var newCat = $('#location').val();
    //alert (newCat);
     if(newCat == 'other_loc'){
      $("#otherlocDiv").show();
        $("#otherLoc").attr("placeholder", "Add new Location").focus();
     }
     else{
      $("#otherlocDiv").hide();
      
     }
    }
  </script>
@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/projects').'/' }}" >Projects</a> <a href="{{ URL::to('backend/update-property/').'/' }}{{$propertyList->id}}" class="current">Update Project Details</a> </div>
  <h1>Update Project details   </h1>
</div>
 <div class="container-fluid">
  <hr>
  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box" style="display:none ;">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Basic info</h5>
        </div>
        <div class="widget-content nopadding">
              @include('backend.layouts.errors')
           <div class="control-group">
              <label class="control-label">Name </label>
              <div class="controls">
                <input type="text" class="span11" required="" name="name" value="{{$projectInfo->name}}" placeholder="name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Url :</label>
              <div class="controls">
                <input type="text" name="url" required="" value="{{$projectInfo->url}}" class="span11" placeholder="Url(ex:property-name)" />
              </div>
            </div>
             <!-- rera_id -->
             <div class="control-group">
              <label class="control-label">Rera Id :</label>
              <div class="controls">
                <input type="text" name="rera_id"  class="span11" placeholder="Rera Id"  value="{{$projectInfo->rera_id}}"/>
              </div>
            </div>
            <!-- close -->
            <div class="control-group">
              <label class="control-label">Select Catecory</label>
              <div class="controls">
                  <input type="text"  name="category" value="{{$projectInfo->category_id}}">
              
              </div>
            </div>
       
            <div class="control-group">
              <label class="control-label">Location</label>
              <div class="controls">
                 <input type="text"  name="l_id" value="{{$projectInfo->location_id}}">
              
              </div>
            </div>
           

            <div class="control-group">
              <label class="control-label">Builder</label>
               <div class="controls" id="builderDropDown">
                <input type="text"  name="b_id" value="{{$projectInfo->builder_id}}">
                  
              </div>
             </div>

            <div class="control-group">
              <label class="control-label">Project</label>
               <div class="controls">
                <input type="text" name="project_id" value="{{$projectInfo->id}}">
                
              </div>
             </div>

        </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Other Info</h5>
        </div>
        <div class="widget-content nopadding">
             <div class="control-group">
              <label class="control-label">Website link</label>
              <div class="controls">
                <input type="text"  value="{{!empty($propertyList)?$propertyList->web_url:''}}" class="span11" name="website" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Price</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->price:''}}" class="span11" name="price" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Sold</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->sold:''}}" class="span11" name="sold" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Contract</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->contract:''}}" name="contract"  class="span11" />
              </div>
            </div>
            <!-- file upload -->
           <div class="control-group">
              <label class="control-label"> upload Brochuer</label>
              <div class="controls">
                <input type="file" id="image" accept="application/pdf" value="" name="broucher" onchange="document.getElementById('preview').src = '../../../public/upload/brouchers/pdf-icon.png">
                @if(!empty($propertyList->brochure))
                <a  href="../../../public/upload/brouchers/{{$propertyList->brochure}}" target="_blank">{{$propertyList->brochure}}</a>
                 @endif
              </div>
            </div>
              <!-- close upload -->
           </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Attribute</h5>
        </div>
        <div class="widget-content nopadding">
            <div class="control-group" style="display: none;">
              <label class="control-label">Rooms</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->rooms:''}}" name="room"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group" style="display: none;">
              <label class="control-label">Bed</label>
              <div class="controls">
                <input type="text" name="bed" value="{{!empty($propertyList)?$propertyList->bed:''}}" value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group" style="display: none;">
              <label class="control-label">Bath</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->bath:''}}" name="bath"  value="" class=" span11">
                
              </div>
            </div>
            <div class="control-group" >
              <label class="control-label">Home Area</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->home_area:''}}" name="home_area"  value="" class=" span11">
                 
              </div>
            </div>
           <div class="control-group">
              <label class="control-label">Lot area</label>
              <div class="controls">
                <input type="text" value="{{!empty($propertyList)?$propertyList->lot_area:''}}" name="lot_area"   class=" span11">
                
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Lot Dimensions</label>
              <div class="controls">
                <input type="text" name="lot_dimension"  value="{{!empty($propertyList)?$propertyList->lot_dimension:''}}"  class=" span11">
                
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Garages</label>
              <div class="controls">
                <input type="text" name="garages"  value="{{!empty($propertyList)?$propertyList->garages:''}}"  class=" span11">
                
              </div>
            </div>
            
         </div>
      </div>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Payment Details</h5>
        </div>
        <div class="widget-content nopadding">
           
          <div class="control-group">
              <label class="control-label">Resale Price</label>
              <div class="controls">
                <input type="text" name="resale_price"  value="{{$propertyList->resale_price}}" class=" span11">
                 
              </div>
            </div>
           <div class="control-group">
              <label class="control-label">Builder Price</label>
              <div class="controls">
                <input type="text" name="builder_price"  value="{{$propertyList->builder_price}}" class=" span11">
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Initial Price(At time of booking)</label>
              <div class="controls">
                <input type="text" name="initial_price"  value="{{$propertyList->initial_price}}" class=" span11">
             </div>
            </div>
            <div class="control-group">
              <label class="control-label">Booking amount (%)</label>
              <div class="controls">
                <input type="text" name="booking_amount" placeholder="% amount to pay on booking"  value="{{$propertyList->booking_amount}}" class=" span11">
             </div>
            </div>
            <div class="control-group">
              <label class="control-label">Price to pay on Terrace completion (%)</label>
              <div class="controls">
                <input type="text" name="terrace_comp_amt" placeholder="% amount to pay on Terrace completion"  value="{{$propertyList->terrace_comp_amt}}" class=" span11">
             </div>
            </div>
             <div class="control-group">
              <label class="control-label">Price to pay on possession (%)</label>
              <div class="controls">
                <input type="text" name="possesion_amt" placeholder="% amount to pay  on Possession"  value="{{$propertyList->possesion_amt}}" class=" span11">
             </div>
            </div>

            <div class="control-group">
              <label class="control-label">Booking payment time(days/months)</label>
              <div class="controls">
                <input type="text" name="booking_payment_time" placeholder="Winthin days/months to pay after Registration"  value="{{$propertyList->booking_payment_time}}" class=" span11">
             </div>
            </div>

          
         </div>
       </div>
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Amenities</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group">
              <div class="controls" style="overflow-y: scroll;
    height: 280px;">
                @foreach($amenities as $amenity)
               
                @if(in_array($amenity->id,$typeIdArray))
               
                 <label>
                 <input type="checkbox" value="{{$amenity->id}}" name="amenity[]" checked/>
                  {{$amenity->name}}
                </label>
                @else
                  <label>
                 <input type="checkbox" value="{{$amenity->id}}" name="amenity[]" />
                  {{$amenity->name}}
                </label>
                @endif
                 
                @endforeach

               
              </div>
            </div>
         </div>
      </div>

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Type</h5>
        </div>
        <div class="widget-content nopadding">
           <div class="control-group" style="overflow-y: scroll;
    height: 258px;">
               <div class="controls">
                @foreach($types as $type)
               @if(in_array($type->id,$propertyTypeId))
                <label>
                  <input type="checkbox" value="{{$type->id}}" name="type[]" checked/>
                  {{$type->name}}
                </label>
                @else
                <label>
                  <input type="checkbox" value="{{$type->id}}" name="type[]" />
                  {{$type->name}}
                </label>
                 @endif
                @endforeach
             
              </div>
            </div>
        </div>
    </div>
     <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Gallery</h5>
        </div>
        <div class="widget-content nopadding" id="addmorediv1">
          <div class="control-group">
              <label class="control-label"> Upload Images</label>
                <div class="controls">
                    <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
                </div>
             </div>
            
              <div class="control-group" id="image_preview">
                 @if(!empty($gallery))
                    @foreach($gallery as $g_img)
                    <div style="width:98px;height: 90px; float: left; border: 1px solid black; margin-left: 5px; margin-top: 2px">
                    <img class="img-responsive" height="70" width="88"    style=" margin-left: 5px; margin-top: 5px; width: 88px; height: 70px; "src="{{'../../../public/upload/property_gallery/'.$g_img->img_name}}"/>
                    <div><a href="javascript:void(0)" onclick="deleteImage({{$g_img->id}},{{$g_img->property_id}})">Delete</a></div>
                    </div>
                     @endforeach
                  @endif
              </div>
               <br>
           </div>    
    </div>

  <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Floor Plan</h5>
        </div>
        <div class="widget-content nopadding" id="addmorediv1">
            
            <div class="control-group">
              <label class="control-label">Type</label>
                <div class="controls">
                    <input name="floor_plan_type[]" class="form-control" />
                </div>   
             </div>
             <div class="control-group">
              <label class="control-label">Image</label>
                <div class="controls">
                    <input type="file" class="form-control" id="floor_plan" name="floor_plan[]" onchange=""/>
                </div>   
             </div>
            </div>
             <div id="add"></div>
          <div class="control-group">
            <label class="control-label"></label>
            <a href="#" id="add-more" style="margin-left: 200px;"><i  class="icon-plus-sign add-btn"></i>ADD MORE</a>
           <div class="control-group" id="image_f_preview" >
            
                 @if(!empty($floorPlan))
                    @foreach($floorPlan as $img)
                    <div style="width:98px;height: 90px; float: left; border: 1px solid black; margin-left: 5px; margin-top: 2px">
                    <img class="img-responsive" height="70" width="88"    style=" margin-left: 5px; margin-top: 5px; width: 88px; height: 70px; " src="{{'../../../public/upload/floor_plans/'.$img->image}}"/>
                    <div><a href="javascript:void(0)" onclick="deletef_Image({{$img->id}},{{$img->property_id}})">Delete</a>  <span; style="margin-left: 10px;color: red">{{$img->type}}</span> </div> 
                    </div>

                     @endforeach

                  @endif
              </div>
              <br>
           </div>  

    </div>
     <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Site Plan</h5>
        </div>
        <div class="widget-content nopadding" >
           <div class="control-group">
              <label class="control-label">Image</label>
                <div class="controls">
                    <input type="file" class="form-control" id="site_plan" name="site_plan" onchange=""/> <br>
                     @if(!empty($propertyList->site_plan_image))
                <img style="width:100px; height: 100px;" src="../../../public/upload/project_image/site_plan/{{$propertyList->site_plan_image}}" />
                 @endif
                </div>   
             </div>
            </div>
       </div>
       
  
  </div>
  <div class="row-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Address</h5>
        </div>
        <div class="widget-content " >
            <div class="control-group">
              <label class="control-label">Address</label>
                <div class="controls">
                      <input id="geocomplete" name="address" type="text" placeholder="Type in an address" value="{{$propertyList->address}}" size="90" /></div>
                
            </div>
            <div class="control-group" style="margin-left: 130px;">
              <div class="map_canvas"></div>
            </div>
            <div id="info" class="control-group">
              <div class="controls">
                      Latitude
                <input name="lat" type="text" value="{{$propertyList->latitude}}">
                      Longitude 
                <input name="lng" type="text" value="{{$propertyList->longitude}}">
              </div>
           </div>
           <div class="control-group">
              <label class="control-label">Near By : </label>
              
               <div class="controls">
               @if(in_array('School',$nearByArray))
               <input type="checkbox" value="School" name="nearBy[]" checked="" />School <br>
               @else
               <input type="checkbox" value="School" name="nearBy[]" />School <br>
               @endif
               @if(in_array('Hospital',$nearByArray))
               <input type="checkbox" value="Hospital" name="nearBy[]" checked="" />Hospital <br>
               @else
               <input type="checkbox" value="Hospital" name="nearBy[]" />Hospital <br>
               @endif
               @if(in_array('ATM',$nearByArray))
               <input type="checkbox" value="ATM" name="nearBy[]" checked="" />ATM <br>
               @else
               <input type="checkbox" value="ATM" name="nearBy[]" />ATM <br>
               @endif
               @if(in_array('Market',$nearByArray))
               <input type="checkbox" value="Market" name="nearBy[]" checked="" />Market <br>
               @else
               <input type="checkbox" value="Market" name="nearBy[]" />Market <br>
               @endif
               @if(in_array('Gym',$nearByArray))
               <input type="checkbox" value="Gym" name="nearBy[]" checked="" />Gym <br>
               @else
               <input type="checkbox" value="Gym" name="nearBy[]" />Gym <br>
               @endif
               @if(in_array('Entertainment',$nearByArray))
               <input type="checkbox" value="Entertainment" name="nearBy[]" checked="" />Entertainment <br>
               @else
               <input type="checkbox" value="Entertainment" name="nearBy[]" />Entertainment <br>
               @endif
               @if(in_array('Cafe',$nearByArray))
               <input type="checkbox" value="Cafe" name="nearBy[]" checked="" />Cafe <br>
               @else
               <input type="checkbox" value="Cafe " name="nearBy[]" />Cafe <br>
               @endif
               @if(in_array('Bus Stand',$nearByArray))
               <input type="checkbox" value="Bus Stand" name="nearBy[]" checked="" />Bus stand
               @else
               <input type="checkbox" value="Bus Stand" name="nearBy[]" />Bus stand
               @endif
              </div>
            </div>
        </div>

        </div>
  </div>

  </div>
  <div class="row-fluid">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Description</h5>
      </div>
      <div class="widget-content">
         <div class="control-group">
              <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="description" value="" required>{{!empty($propertyList)?$propertyList->description:''}}</textarea>
         </div>
      </div>
    </div>
  </div>
</div> 
         <div class="form-actions">
            <label class="control-label"></label>
              <button type="submit" class="btn btn-success">Update</button>
            </div>
</form>
</div>
</div>
</div>
<!--Footer-part-->
<script type="text/javascript">
$(document).ready(function() {
  
  $("#add-more").click(function(e){

    e.preventDefault();
    var add='<div><hr><label class="control-label"></label><a href="#" id="remove" class="margin" title="REMOVE"><i class="icon-minus-sign remove-btn"></i> </a><div class="control-group"><label class="control-label">Type</label><div class="controls"><input name="floor_plan_type[]" class="form-control" /></div></div><div class="control-group"><label class="control-label">Image</label><div class="controls"><input type="file" class="form-control" id="floor_plan" name="floor_plan[]" onchange=""/></div></div>';
    $("#add").append(add); 
  });
    $(document).on('click','#remove',function(){
      $(this).closest('div').empty();
    });
});
  function deleteImage(imgId,propId){
     var imgId  = imgId;
     var Url = "{{url('/')}}/backend/deletePropImg/"+imgId+"/"+propId;
     if(confirm("Are you sure to delete image")){
      $.ajax({
        url: Url,
        type: "get",
        dataType: "json",
        success: function(data){
         //alert(data);
          $('#image_preview').html(data);
        }
      });
    }
  }
  function deletef_Image(imgId,propId){
     var imgId  = imgId;
     var Url = "{{url('/')}}/backend/deletePropf_Img/"+imgId+"/"+propId;
     if(confirm("Are you sure to delete image")){
      $.ajax({
        url: Url,
        type: "get",
        dataType: "json",
        success: function(data){
         //alert(data);
          $('#image_f_preview').html(data);
        }
      });
    }
  }
 /* $(document).ready(function() {
   $('select[name="l_id"]').on('change', function(){
            var locID = $(this).val();
              if(locID) {
               $.ajax({
                    url: "../ajaxBuilder/"+locID,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                     $('select[name="b_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="b_id"]').append('<option value="'+ value['id'] +'">'+ value['name'] +'</option>');
                        });
                        
                    }
                });
            }else{

                $('select[name="b_id"]').append('<option value=" ">-Select Builder-</option>');
            }
        });
        $('select[name="l_id"]').on('change', function() {
            var locIDs = $(this).val();
            if(locIDs) {
                $.ajax({
                    url: "../ajaxProject/"+locIDs,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="project_id"]').empty();
                        $.each(data, function(keys, values) {
                          //alert(value['name'])
                           $('select[name="project_id"]').append('<option value="'+ values['id'] +'">'+ values['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').append('<option value=" ">-Select Project-</option>');
            }
        });
 $('select[name="b_id"]').on('change', function() {
            var bIDs = $(this).val();
            if(bIDs) {
                $.ajax({
                    url: "../ajaxBuilderProject/"+bIDs,
                    type: "get",
                    dataType: "json",
                    success:function(data) {
                    $('select[name="project_id"]').empty();
                        $.each(data, function(keys, values) {
                          //alert(value['name'])
                           $('select[name="project_id"]').append('<option value="'+ values['id'] +'">'+ values['name'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').append('<option value=" ">-Select Project-</option>');
            }
        });


    });*/
    </script>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script>CKEDITOR.replace('textarea');</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!--Footer-part-->
@include('backend.layouts.footer')
