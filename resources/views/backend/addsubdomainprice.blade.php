@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
<!--Header-part to commit-->
<style type="text/css">
  .add-btn{
       font-size: 18px;
    color: #c75c59;
  }
  .remove-btn{
    font-size:18px;
    color: #c75c59;
 
  }
 
  .margin{
    margin-left: 400px;
  }
</style>
  @include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="{{ URL::to('backend/subdomains').'/' }}" class="tip-bottom">Subdomain</a>  <a href="#" class="current">Subdomain Price</a> </div>
  <h1>Add Subdomain Price</h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>Add Subdomain Price </h5>
        </div>
        @if(!empty(session('message')=='success'))
        <div class="alert alert-success">Record Added Successfully</div>
        @endif
    <div class="widget-content nopadding">
             @include('backend.layouts.errors')
      <form  method="post" action="" >
          {{ csrf_field() }}
          <div class="form-horizontal">
              <br>
            <div class="control-group">
              <label class="control-label">Subdomain</label>
              <div class="controls">
               <select name="subdomain_id" class="span4">
                   @foreach($subdomains as $subdomain)
                  <option value="{{$subdomain->id}}">{{$subdomain->name}}</option>
                  @endforeach
                 </select>
              </div>
            </div>
          <div class="control-group">
              <label class="control-label">Acoomodation Type:</label>
              <div class="controls">
               <input class="span4" type="text" name="accomadation_type[]" value="" requird >
              </div>
          </div>
          
          <div class="control-group">
            <label class="control-label">Unit Size:</label>
            <div class="controls">
             <input class="span4" type="text" name="unit_size[]" value="" requird >
            </div>
          </div>
          <div class="control-group">
              <label class="control-label">Basic Price:</label>
              <div class="controls">
               <input class="span4" type="text" name="basic_price[]" value="" requird >
              </div>
          </div>
          <div class="control-group">
              <label class="control-label">Total Price:</label>
              <div class="controls">
               <input class="span4" type="text" name="total_price[]" value="" requird >
             </div>
          </div>
          <div id="add"></div>
          <div class="control-group">
            <label class="control-label"></label>
            <a href="#" id="add-more" class="margin"><i  class="icon-plus-sign add-btn"></i>ADD MORE</a>
          </div>
          <div class="form-actions">
           <label class="control-label"></label>
            <button type="submit" class="btn btn-success">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
 
</div>

</div></div></div>
<script type="text/javascript">
  
$(document).ready(function() {
$("#add-more").click(function(e){
e.preventDefault();
var add='<div><hr><label class="control-label"></label><a href="#" id="remove" class="margin" title="REMOVE"><i class="icon-minus-sign remove-btn"></i> </a><div class="control-group">'+
'<label class="control-label">Acoomodation Type:</label>'+
'<div class="controls">'+
' <input class="span4" type="text" name="accomadation_type[]" value="" requird > </div> </div><div class="control-group">'+
' <label class="control-label">Unit Size:</label>'+
'<div class="controls">'+
' <input class="span4" type="text" name="unit_size[]"'+ 'value="" requird >'+
'</div></div>'+' <div class="control-group">'+
'<label class="control-label">Basic Price:</label>'+
'<div class="controls">'+
'<input class="span4" type="text" name="basic_price[]" value="" requird >'+
' </div>'+
'</div> <div class="control-group">'+
'<label class="control-label">Total Price:</label>'+
'<div class="controls">'+
' <input class="span4" type="text" name="total_price[]" value="" requird ></div>'+
'</div></div>';
$("#add").append(add); 
        
    });
$(document).on('click','#remove',function(){
  $(this).closest('div').empty();
});
});
   
    </script>
<<!--Footer-part-->
@include('backend.layouts.footer')
