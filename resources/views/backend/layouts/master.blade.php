<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>DelhiSmartCities Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
       
    <link rel="stylesheet" href="{{ asset('public/backend/vendor/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{asset('public/backend/vendor/bootstrap/css/bootstrap-responsive.min.css')}}" />
    <link rel="stylesheet" href="{{asset('public/backend/css/matrix-login.css')}}" />
    
    <link rel="stylesheet" href="{{asset('public/backend/css/matrix-style.css')}}" />
    <link rel="stylesheet" href="{{asset('public/backend/css/matrix-media.css')}}" />
    <link href="{{asset('public/backend/vendor/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>



    </head>
    <body>


    @yield('content')

    <!-- Javascript files-->
    
     <script src="{{asset('public/backend/js/jquery.min.js')}}"></script>  
        <script src="{{asset('public/backend/js/matrix.login.js')}}"></script> 
    </body>

</html>