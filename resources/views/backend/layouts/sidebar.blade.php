<!--sidebar-menu-->
<div id="sidebar">
    <a href="{{ URL::to('backend/index').'/' }}" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
         
  <ul>
    <li class="{{ ( $sidebarTab == 'Home' ) ? 'active' : '' }}"><a href="{{ URL::to('backend/index').'/' }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
   
    <li class="{{ ( $sidebarTab == 'Pages' ) ? 'active' : '' }} submenu"><a href="{{ URL::to('backend/pages').'/' }}"><i class="icon icon-th"></i> <span>Pages</span></a>
    	<ul>
        <li><a href="{{ URL::to('backend/edit-home-page').'/' }}{{PagesController::getHomePageID()}}">Edit Home page</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-page').'/' }}">Add Static pages</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/pages').'/' }}">All pages</a></li>
        </ul>


    </li>
   <li class="{{ ( $sidebarTab == 'Projects' || $sidebarTab == 'Properties' ) ? 'active' : '' }} submenu"><a href="{{ URL::to('backend/projects').'/' }}"><i class="icon icon-file"></i> <span>Projects</span></a>
    	<ul>
        <li><a href="{{ URL::to('backend/projects').'/' }}">Project List</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-projects').'/' }}">Add Project</a></li>
        </ul>
        
        <ul>
        <li><a href="{{ URL::to('backend/categories').'/' }}">Category</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/locations').'/' }}">Location</a></li>
        </ul>
    
        
        <ul>
        <li><a href="{{ URL::to('backend/amenities').'/' }}">Amenities</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/prop-type').'/' }}"> type</a></li>
        </ul>
        

    </li>
    <li class="{{ ( $sidebarTab == 'Builders' ) ? 'active' : '' }}"><a href="{{ URL::to('backend/builders').'/' }}"><i class="icon icon-th"></i> <span>Builders</span></a></li>
    <li class="{{ ( $sidebarTab == 'Blogs' || $sidebarTab == 'Blogs' ) ? 'active' : '' }} submenu">
    <a href="{{ URL::to('backend/blogs').'/' }}"><i class="icon icon-file"></i> <span>Blogs</span></a>
    	<ul>
        <li><a href="{{ URL::to('backend/blogs').'/' }}">Blogs</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/add-blogs').'/' }}">Add Blog</a></li>
        </ul>
        <ul>
        <li><a href="{{ URL::to('backend/blog-cat').'/' }}">Blog Category</a></li>
        </ul>
       
        

    </li>
  
    
  </ul>

</div>
<!--sidebar-menu-->