@if ( count($errors) )

	<div class="form-group">

		<div class="alert alert-danger" role="alert">

			@if ( count($errors) > 1 )
				<ul>

				@foreach ( $errors->all() as $error )

					<li>{{ $error }}</li>

				@endforeach

				</ul>
			@else
				{{ $errors->all()[0] }}
			@endif
		</div>

	</div>

@endif