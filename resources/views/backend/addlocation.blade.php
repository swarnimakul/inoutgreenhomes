@include('backend.layouts.master')

<!--Header-part to commit-->

  @include('backend.layouts.header')
  

<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ URL::to('backend/projects').'/' }}" class="tip-bottom">Projects</a><a href="{{ URL::to('backend/locations').'/' }}" >Location</a>  <a href="add-location" class="current">{{!empty($location)?'Update Location':'Add Location'}}</a> </div>
  <h1></h1>
</div>



<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>{{!empty($location)?'Update Location':'Add Location'}}</h5>
        </div>
         @if(!empty($message))
        @if($message=='fail')
        <div class="alert alert-danger">Location already Exists</div>
        @endif
         @if($message=='success')
        <div class="alert alert-success">Location added successfully!</div>
        @endif
      @endif
        <div class="widget-content nopadding">
          <form  method="post" action="" >
          {{ csrf_field() }}
          <div class="form-horizontal">
             @include('backend.layouts.errors')
               
           <div class="control-group">
              <label class="control-label"> Location
:</label>
              <div class="controls">
               <input class="span4" type="text" name="location" value="{{!empty($location)?$location->location:''}}" requird >
              </div>
            </div>
            <div class="form-actions">
              <label class="control-label"></label>
              <button type="submit" class="btn btn-success">{{!empty($location)?'Update':'Save'}}</button>
            </div>
          </div>
        </form>
        </div>
      </div>
</div>
</div>
</div></div></div>
<<!--Footer-part-->
@include('backend.layouts.footer')
