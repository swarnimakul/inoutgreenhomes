@include('backend.layouts.master')
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />
 
<style type="text/css">
  a{
    cursor: pointer;
  }
  .sp{
    color:red;
    margin-left:10px;
    text-transform: uppercase;
    
  }
  .hide{
    visibility: hidden;

  }
</style>
<!--Header-part-->


@include('backend.layouts.header')
<!--close-Header-part--> 
<!--sidebar-menufd-->
@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" >Profile</a> <a class="current">Change Password</a> </div>
  <h1>Change Password </h1>
</div>
 <div class="container-fluid">
  <hr>
 <form method="post" id="submit" class="form-horizontal" enctype="multipart/form-data">
  {{ csrf_field() }}
@if(session('info')=='success')
    <div class="alert alert-success"> Password Change Successfully!</div>
    @endif
  
 <div class="row-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Change Password</h5>
        </div>
             <div class="control-group">
              <label class="control-label">Current Password:</label>
              <div class="controls">
                <input type="text" name="cupassword" required="" id="cupassword" class="span4" placeholder="" />
                @if(session('info')=='fail')
                 <span class="alert alert-danger">
                    <strong>Current Password Not Match</strong>
                </span>
                @endif
                
              </div>
            </div>

             <div class="control-group">
              <label class="control-label">New Password:</label>
              <div class="controls">
                <input type="text" id="password-1" name="password" required="" pattern="{6,8}" title="Must contain at 8 or more number and characters"value="" class="span4" placeholder="" />
                 @if ($errors->has('password'))
                                    <span class="alert alert-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              </div>
            </div>

            <div class="control-group">
          <label class="control-label">Confirm Password:</label>
              <div class="controls">
                <input type="text" id="password-2" name="cpassword" required=""  value="" class="span4 password" placeholder="" " />
            <span class="alert alert-danger hide" id="message">Password Not Match</span>
              </div>
            </div>

       

        </div>
  </div>

  <!-- submit btn -->
            <div class="form-actions">
            <label class="control-label"></label>
              <button type="submit" class="btn btn-success" id="submit-btn">Change Password</button>
            </div>

 </form>
</div>
</div>
</div>
<!--Footer-part-->
<script type="text/javascript">
     $('#submit').click(function(event){
      if($("#password-1").val()!=''&&$("#password-2").val()!=''){
      if($("#password-1").val()!= $("#password-2").val()){
            /* they are different */

            $("#message").removeClass('hide').addClass('sp');
            $("#submit-btn").attr("disabled",true);
             
      }
      else{
         $("#message").removeClass('sp').addClass('hide');
        $("#submit-btn").removeAttr("disabled");
      }
     
    }
 });
  
</script>

<!--Footer-part-->
@include('backend.layouts.footer')
