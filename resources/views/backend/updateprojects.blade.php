
@include('backend.layouts.master')
<style type="text/css">
  .img{
    width: 100px;
    border: 1px solid green;
    border-radius: 5px;
  }

</style>
<script type="text/javascript">

  function addLocation(){
    var newCat = $('#location').val();
    //alert (newCat);
     if(newCat == 'other_loc'){
      $("#otherlocDiv").show();
        $("#otherLoc").attr("placeholder", "Add new Location").focus();
     }
     else{
      $("#otherlocDiv").hide();
      
     }
    }
</script>
<link rel="stylesheet" href="{{asset('public/backend/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('public/backend/css/select2.css')}}" />

 
<!--Header-part-->

  @include('backend.layouts.header')
<!--close-Header-part--> 


<!--sidebar-menu-->

@include('backend.layouts.sidebar')  

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ URL::to('backend/index').'/' }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="{{ URL::to('backend/projects').'/' }}" >Projects</a> <a href="" class="current"> {{!empty($edit)?'Update':'Add'}} project </a> </div>
  <h1>{{!empty($edit)?'Update':'Add New '}} project</h1>
</div>
<div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-plus-sign"></i> </span>
            <h5>{{!empty($edit)?'Update':'Add'}} </h5>
          </div>
          @if(!empty(session('info')))
    <div class="alert alert-danger">{{session('info')}}</div>
    @endif
          <div class="widget-content nopadding">
            <form  method="post" action="#" enctype="multipart/form-data" >
              <div class="form-horizontal">
                {{ csrf_field() }}

                @include('backend.layouts.errors')
           <div class="control-group">
              <label class="control-label">Builder</label>
            
             <div class="controls">
                <select name="b_id"  class="span4" >
                    <option value="" selected>-Select Builder-</option>
                  @foreach($builders as $builder)
                   @if($builder->name==$edit->builder_name)
                  <option value="{{$builder->id}}" selected>{{$builder->name}}</option>
                  @else
                  <option value="{{$builder->id}}" >{{$builder->name}}</option>
        @endif
        @endforeach
                </select>
             </div>
           </div>
             
              <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                  <input type="text" name="name" required="" class="span4" value="{{!empty($edit)?$edit->name:''}}" >
                </div>
              </div>
         
             <div class="control-group">
                 <label class="control-label" >Location</label>
              <div class="controls">
                <select name="location[]" multiple id="location" class="span4" onchange="addLocation()" >
                   @if(!empty($locations))
                  @foreach($locations as $loc)
        
                  @if(!empty($edit)&&!empty($locarray))
            @foreach($locarray as $array)
          @if($array->location==$loc->location)
                  <option value="{{$array->id}}" selected>{{$loc->location}}</option>
        @endif
        @endforeach
                 @endif
                     @if(!empty($edit)&& $edit->location!=$loc->location)
                      <option value="{{$loc->id}} ">{{$loc->location}}</option>
                 @endif
                   @if(empty($edit))
                    <option value="{{$loc->id}} ">{{$loc->location}}</option>
                  @endif
                  @endforeach
                  @endif
                  <option value="other_loc">Other</option>
                 
                </select>
              </div>
              </div>
               <div class="control-group" id="otherlocDiv" style="display: none;" >
                 <label class="control-label"></label>
                <div class="controls">
                  <input type="text" name="otherLoc" id="otherLoc"  >
               
                </div>
              </div>
<!-- category open -->

     <div class="control-group" id="selectCat">
                 <label class="control-label">Category</label>
              <div class="controls">
                <select name="category" class="span4" id="category"  >  <option value=" ">-Select Category-</option>
                  
                  @if(!empty($category))
                  @foreach($category as $cat)
                      @if(!empty($edit)&& $edit->category==$cat->category)
                   <option value="{{$edit->category_id}} " selected>{{$cat->category}}</option>
                   @endif
                    @if(!empty($edit)&& $edit->category!=$cat->category)
                  <option value="{{$cat->id}} ">{{$cat->category}}</option>
                 @endif
                   @if(empty($edit))
                  <option value="{{$cat->id}} ">{{$cat->category}}</option>
                  @endif
                  @endforeach
                  @endif
              
                    
                </select>
              </div>
              </div>
              
<!-- category close -->
              
  <!-- url -->
             <div class="control-group">
              <label class="control-label">Url :</label>
              <div class="controls">
                <input type="text" name="url" required="" class="span4"  placeholder="Url(ex:project-name)" value="{{!empty($edit)?$edit->url:''}}" />
              </div>
            </div>
        <!-- close -->
         <!-- rera_id -->
             <div class="control-group">
              <label class="control-label">Rera Id :</label>
              <div class="controls">
                <input type="text" name="rera_id"  class="span4" placeholder="Rera Id" value="{{!empty($edit)?$edit->rera_id:''}}"/>
              </div>
            </div>
            <!-- close -->


          <!-- Minimum price -->
          <div class="control-group">
            <label class="control-label">Min. Price</label>
              <div class="controls">
                <input type="text" placeholder="Enter Minimum Price"  class="span4" value="{{!empty($edit)?$edit->min_price:''}}" name="min_price" />
              </div>
            </div> 

            <!-- close -->
              <!-- Maximum price -->
          <div class="control-group">
            <label class="control-label">Max. Price</label>
              <div class="controls">
                <input type="text" placeholder="Enter Maximum Price"  class="span4" value="{{!empty($edit)?$edit->max_price:''}}" name="max_price" />
              </div>
            </div> 

            <!-- close -->
            <!-- bed -->
            <div class="control-group">
              <label class="control-label">Bedroom</label>
              <div class="controls">
                <input type="text" placeholder="No. Of Bedrooms" name="bed" value="{{!empty($edit)?$edit->bed:''}}"  class=" span4">
                
              </div>
            </div>


            <!-- bed -->
            <!-- bath -->
           <div class="control-group">
              <label class="control-label">Bathroom</label>
              <div class="controls">
                <input type="text" placeholder="No. Of Bathrooms" name="bath"  value="{{!empty($edit)?$edit->bath:''}}" class=" span4">
                
              </div>
            </div>

            <!-- close -->
            <!-- size -->
            <div class="control-group">
              <label class="control-label">Size</label>
              <div class="controls">
                <input type="text" placeholder="Size " name="size"  value="{{!empty($edit)?$edit->size:''}}" class=" span4">
                 
              </div>
            </div>
            <!-- close -->
               <!-- project type -->
           <div class="control-group">
            <label class="control-label">Project feature</label>
              <div class="controls">
                 <input type="text" name="project_feature" value="{{!empty($edit)?$edit->project_feature:''}}" placeholder="Ex: 2BHK 3Bhk"  class="span4" />
            
              </div>
            </div> 

            <div class="control-group">
            <label class="control-label">Project type</label>
              <div class="controls">
                <select  name ="project_type[]" class="span4" multiple="">
                 @if(!empty($edit) && in_array('Apartment',$pTypeArray)) 
                 <option value="Apartment" selected="selected" >Apartment</option>
                 @else
                  <option value="Apartment">Apartment</option>
                 @endif
                 @if(!empty($edit) && in_array('Residential Plot',$pTypeArray))
                <option value="Residential Plot" selected="selected">Residential Plot</option>
                @else
                <option value="Residential Plot">Residential Plot</option>
                @endif
                @if(!empty($edit) && in_array('Independent Floor',$pTypeArray))
                <option value="Independent Floor" selected="selected">Independent Floor</option>
                @else
                <option value="Independent Floor">Independent Floor</option>
                @endif
                @if(!empty($edit) && in_array('Villa',$pTypeArray))
                <option value="Villa" selected="selected">Villa</option>
                @else
                <option value="Villa" >Villa</option>
                @endif
                @if(!empty($edit) && in_array('Studio',$pTypeArray))
                <option value="Studio" selected="selected">Studio</option>
                @else
                <option value="Studio" >Studio</option>
                @endif
                @if(!empty($edit) &&  in_array('Row House',$pTypeArray))
                <option value="Row House" selected="selected">Row House</option>
                @else
                <option value="Row House" >Row House</option> 
                @endif
                @if(!empty($edit) && in_array('Independent House',$pTypeArray))
                <option value="Independent House" selected="selected">Independent House</option>
                @else
                <option value="Independent House">Independent House</option>
                @endif
                @if(!empty($edit) && in_array('Farm House',$pTypeArray))
                <option value="Farm House" selected="selected">Farm House</option>
                @else
                <option value="Farm House">Farm House</option>
                @endif
                @if(!empty($edit) &&  in_array('Town Suites',$pTypeArray))
                <option value="Town Suites" selected="selected">Town Suites</option>
                @else
                <option value="Town Suites">Town Suites</option>
                @endif
                @if(!empty($edit) && in_array('Township',$pTypeArray))
                <option value="Township" >Township</option>
                @else
                 <option value="Township">Township</option>
                @endif
                @if(!empty($edit) && in_array('Bungalow',$pTypeArray))
                <option value="Bungalow" selected="selected">Bungalow</option>
                @else
                <option value="Bungalow">Bungalow</option>
                @endif
                @if(!empty($edit) && in_array('Retail Shop',$pTypeArray))
                <option value="Retail Shop" selected="selected">Retail Shop</option>
                @else
                <option value="Retail Shop">Retail Shop</option>
                @endif
                 @if(!empty($edit) && in_array('Office Space',$pTypeArray))
               
                <option value="Office Space" selected="">Office Space</option>
                @else
                <option value="Office Space">Office Space</option>
                
                @endif
                @if(!empty($edit) && in_array('Plots',$pTypeArray))
               
                <option value="Plots" selected="">Plots</option>
                @else
                <option value="Plotes">Plots</option>
                
                @endif
            </select> 
              </div>
            </div> 
            <div class="control-group">
            <label class="control-label">Project Phase</label>
              <div class="controls">
                 <select name="phase" class="span4">
                   @if(!empty($edit) && $edit->phase == 'Launch')
                   <option value="Launch" selected="">Launch</option>
                   @else
                   <option value="Launch">Launch</option>
                   @endif
                   @if(!empty($edit) && $edit->phase == 'Under Construction')
                   <option value="Under Construction" selected="">Under Construction</option>
                   @else
                   <option value="Under Construction">Under Construction</option>
                   @endif
                   @if(!empty($edit) && $edit->phase == 'Ready to Move')
                   <option value="Ready to Move" selected="">Ready to Move</option>
                   @else
                   <option value="Ready to Move">Ready to Move</option>
                   @endif
                   @if(!empty($edit) && $edit->phase == 'Depend draw')
                   <option value="Depend draw" selected="">Depend draw</option>
                   @else
                   <option value="Depend draw">Depend draw</option>
                   @endif
                    @if(!empty($edit) && $edit->phase == 'Upcoming Projects')
                   <option value="Upcoming Projects" selected="">Upcoming Projects</option>
                   @else
                   <option value="Upcoming Projects">Upcoming Projects</option>
                   @endif
                   @if(!empty($edit) && $edit->phase == 'Pre Launch')
                   <option value="Pre Launch" selected="">Pre Launch</option>
                   @else
                   <option value="Pre Launch">Pre Launch</option>
                   @endif
                 </select>
              </div>
            </div> 
             <div class="control-group">
            <label class="control-label">Locality</label>
              <div class="controls">
                 <input type="text" name="locality" value="{{$edit->locality}}"  class="span4" />
            
              </div>
            </div> 
            <!-- close -->
              <!--  Parking -->
              <div class="control-group">
          <label class="control-label">Parking</label>
               <div class="controls">
                <input type="text" name="parking" value="{{$edit->parking}}"  class="span4" />
                </div>
            </div> 
            <!-- close -->
              <!-- Logo -->
              <div class="control-group">
                <label class="control-label"> Project Logo</label>
                <div class="controls">
   <input type="file" id="image" value="{{!empty($edit)?$edit->logo:''}}" name="logo"  onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])"/>
 <img class="img" id="preview" src="{{!empty($edit->logo)?'../../../public/upload/project_logo/'.$edit->logo:''}}"
    >
                </div>
              </div>
              <!-- project logo -->
               <!-- projects image -->
                <div class="control-group">
                <label class="control-label"> Project Image</label>
                <div class="controls">
           <input type="file" id="image" value="{{!empty($edit)?$edit->project_image:''}}" name="project_image"  onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])"/>
            <img class="img" id="preview2" src="{{!empty($edit->project_image)?'../../../public/upload/project_image/'.$edit->project_image:''}}">

                </div>
              </div>
              <!-- close -->

<!-- featured open -->
            <div class="control-group">
              <label class="control-label">Featured</label>
                <div class="controls">
                <input type="checkbox" name="is_featured"  class="span11" {{($edit->is_featured=='yes')?'checked':''}}>
                </div>
            </div>
            <div class="control-group">
            <label class="control-label">Meta Title</label>
              <div class="controls">
                 <textarea name="meta_tag"   class="span4" >{{$edit->meta_tag}}</textarea>
            
              </div>
            </div> 
            <div class="control-group">
            <label class="control-label">Meta Description</label>
              <div class="controls">
                 <textarea name="meta_desc" class="span4">{{$edit->meta_desc}}</textarea>
             </div>
            </div> 
            <div class="control-group">
            <label class="control-label">Meta Keyword</label>
              <div class="controls">
                 <textarea name="meta_keyword" class="span4">{{$edit->meta_keyword}}</textarea>
             </div>
            </div>
            <!-- og title -->
             <div class="control-group">
              <label class="control-label">OG Title </label>
              <div class="controls">
                
                <textarea class="span11" name="og_title" value="" >{{ ! empty($edit->og_title) ? $edit->og_title : '' }}</textarea>
             
              </div>
            </div>
            <!-- og site name -->
              <div class="control-group">
              <label class="control-label">OG Site Name</label>
              <div class="controls">
                
                <textarea class="span11" name="og_site_name" value="" >{{ ! empty($edit->og_site_name) ? $edit->og_site_name : '' }}</textarea>
             
              </div>
            </div>
            <!-- og Type -->
              <div class="control-group">
              <label class="control-label">OG Type </label>
              <div class="controls">
                
                <textarea class="span11" name="og_type" value="" >{{ ! empty($edit->og_type) ? $edit->og_type : '' }}</textarea>
             
              </div>
            </div>
            <!-- og image -->
              <div class="control-group">
              <label class="control-label">OG Image </label>
              <div class="controls">
                
                <textarea class="span11" name="og_image" value="" >{{ ! empty($edit->og_image) ? $edit->og_image : '' }}</textarea>
             
              </div>
            </div>
            <!-- og url -->
               <div class="control-group">
              <label class="control-label">OG Url </label>
              <div class="controls">
                
                <textarea class="span11" name="og_url" value="" >{{ ! empty($edit->og_url) ? $edit->og_url : '' }}</textarea>
             
              </div>
            </div>
       <!-- close --> 

<!-- featured close -->
                   </div>
                 </div>
       <div class="widget-box">
      <div class="widget-title"> 
        <h5>Description</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        
            <div class="controls">
             <center> <textarea class=" span12" rows="6" placeholder="Enter text ..." id="textarea" name="description" value="" >{{!empty($edit)?$edit->description:''}}</textarea></center>
            </div>
         
        </div>
      </div>
    </div>
              <div class="form-actions">
              <button type="submit" class="btn btn-success">{{!empty($edit)?'Update':'Save'}}</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
       
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<!--Footer-part-->
@include('backend.layouts.footer')
