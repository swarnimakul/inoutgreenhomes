<section  class="num_tab ">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"> <span>Talk to Us. We hear you patiently.</span> <a href="tel:+91 {{$address->phone}}">(+91) {{$address->phone}}</a> </div>
    </div>
  </div>
</section>
<footer>
<div class="footer1">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
<h4>Top Cities Project</h4>
<ul>
    <li><a href="{{ URL::to('/new-delhi/') }}/">New Delhi</a></li>
    <li><a href="{{ URL::to('/noida/') }}/">Noida</a></li>
    <li><a href="{{ URL::to('/greater-noida/') }}/">Greater Noida</a></li>
    <li><a href="{{ URL::to('/gurugram/') }}/">Gurugram</a></li>
    <li><a href="{{ URL::to('/bangalore/') }}/">Bangalore</a></li>
    <li><a href="{{ URL::to('/pune/') }}/">Pune</a></li>
    <li><a href="{{ URL::to('/mumbai/') }}/">Mumbai</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
<h4>Quick Links</h4>
<ul>
      <li><a href="{{ URL::to('/') }}/">Home</a></li>
    <li><a href="{{ URL::to('/about-us/') }}/">About Us</a></li>
    <li><a href="https://www.inoutgreen.com/">In Out Green Interiors</a></li>
    <li><a href="{{ URL::to('/green-building-consultants/') }}/">Green Building Consultants</a></li>
    <li><a href="http://www.inoutgreenhomes.com/blogs/">Blogs</a></li>
    <li><a href="{{ URL::to('/contact-us/') }}/">Contact us</a></li>
    <li><a href="{{ URL::to('/we-are-hiring/') }}/">We Are Hiring</a></li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
<h4>Recent Posts</h4>
 @foreach($footerPost as $newposts)
          <p>
            @if($newposts->getImageAttribute() != '')
            <img src="{{$newposts->getImageAttribute()}}"  style="width: 60px; height: 49px; "alt=""/>
            @else
               <img src="{{asset('public/images/no-image.png')}}"  style="width: 60px; height: 49px; "alt=""/>
            @endif
             <a href="{{ URL::to('/blogs/') }}/{{$newposts->slug}}/">{{$newposts->title}}</a></p>
          @endforeach
         

</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
<h4>Contact Us</h4>
<p><i class="fa fa-map-marker"></i> <span>Delhi Office:<br/>
        Dwarka Mor, Dwarka-110075 </span> <span> Noida Office:<br/>
        FF 17, Sethi Arcade, Sec-76,<br/>
        Noida - 201301 </span> </p>
<p><i class="fa fa-phone"></i> <a href="tel:+91 {{$address->phone}}">+91 8010-179-239</a></p>
<p><i class="fa fa-envelope"></i> <a href="mailto:{{$address->email}}">inoutgreenhomes@gmail.com</a></p>
</div>
</div>
</div>
</div>
<div class="footer2">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ft"> <span>Copyright &copy; 2020 <a href="{{ URL::to('/') }}">In Out Green Homes</a>. All Rights Reserved</span> </div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ft">
<ul>
    <li><a target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
    <li><a target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
    <li><a target="_blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
    <li><a target="_blank" href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
    <li><a target="_blank" href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
    
</ul>
</div>
</div>
</div>
</div>
</footer>
<script src="{{asset('public/js/jquery.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/jquery-ui.js')}}"></script>
<script src="{{asset('public/js/bootstrap-select.js')}}"></script>
<!-- Import Owl carosel js-->
<script src="{{asset('public/js/owl.carousel.js')}}"></script>
<script src="{{asset('public/js/tab.js')}}"></script>
 <!-- start-smooth-scrolling -->
        <script type="text/javascript" src="{{asset('public/js/move-top.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/easing.js')}}"></script> 
        
<script src="{{asset('public/backend/js/jquery.validate.js')}}"></script> 

<!-- logo slider-->
<script>
$(document).ready(function () {
$('#Carousel').carousel({
interval: 5000
})
});
</script>
<script>
$(window).on('load', function () {
$('.selectpicker').selectpicker({
'selectedText': 'cat'
});
});
$(function () {
        $("#slider-range").slider({
        range: true,
        min: 1500000,
        max: 15000000,
        values: [150000, 150000000],
        change: function (event, ui){ searchData(); },
        slide: function (event, ui) {
            $("#amount").val("INR " + ui.values[ 0 ] + " - INR " + ui.values[ 1 ]);
          }
          
        });
        $("#amount").val("INR " + $("#slider-range").slider("values", 0) +
        " - INR " + $("#slider-range").slider("values", 1));
        });
$("#radioset").buttonset();
$("#radioset2").buttonset();
//            $("#accordion").accordion();
$("#controlgroup").controlgroup();

//            $(".fcInit").hover(function (){
//                $(".fcInit").removeClass("none");
//            },function({
//                $(".fcInit").addClass("none");
//            });

$(function() {
$(".feature_containers .sub a").hover(function() {
$(this).next(".wrapHvr").show("slow");
},function(){
$(this).next(".wrapHvr").hide("slow");
});
});
</script>


<script>
//            $(function(){
//               $(".elipDots .__readMore").click(function(){
//                   $(this).next(".descHidden").show("slow");
//               },function(){
//                   $(this).next(".descHidden").hide("slow");                   
//               });               
//           });
// tabbed content
//http://www.entheosweb.com/tutorials/css/tabs.asp
$(".tab_content").hide();
$(".tab_content:first").show();

/* if in tab mode */
$("ul.tabs li").click(function () {

$(".tab_content").hide();
var activeTab = $(this).attr("rel");
$("#" + activeTab).fadeIn();

$("ul.tabs li").removeClass("active");
$(this).addClass("active");

$(".tab_drawer_heading").removeClass("d_active");
$(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

/*$(".tabs").css("margin-top", function(){ 
return ($(".tab_container").outerHeight() - $(".tabs").outerHeight() ) / 2;
});*/
});
$(".tab_container").css("min-height", function () {
return $(".tabs").outerHeight() + 50;
});
/* if in drawer mode */
$(".tab_drawer_heading").click(function () {

$(".tab_content").hide();
var d_activeTab = $(this).attr("rel");
$("#" + d_activeTab).fadeIn();

$(".tab_drawer_heading").removeClass("d_active");
$(this).addClass("d_active");

$("ul.tabs li").removeClass("active");
$("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
});
/* Extra class "tab_last" 
to add border to bottom side
of last tab 
$('ul.tabs li').last().addClass("tab_last");*/
</script>
<script>
function switch_tabs($obj) {
$('.tabs-content-wrap').hide();
$('.nav-tabs a').removeClass('active');
var id = $obj.attr('href');
$(id).show().css('display', 'block');
$obj.addClass('active');
}
$('.nav-tabs a').click(function () {
switch_tabs($(this));
return false;
});
switch_tabs($('.nav-tabs a.active'));
</script>
<!-- builder /-->
<script>
$(document).ready(function () {
 $("#owl-demo8").owlCarousel({
                    navigation: false,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-grey'></i>",
                        "<i class='fa fa-chevron-right icon-grey'></i>"
                    ],
                    autoPlay: 8000,
                    items: 1,
                    itemsDesktop: [1199, 2],
                    itemsDesktopSmall: [979, 1]
                });
                $(".owl-pagination").remove();

              

});         


</script>
<script>
            $(function () {
                $('ul.link li').on('click', function () {
                    $(this).parent().find('li.active-link').removeClass('active-link');
                    $(this).addClass('active-link');
                });
            });

            //                  $(".more").click(function(){
            //                      console.log(this);
            //                      if($(this).text() == 'View More...' || $(this).text() == 'More...'){
            //                          $(this).text("less...").siblings(".complete").show();
            //                          $('.complete').show();
            //                      }else if($(this).text() == 'less...'){
            //                          $(this).text("More...").siblings(".complete").show();
            //                          $('.complete').hide();
            //                      }                      
            //                  }); 

            function showHideDivs(obj, classId) {
                if ($(obj).text() == 'View More...' || $(obj).text() == 'More...') {
                    $(obj).text("View less...").siblings(".complete").show();
                    $('.complete' + classId).show();
                } else if ($(obj).text() == 'View less...') {
                    $(obj).text("View More...").siblings(".complete").show();
                    $('.complete' + classId).hide();
                }
            }

        </script>
        
<!-- smooth-scrolling-of-move-up -->
@if($selected_tab == 'projects')
        <script type="text/javascript">
            $(document).ready(function () {

                var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear'
                };

                $().UItoTop({easingType: 'easeOutQuart'});

                  $(".moreBox").slice(0, 10).show();
                     
                    $("#loadMore").on('click', function (e) {
                        var totalItem = $('#totalItems').val();
                        var remainingItem = totalItem-10;
                        {{sizeof($projectsList)-10}}
                      e.preventDefault();
                      $(".moreBox:hidden").slice(0, 10).slideDown();
                      $("#loadMore").html('<a href="javascript:void(0)" class="contact_but">View '+remainingItem+' More Projects</a>')  
                      $('#totalItems').val(remainingItem);
                      if ($(".moreBox:hidden").length == 0) {
                        $("#loadMore").fadeOut('slow');
                      }
                    });
                    
                    $("#contact_validate").validate({
                            rules:{
                              required:{
                                required:true,
                                alphanumeric: true
                              },
                              email:{
                                required:true,
                                email: true
                              }, 
                              pnumber:{
                                required:true,
                                number:true,
                                maxlength:10
                              }
                            },
                        errorClass: "help-inline",
                        errorElement: "span",
                        highlight:function(element, errorClass, validClass) {
                          $(element).parents('.control-group').addClass('error');
                        },
                        unhighlight: function(element, errorClass, validClass) {
                          $(element).parents('.control-group').removeClass('error');
                          $(element).parents('.control-group').addClass('success');
                        }
                      }); 



            });
            $(function() {
                //----- OPEN
                $('[data-popup-open]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
                });
                //----- CLOSE
                $('[data-popup-close]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
                });
                });


        </script>
@endif        
       <script type="text/javascript">
  $(document).ready(function(){
    $("li.menu-list").hover(
        function() {
            $('.itm').css("display", "block");
        },
        function() {
            $('.itm').css("display", "none");
        });
    });
    </script>     <!-- //smooth-scrolling-of-move-up -->
</body>
</html>
