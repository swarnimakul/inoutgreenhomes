<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/favicon.ico')}}" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name='viewport' content='width=device-width, initial-scale=1'>
<title>{{$title}}</title>
@if(isset($pageData))
<meta name="description"  content="{{$pageData->meta_desc}}"/>
{!!$pageData->og_title!!}
{!!$pageData->og_image!!}
{!!$pageData->og_type!!}
{!!$pageData->og_site_name!!}
{!!$pageData->og_url!!}
@endif
<link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/style.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/serach-page.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/project-page.css')}}" type="text/css">
<link href="{{asset('public/css/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('public/css/owl.theme.css')}}" rel="stylesheet">
<link href="{{asset('public/css/jquery-ui.css')}}" rel="stylesheet">
<link href="{{asset('public/css/css-floating-form.css')}}" rel="stylesheet" media='all'/>

</head>
<body>