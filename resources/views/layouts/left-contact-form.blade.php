<style type="text/css">
  .help-block, .help-inline {
  color: red;
}
.success {color:green}
.error {color:red}
</style>
<script src="{{asset('public/js/jquery.min.js')}}"></script>


<script type="text/javascript">
    
   //if(projectId != null){
  function validateForm(id){
        var name = $('#name_'+id).val();
        var phone = $('#pnumber_'+id).val(),
        intRegex = /[0-9 -()+]+$/;
        var email = $('#email_'+id).val(),
        emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        $("#name_error_"+id).html("");
        $("#pnumber_error_"+id).html("");
        $("#email_error_"+id).html("");
          
      if(name == '' && phone == '' && email == ''){
          $("#name_error_"+id).html("This field is required");
          $("#pnumber_error_"+id).html("Please enter a valid Phone no.");
          $("#email_error_"+id).html("Please enter a valid Email");
       
      }
      if (name == '')
      {
          $("#name_error_"+id).html("This field is required");
           $('#name_'+id).focus();

          return false;
      }
//validate phone
       if((phone.length < 6) || (!intRegex.test(phone)) || phone == '')
      {
          $("#pnumber_error_"+id).html("Please enter a valid Phone no.");
           $('#pnumber_'+id).focus();
           return false;
      }
      //validate email
      if(!emailReg.test(email) || email == '')
      {
            $("#email_error_"+id).html("Please enter a valid Email");
          $('#email_'+id).focus();

           return false;
      }

      $('#contact_validate_'+id).submit();

}
</script>
<div class="col-xs-12 col-sm-12 col-md-12">
  @if(isset($projects))
          <form action="{{action('UserQueriesController@store')}}"  method="post" accept-charset="utf-8" name="contact_validate" id="contact_validate_{{$projects->id}}" novalidate="novalidate">
  @else
          <form action="{{ action('UserQueriesController@store') }}"  method="post" accept-charset="utf-8" name="contact_validate" id="contact_validate" novalidate="novalidate">

  @endif
              <input name="_token" type="hidden" value="{{ csrf_token() }}"/>             
              <h4>LET US HELP</h4>
              <p>We can contact you at a time convenient to you to help you on your search.</p>
              @if(isset($builder))
               <input name="builder_id" value="{{$builder->id}}" maxlength="50" required="" placeholder="Name*" class="com-marg"  autocomplete="off" tabindex="7" type="hidden">
              @endif
               @if(isset($projects))
                <input id="project_id" name="project_id" value="{{$projects->id}}" class="form-control input" placeholder="" required="" autocomplete="off" type="hidden">
               @endif
              @if(isset($projects))
              <input name="name" id="name_{{$projects->id}}"  value="" maxlength="50" required="" placeholder="Name*" class="com-marg"  autocomplete="off" tabindex="7" type="text">
              <span id="name_error_{{$projects->id}}" class="help-inline"></span>
              @else
               <input name="name" id="name"  value="" maxlength="50" required="" placeholder="Name*" class="com-marg"  autocomplete="off" tabindex="7" type="text">
              @endif
              @if(isset($projects))
              <input name="pnumber" id="pnumber_{{$projects->id}}" value="" maxlength="25" class="com-marg" tabindex="8" autocomplete="off" placeholder="Mobile*" required="" type="text">
              <span id="pnumber_error_{{$projects->id}}" class="help-inline"></span>
             
              @else
              <input name="pnumber" id="pnumber" value="" maxlength="25" class="com-marg" tabindex="8" autocomplete="off" placeholder="Mobile*" required="" type="text">
              @endif
              @if(isset($projects))
              <input name="email" id="email_{{$projects->id}}" value=""  placeholder="Email*" required="" class="com-marg" tabindex="9" autocomplete="off" type="text">
               <span id="email_error_{{$projects->id}}" class="help-inline"></span>
             
              @else
              <input name="email" id="email" value=""  placeholder="Email*" required="" class="com-marg" tabindex="9" autocomplete="off" type="text">
              
              @endif

              <select id="bs3Select" class="selectpicker form-control" name="msg">
                <option value="Call Me">Call Me</option>
                <option value="Email Me">Email Me</option>
              </select>
              <select id="bs3Select" class="selectpicker form-control" name="when">
                <option value="Today">Today</option>
                <option value="Tomorrow">Tomorrow</option>
                <option value="Next Week">Next Week</option>
              </select>
              <div class="submit">
                 @if(isset($projects))
                <input id="send_id" class="search_send_but" style="border:none;cursor:pointer;" alt="SEND"  onclick="validateForm({{$projects->id}})" tabindex="12" value="SEND" type="button">
                @else
                  <input id="send_id" class="search_send_but" style="border:none;cursor:pointer;" alt="SEND" tabindex="12" value="SEND" type="submit">
                
                @endif
              </div>
            </form>
          </div>