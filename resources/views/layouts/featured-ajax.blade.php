<link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/font-awesome.min.css')}}" type="text/css">

<link rel="stylesheet" href="{{asset('public/css/style.css')}}" type="text/css">

<link href="{{asset('public/css/owl.carousel.css')}}" rel="stylesheet">
   <link href="{{asset('public/css/owl.theme.css')}}" rel="stylesheet">
    

<!-- logo slider-->
@if(!empty($projects))
 <div id="owl-demo6" class="owl-carousel">

@foreach($projects as $project)
             <div class="item">
                <div class="iten-cont"> <img src="{{asset('public/upload/project_image/')}}/{{$project->project_image}}" alt=""/>
                  <div class="item-cont-top">
                    <div class="row">
                      <div class="col-md-7 col-sm-6 col-xs-12">
                        <h3 class="head-title">{{$project->name}}</h3>
                        <p><i class="fa-home fa"></i> {{$project->project_feature}}</p>
                      </div>
                      <div class="col-md-5 col-sm-6 col-xs-12">
                        <h4 class="price">@if(!empty($project->min_price))
                          {{$project->min_price}}
                          @else
                          0
                          @endif  </h4>
                      </div>
                    </div>
                  </div>
                  <div class="item-cont-link"> <a href="{{ URL::to($project->url) }}/"> More Details</a></div>
                  <div class="item-cont-bottom">
                    <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-6 com-marg font10 no-rpad"><i class="fa fa-area-chart"></i>
                      @if(!empty($project->size))
                        {{$project->size}}
                      @else
                      -
                      @endif</div>
                      <div class="col-md-3 col-sm-6 col-xs-6 com-marg font10"><i class="fa-bed fa"></i>@if(!empty($project->bed)) 
                        {{$project->bed}}
                      @else
                      0
                      @endif
                      </div>
                      <div class="col-md-2 no-lpad col-sm-6 com-marg col-xs-6 font10"><i class="fa-bathtub fa"></i> @if(!empty($project->bath))
                        {{$project->bath}}
                      @else
                      0
                      @endif
                      </div>
                      <div class="col-md-3 col-sm-6 col-xs-6 com-marg font10"><i class="fa-car fa"></i> @if(!empty($project->parking) && $project->parking!= 'no')
                      {{$project->parking}} 
                      @else
                      0
                      @endif</div>
                    </div>
                  </div>
                  <div class="item-cont-overlay"></div>
                </div>
              </div>
              @endforeach
              

              
            </div>
            @endif
            @if(sizeof($projects) == 0)
                <div style="margin-bottom: 50px; width: 1140px; height: 300px;">
                  <img src="{{asset('public/images/under-construction.png')}}" style="width: 1140px;" >
                </div> 
               
              @endif
            <script src="{{asset('public/js/jquery.min.js')}}"></script>

              <script src="{{asset('public/js/owl.carousel.js')}}"></script>
<script>
            $(document).ready(function() {
              $("#owl-demo6").owlCarousel({
                navigation: true,
                navigationText: [
                     "<i class='fa fa-chevron-left icon-grey'></i>",
                     "<i class='fa fa-chevron-right icon-grey'></i>"
                ],
              autoPlay: 8000,
              items : 3,
              itemsDesktop : [1199,3],
              itemsDesktopSmall : [979,3]
              });
              $(".owl-pagination").remove();

            });
    </script>