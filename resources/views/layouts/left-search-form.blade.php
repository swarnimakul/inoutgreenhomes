<script>
function searchData(){
	 var location = $('#location').val();
	 var category = $("input[name='category']:checked").val();
	 var type 	  = $('#type').val();
	 var phase 	  = $('#phase').val();
	 var project  = $('#project').val();
     var builder  = $('#builder').val();
     var amount  = $('#amount').val();
     var locality = [];
     $.each($("input[name='locality']:checked"), function(){            
                locality.push($(this).val());
     });
     var bedroom = [];
     $.each($("input[name='bedroom']:checked"), function(){            
                bedroom.push($(this).val());
     });
     var area = [];
     $.each($("input[name='area']:checked"), function(){            
                area.push($(this).val());
     });
	 var addurl ='';
	 //alert(type)

    if(addurl ==''){
    	var joinstr ="?";
    }
    //addurl = joinstr;
   //alert(amount);
	 if(location != "-1"){
		addurl =joinstr+"location="+location;
		joinstr = "&";
 	 }if(category != null){
      	addurl= addurl+joinstr+"category="+category;
      	joinstr = "&";
 	 }if(type != "-1"){
      	addurl= addurl+joinstr+"type="+type;
      	joinstr = "&";
 	 }if(phase != "-1"){
        addurl= addurl+joinstr+"phase="+phase;
        joinstr = "&";
 	 }if(builder != "-1"){
        addurl= addurl+joinstr+"builder="+builder;
        joinstr = "&";
     }if(project != "-1"){
        addurl= addurl+joinstr+"project="+project;
        joinstr = "&";
     }if(locality.length != 0){
        addurl= addurl+joinstr+"locality="+locality;
        joinstr = "&";
     }if(bedroom.length != 0){
        addurl= addurl+joinstr+"bedroom="+bedroom;
        joinstr = "&";
     }if(area.length != 0){
        addurl= addurl+joinstr+"area="+area;
        joinstr = "&";
     }if(amount != null){
        addurl= addurl+joinstr+"amount="+amount;
        joinstr = "&";
     }

      var Url = "{{url('/search')}}/searchProjects"+addurl;
	//alert(Url)
     $.ajax({
        url: Url,
        type: "get",
      	beforeSend: function()
        {
                $("#searchdata").html('loading...');
        },
        success: function(data){
        	//alert(data)
         $("#searchdata").html(data);
         //$("#location").html(lName+ " Projects");
      
         }
       });
}
</script>
<div class="container search-cont">
<div class="row">
<div class="col-xs-12 col-sm-3 col-md-3">
<div class="row search-cont-left">
    <form method="post" action="">
    	<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                  
<div class="col-xs-12 col-sm-12 col-md-12"><h5>Select City</h5>
    <select id="location" name="location" class="selectpicker form-control" data-live-search="true" onchange="searchData()">
        <option value="-1">All Cities</option>
        @foreach($locData as $loc)
        <option value="{{$loc->id}}" {{ ( $loc->id == $locId ) ? 'selected' : '' }}>{{$loc->location}}</option>
        @endforeach
       </select> 
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <label class="check-radio ta-radio fl mar-left10 font16">Residential 
        <input name="category" type="radio" value="2" onclick="searchData()"> 
        <span class="check-radio-indicater"></span>
    </label>
    <label class="check-radio ta-radio fl mar-left10 font16">Commercial 
        <input name="category" type="radio" value="1" onclick="searchData()"> 
        <span class="check-radio-indicater"></span>
    </label>

   
</div>
<div class="divider"></div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <select id="type" class="selectpicker form-control" onchange="searchData()">
        <option value="-1" selected="selected">All Residential</option>
       <option value="Apartment">Apartment</option>
                <option value="Residential Plot">Residential Plot</option>
                <option value="Independent Floor">Independent Floor</option>
                <option value="Villa">Villa</option>
                <option value="Studio">Studio</option>
                <option value="Row House">Row House</option>
                <option value="Independent House">Independent House</option>
                <option value="Farm House">Farm House</option>
                <option value="Town Suites">Town Suites</option>
                <option value="Township">Township</option>
                <option value="Bungalow">Bungalow</option>
                <option value="Retail Shop">Retail Shop</option>
                 <option value="Office Space">Office Space</option>
                 <option value="Plots">Plots</option>
                
    </select> 
</div>
<div class="divider"></div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="price-cont">
        <input type="text" id="amount"  readonly class="input">
        <div id="slider-range"  ></div>
    </div>
</div>
<div class="divider"></div>
<div class="col-xs-12 col-sm-12 col-md-12"> <h5>Select Phase</h5>
    <select id="phase" class="selectpicker form-control" onchange="searchData();">
        <option value="-1">Any Phase</option>
         <option value="Launch">Launch</option>
                  <option value="Under Construction">Under Construction</option>
                  <option value="Ready to Move">Ready to Move</option>
                   <option value="Depend draw">Depend draw</option>
                 <option value="Upcoming Projects">Upcoming Projects</option>
                 <option value="Pre Launch">Pre Launch</option>
                 
    </select>
</div> 
<div class="col-xs-12 col-sm-12 col-md-12">
    <h5>BHK <small>(No. of Bedrooms)</small></h5>
    <label class="check-radio ta-checkbox fl mar-left10 font16">01 
        <input name="bedroom" value="1" onclick="searchData()" type="checkbox">
        <span class="check-radio-indicater"></span>
    </label>
    <label class="check-radio ta-checkbox fl mar-left10 font16">02 
        <input name="bedroom" value="2" onclick="searchData()" type="checkbox">
        <span class="check-radio-indicater"></span>
    </label>
    <label class="check-radio ta-checkbox fl mar-left10 font16">03 
        <input name="bedroom" value="3"  onclick="searchData()" type="checkbox">
        <span class="check-radio-indicater"></span>
    </label>
    <label class="check-radio ta-checkbox fl mar-left10 font16">4+ 
        <input name="bedroom" value="4" onclick="searchData()" type="checkbox">
        <span class="check-radio-indicater"></span>
    </label> 
</div> 
<div class="col-xs-12 col-sm-12 col-md-12">
        <select id="builder" class="selectpicker form-control" data-live-search="true" onchange="searchData();">
                <option value="-1">Any Builder</option>
                @foreach($builderList as $builders)
                 <option value="{{$builders->id}}">{{$builders->name}}</option>
                @endforeach
        </select>
         <select id="project" class="selectpicker form-control" data-live-search="true" onchange="searchData();">
                <option value="-1">Any Project</option>
                @foreach($dropDownProjects as $dropDownProject)
                 <option value="{{$dropDownProject->id}}">{{$dropDownProject->name}}</option>
                @endforeach
          </select>

</div>
<div class="divider"></div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="row">
        <div class="panel-group" id="accordion">
              <div class="panel panel-default" id="toggle1">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Area</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body"> 
                            <label class="check-radio ta-checkbox font16">300 sq.ft. - 500 sq.ft.
                                <input name="area" type="checkbox" value="300 sq.ft. - 500 sq.ft." onclick="searchData()" >
                                <span class="check-radio-indicater"></span>
                            </label>
                            <label class="check-radio ta-checkbox font16">500 sq.ft. - 700 sq.ft.
                                <input name="area" type="checkbox" value="500 sq.ft. - 700 sq.ft." onclick="searchData()" >
                                <span class="check-radio-indicater"></span>
                            </label>
                            <label class="check-radio ta-checkbox font16">700 sq.ft. - 900 sq.ft.
                                <input name="area" type="checkbox"  value="700 sq.ft. - 900 sq.ft." onclick="searchData()">
                                <span class="check-radio-indicater"></span>
                            </label>
                            <label class="check-radio ta-checkbox font16">900 sq.ft. - 1100 sq.ft.
                                <input name="area" type="checkbox"  value="900 sq.ft. - 1100 sq.ft." onclick="searchData()">
                                <span class="check-radio-indicater"></span>
                            </label>
                            <label class="check-radio ta-checkbox font16">1100 sq.ft. - 1300 sq.ft.
                                <input name="area" type="checkbox"  value="1100 sq.ft. - 1300 sq.ft." onclick="searchData()">
                                <span class="check-radio-indicater"></span>
                            </label>
                            <label class="check-radio ta-checkbox font16">1300 sq.ft. - 1500 sq.ft.
                                <input name="area" type="checkbox"  value="1300 sq.ft. - 1500 sq.ft." onclick="searchData()">
                                <span class="check-radio-indicater"></span>
                            </label>
                            <a href="#" class="fr">More </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default" id="toggle2">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed"  data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Locality</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            @foreach($locality as $l)
                            <label class="check-radio ta-checkbox font16">{{$l->locality}}
                                <input name="locality"  value="{{$l->locality}}" type="checkbox" onclick="searchData()">
                                <span class="check-radio-indicater"></span>
                            </label>
                            @endforeach
                            <a href="#" class="fr">More </a> 
                        </div>
                    </div>
                </div>
        </div> 
    </div>
</div>
</form>

     @include('layouts.left-contact-form')
                 <div class="clearfix">&nbsp;</div>
                        </div> 
                    </div>