
<style type="text/css">/* Outer */
.popup {
width:100%;
height:100%;
display:none;
position:fixed;
top:0px;
left:0px;
background:rgba(0,0,0,0.75);
z-index:1031;
}
/* Inner */
.popup-inner {
max-width:700px;
width:90%;
padding:40px;
position:absolute;
top:50%;
left:50%;
-webkit-transform:translate(-50%, -50%);
transform:translate(-50%, -50%);
box-shadow:0px 2px 6px rgba(0,0,0,1);
border-radius:3px;
background:#fff;
z-index:1031;
}
/* Close Button */
.popup-close {
width:30px;
height:30px;
padding-top:4px;
display:inline-block;
position:absolute;
top:0px;
right:0px;
transition:ease 0.25s all;
-webkit-transform:translate(50%, -50%);
transform:translate(50%, -50%);
border-radius:1000px;
background:rgba(0,0,0,0.8);
font-family:Arial, Sans-Serif;
font-size:20px;
text-align:center;
line-height:100%;
color:#fff;
}
.popup-close:hover {
-webkit-transform:translate(50%, -50%) rotate(180deg);
transform:translate(50%, -50%) rotate(180deg);
background:rgba(0,0,0,1);
text-decoration:none;
}
    /* Outer */
.popup {
width:100%;
height:100%;
display:none;
position:fixed;
top:0px;
left:0px;
background:rgba(0,0,0,0.75);
z-index:1031;
}
/* Inner */
.popup-inner {
max-width:700px;
width:90%;
padding:40px;
position:absolute;
top:50%;
left:50%;
-webkit-transform:translate(-50%, -50%);
transform:translate(-50%, -50%);
box-shadow:0px 2px 6px rgba(0,0,0,1);
border-radius:3px;
background:#fff;
z-index:1031;
}
/* Close Button */
.popup-close {
width:30px;
height:30px;
padding-top:4px;
display:inline-block;
position:absolute;
top:0px;
right:0px;
transition:ease 0.25s all;
-webkit-transform:translate(50%, -50%);
transform:translate(50%, -50%);
border-radius:1000px;
background:rgba(0,0,0,0.8);
font-family:Arial, Sans-Serif;
font-size:20px;
text-align:center;
line-height:100%;
color:#fff;
}
.popup-close:hover {
-webkit-transform:translate(50%, -50%) rotate(180deg);
transform:translate(50%, -50%) rotate(180deg);
background:rgba(0,0,0,1);
text-decoration:none;
}
</style>



   @if(session('info')=='fail')
                         <div class="alert alert-danger">Message Not Send</div>
                         @endif
                         @if(session('info')=='success')
                          <div class="alert alert-success">Thank you for your Interest. We will contact you Soon.</div>
                         @endif
                @php $display =""; @endphp
                @if(!empty($projectsList))
                  @foreach($projectsList as $key=>$projects)
                         @if($key >=10)
                                @php 
                                $display ="none"; 
                                     
                                @endphp
                         @endif     
              <div class="property-cont moreBox" style="display: {{$display}}">
                            <div class="property-header">
                                <h4 class="fl"><i class="fa fa-rupee"></i><a href="{{ URL::to($projects->url) }}/"> {{$projects->min_price}} {{$projects->project_feature}}, {{$projects->project_name}} in 
                                  @if($projects->locality != '')
                                  {{$projects->locality}}
                                  @else
                                   {{$projects->location}}
                                  @endif </a> </h4> 
                                <span class="dealer-logo"> <img src="{{url('/public/upload/project_logo'.'/'.$projects->logo)}}" alt=""/></span> </span> 
                            </div>

                            <div class="srpWrap">
                                <div class="col-md-3 col-sm-3 col-xs-12 com-marg">
                                    <div class="row">
                                    <div class="property-pic" style="height: 140px;">
                                        @if($projects->project_image!='' )
                                 <img src="{{url('/public/upload/project_image'.'/'.$projects->project_image)}}" style=" width: 189px; height: 138px;" alt=""/> @else
                                 <img src="{{URL::to('')}}/public/upload/project_image/no-image.png" style=" width: 189px; height: 138px;"  alt=""/>
                                  @endif
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <div class="row"> 
                                        <p>Super built-up Area: <b>{{$projects->size}}</b> 
                                        <p>Highlight: {{$projects->phase}} / {{$projects->category}}
                                           @if(!empty($projects->project_feature))
                                            / {{$projects->project_feature}}
                                           @endif
                                           @if(!empty($projects->project_type) && $projects->project_type!= $projects->project_feature)
                                            / {{$projects->project_type}}
                                           @endif
                                           </p>
                                        <div class="fl f12 wBr srpDataWrap" >
                                            <b>Description :</b>
                                            <span class="srpDes">{!!substr($projects->description,0,200)!!}</span>
                                            </div>
                                        <div class="rel clr">
                                            <div class="fl mt13 mr13">Amenities:</div>
                                            <div class="feature_containers">
                                                <div class="sub">
                                                       <div class="iconDiv fc_icons fcInit">
                                                            <i class="i4" value="Reserved Parking">&nbsp;</i>
                                                            <i class="i5" value="Feng Shui / Vaastu Compliant">&nbsp;</i>
                                                            <i class="i21" value="Lift(s)">&nbsp;</i>
                                                            <i class="i9" value="Security Personnel">&nbsp;</i>
                                                            <i class="i24" value="Water Storage">&nbsp;</i>
                                                        </div>
                                                        <!--<div class="fl mt13"><b>+ 3 more</b></div>-->
                                                 
                                                </div>
                                            </div>  
                                        </div>                                        
                                    </div>                                        
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 mt13">
                                    <div class="row"> 
                                        
                                      <a data-popup-open="popup-{{$projects->id}}" href="#" class="contact_but btn">Contact Us</a>

                                    </div>
                                </div>
                                  
                                  
                            </div> 
                        </div>
                        <div class="popup" data-popup="popup-{{$projects->id}}">
                                            <div class="popup-inner">
                                                    @include('layouts.left-contact-form')
                                            <p><a data-popup-close="popup-{{$projects->id}}" href="#">Close</a></p>
                                            <a class="popup-close" data-popup-close="popup-{{$projects->id}}" href="#">x</a>
                                           </div>
                                        </div>
                       @endforeach
                       @if(sizeof($projectsList) > 10)
                       <input type="hidden" id="totalItems" value="{{sizeof($projectsList)-10}}">
<div class="col-md-12 text-center" id="loadMore" style="margin-left: 320px;">
                            <a href="javascript:void(0)" class="contact_but">View {{sizeof($projectsList)-10}} More Projects</a>  
                        </div>
                       @endif
                      @endif 
                       @if(sizeof($projectsList) ==0 )
                         <h3>No Data Found</h3>
                        
                       @endif
                        
                      
<script type="text/javascript">
     $(function() {
                //----- OPEN
                $('[data-popup-open]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
                });
                //----- CLOSE
                $('[data-popup-close]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
                });

                 $(".moreBox").slice(0, 10).show();
                     
                    $("#loadMore").on('click', function (e) {
                        var totalItem = $('#totalItems').val();
                        var remainingItem = totalItem-10;
                        {{sizeof($projectsList)-10}}
                      e.preventDefault();
                      $(".moreBox:hidden").slice(0, 10).slideDown();
                      $("#loadMore").html('<a href="javascript:void(0)" class="contact_but">View '+remainingItem+' More Projects</a>')  
                      $('#totalItems').val(remainingItem);
                      if ($(".moreBox:hidden").length == 0) {
                        $("#loadMore").fadeOut('slow');
                      }
                    });
      });



</script>