<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}" type="text/css">
<style>
body, html {
    height: 100%;
    margin: 0;
}

.bgimg {
    background-image: url('{{url("/public")}}/images/error.gif');
    height: 100%;
    background-position: center;
    background-size: cover;
    position: relative;
    color: white;
    font-family: "Courier New", Courier, monospace;
    font-size: 25px;
}

.topleft {
    position: absolute;
    top: 0;
    left: 16px;
}

.bottomleft {
    position: absolute;
    bottom: 0;
    left: 16px;
}

.middle {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-align: center;
}

hr {
    margin: auto;
    width: 40%;
}
</style>
</head>
<body>

<div class="bgimg">
  <div class="topleft">
    <p>Logo</p>
  </div>
  <div class="middle">

  </div>
  <div class="bottomleft">
    <a href="{{url('/')}}" class="btn btn-info">Back to Page</a>
  </div>
</div>

</body>
</html>