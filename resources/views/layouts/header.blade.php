
<style>
.dropdown-menu a{
    padding: 9px 22px;
    background: #fff;
    color: #333;
    text-transform: uppercase;
    font-family: calibri;
    font-size: 14px;
    font-weight: 400;

}
.menu-list{
  display: none; 
}
.navbar-right .dropdown-menu {
 position: relative; 
 background-color: #fff;
 padding-bottom: 0px;
 padding-top: 0px;
 padding-left: 0px;
 padding-right: 0px;


}
.navbar-right .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .dropdown-menu::before{
 background:url('{{url("/")}}/public/images/menu_icon.png') no-repeat right;;
  color:#fff;
}
.navbar-default .navbar-nav>.open>a:hover{
 background: rgba(10, 151, 11);
 color:#333;
}
.navbar-right .navbar-nav>li>.dropdown-menu{
      border: 0 solid grey;
  
}
.navbar-right .dropdown-menu>li>a{
  font-size: 14px;
      font-weight: 400;
         color: #333;
         border-top: 1px solid #09abd0;
          padding: 10px 17px;}
.dropdown-menu>li>a:hover{
  color:#333;
  
}   
.navbar-right .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
  background: rgba(10, 151, 11);
   border-top: rgba(10, 151, 11);

} 
.navbar-default .navbar-nav>li>a:focus{
color:#fff;
}

</style>

<header>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand logo" href="{{url('/')}}"><img src="{{asset('public/images/logo.png')}}" alt=""/></a> </div>
<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
        <li ><a href="{{ URL::to('/') }}/">Home</a></li>
        <li class="{{ ( $selected_tab == 'about-us' ) ? 'active' : '' }}"><a href="{{ URL::to('/about-us/') }}/">About Us</a></li>
        <li class="dropdown menu-list">
               <a href="javascript:void(0)" class="dropbtn">In Out Green</a>
               <div class="drop-content">
                <a  href="{{ URL::to('/inout-green-homes/') }}/">IOG Homes</a>
                <a href="{{ URL::to('/inout-green-interiors/') }}/">IOG Interiors</a>
                </div> 
              </li>  
              <li><a href="{{ URL::to('/green-consultants/')}}/">Green Consultants</a></li>
              <li><a href="{{ URL::to('/blogs/') }}/">Blogs</a></li>
              <li><a href="{{ URL::to('/contact-us') }}/">Contact Us</a></li>
  </ul>
          </div>
          <!--/.nav-collapse -->
        </nav>
      </div>
    </div>
  </div>
</header>
<section class="banner-intern"> <img src="{{url('/')}}/public/images/banner.jpg" width="100%" alt=""/>
  <div class="overlay">
    <div class="container">
    
    </div>
  </div>
  
</section>