
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body new-pop">
        <div style="margin:0px auto;">
          <div class="container-fluid text-center display" style="background-color: #f9fff6bf" id="registerBoxLoader">
            <div class="row">
              <div class="col-md-12" id="registerBoxLoaderMsg" style="padding: 10px 0px 0px 0px;"></div>
              <div class="col-md-12" style="margin: 5px 0px 15px 0px;">
                <img src="{{ asset('public/images/ajax-loader.gif') }}" />
              </div>
            </div>
          </div>


          <div class="login-popup-order">
            <div class="h-login-dialog">
              <div class="closepop" aria-hidden="true" id="closemodal" data-dismiss="modal"></div>
              <!--login register box -->
              <div class="log-registerBox">
                <div class="nav-signin-up">
                  <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#signIn">Sign In</a></li>
                    <li><a data-toggle="tab" href="#signUp">Sign Up</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="signIn" class="tab-pane fade in active"> 
                      <!--LOGIN PANEL -->
                      <div class="col-md-12 form-signup-in">
                        <form class="form-label form-css-label" name="">
                          <!--<div class="groupsign">
                            <input id="Email/phone No." name="Email/phone No." type="text" autocomplete="off" required />
                            <label for="Email/phone No.">Email/phone No.</label>
                          </div>
                          <div class="groupsign">
                            <input id="Password" name="Password" type="text" autocomplete="off" required />
                            <label for="Password">Password</label>
                          </div>-->
                          <div class="form-group">
                            <label for="signInEmail">Email</label>
                            <input type="text" class="form-control" id="signInEmail" placeholder="Enter email address" />
                          </div>
                          <div class="form-group">
                            <label for="signInPassword">Password</label>
                            <input type="password" class="form-control" id="signInPassword" placeholder="Password" />
                          </div>
                          <div class="rememberme">
                            <div class="leftsave">
                              <input id="rememberme" value="1" type="checkbox" tabindex="3" class="checkout">
                              <label for="rememberme"><span style="font-size:12px;">Keep me logged in</span></label>
                            </div>
                            <div class="frgtpw"> <a href="forgotpwd">Forgot Password?</a> </div>
                          </div>
                          <div class="clear"></div>
                          <div class="login_btns">
                            <input type="button" class="login_btns" value="Sign In" name="login" id="signInBtn">
                          </div>
                          <div class="clear"></div>
                          <div class="signUp-social">
                            <div class="option_acnt"><span><em>or login using</em></span></div>
                            <div class="fb-login">
                              <button type="button" class="btn fb-btn">Facebook</button>
                            </div>
                            <div class="gp-login">
                              <button type="button" class="btn gp-btn" >Google Plus</button>
                            </div>
                          </div>
                          <div class="clear"></div>
                        </form>
                      </div>
                      <!--LOGIN PANEL --> 
                    </div>
                    <div id="signUp" class="tab-pane fade"> 
                      <!--SIGNUP PANEL -->
                      <div class="col-md-12 form-signup-panel">
                        <form class="form-label form-css-label">
                          <div id="registerBox1">
                            <div class="form-group">
                              <label for="fullname">Full Name</label>
                              <input type="text" class="form-control" id="fullname" placeholder="Enter full name" required>
                            </div>
                            <div class="form-group">
                              <label for="email">Email</label>
                              <input type="text" class="form-control" id="email" placeholder="Enter email" required>
                              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                              <label for="phone">Phone Number</label>
                              <input type="text" class="form-control" id="phone" placeholder="Enter phone no" maxlength="12" required>
                              <small id="phoneHelp" class="form-text text-muted">We'll never share your phone no. with anyone else.</small>
                            </div>
                            <div class="register_btns">
                              <input type="button" class="register_btns" value="Sign Up" id="sendOtp">
                            </div>
                          </div>
                          <div id="registerBox2" class="display" style="margin-top: 20px;">
                            <div class="form-group text-center">
                              <input type="text" class="form-control" id="otp" placeholder="Enter OTP" required>
                            </div>
                            <div class="form-group text-center">
                              <button type="button" class="btn btn-warning" id="resendOtp">Resend OTP</button>
                              <button type="button" class="btn btn-info" id="verifyOtp">Verify OTP</button>
                            </div>
                          </div>
                          <div class="clear"></div>
                          <div class="form-group">
                            <div class="termsofuse"> By Signing Up you agree to our <a href="#" target="_blank">Privacy Policy and T&amp;C</a>. </div>
                          </div>
                        </form>
                      </div>
                      <!--SIGNUP PANEL ENDS --> 
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
              <!--login register box ends --> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
