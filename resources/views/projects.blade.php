@include('layouts.master')
<link href="{{asset('public/css/popup.css')}}" rel="stylesheet" media='all'/>

@include('layouts.header')
<section class="seacrh-content container-fluid">

@include('layouts.left-search-form')
<!-- projects -->


<div class="col-xs-12 col-sm-9 col-md-9" id="searchdata">
    @if(session('info')=='fail')
                         <div class="alert alert-danger">Message Not Send</div>
                         @endif
                         @if(session('info')=='success')
                          <div class="alert alert-success">Thank you for your Interest. We will contact you Soon.</div>
                         @endif
                         @php $display =""; @endphp
                         @if(!empty($projectsList))
                         @foreach($projectsList as $key=>$projects)
                         @if($key >=10)
                                @php 
                                $display ="none"; 
                                @endphp
                         @endif           

                        <div class="property-cont moreBox" style="display: {{$display}}">
                            <div class="property-header">
                                <h4 class="fl"><i class="fa fa-rupee"></i><a href="{{ URL::to($projects->url) }}/"> {{$projects->min_price}} {{$projects->project_feature}}, {{$projects->project_name}} in 
                                  @if($projects->locality != '')
                                  {{$projects->locality}}
                                  @else
                                   {{$projects->location}}
                                  @endif </a> </h4> 
                                <span class="dealer-logo"> <img src="{{url('/public/upload/project_logo'.'/'.$projects->logo)}}" alt=""/></span> </span> 
                            </div>

                            <div class="srpWrap">
                                <div class="col-md-3 col-sm-3 col-xs-12 com-marg">
                                    <div class="row">
                                    <div class="property-pic" style="height: 140px;">
                                        @if($projects->project_image!='' )
                                 <img src="{{url('/public/upload/project_image'.'/'.$projects->project_image)}}" style=" width: 189px; height: 138px;" alt=""/> @else
                                 <img src="{{URL::to('')}}/public/upload/project_image/no-image.png" style=" width: 189px; height: 138px;"  alt=""/>
                                  @endif
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <div class="row"> 
                                        <p>Super built-up Area: <b>{{$projects->size}}</b> </p>
                                        <p>Highlight: {{$projects->phase}} / {{$projects->category}}
                                           @if(!empty($projects->project_feature))
                                            / {{$projects->project_feature}}
                                           @endif
                                           @if(!empty($projects->project_type) && $projects->project_type!= $projects->project_feature)
                                            / {{$projects->project_type}}
                                           @endif
                                           </p>
                                        <div class="fl f12 wBr srpDataWrap">
                                            <b>Description :</b>
                                            <span class="srpDes">{!!substr($projects->description,0,200)!!}</span>
                                        </div>
                                        <div class="rel clr">
                                            <div class="fl mt13 mr13">Amenities:</div>
                                            <div class="feature_containers">
                                                <div class="sub">
                            @php $amenities = ProjectsController::getAmenities($projects->id); @endphp
                                              @if(!empty($amenities)) 
                                              @foreach($amenities as $key=>$amenity)
                                                <div class=" fcInit" style="width: 54px; height: 24px;">
                                             
                                                <img src="{{asset('public/images/amenities/')}}/icons/{{$amenity}}.png"  title="{{$amenity}}" alt="{{$amenity}}" style="height: 20px;width: 50px;"  class="img-responsive" />
                                                </div>
                                               @endforeach
                                              @endif
                                              @if(sizeof($amenities) == 0) 
                                                Not available
                                              @endif
                                             
                                                        <!--<div class="fl mt13"><b>+ 3 more</b></div>-->
                                                                                                  
                                                </div>
                                            </div>  
                                        </div>                                        
                                    </div>                                        
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 mt13">
                                    <div class="row"> 
                                        
                                      <a data-popup-open="popup-{{$projects->id}}" href="#" class="contact_but btn">Contact Us</a>

                                    </div>
                                </div>
                                  
                                  
                            </div> 
                        </div>
                        <div class="popup" data-popup="popup-{{$projects->id}}">
                                            <div class="popup-inner">
                                                    @include('layouts.left-contact-form')
                                            <p><a data-popup-close="popup-{{$projects->id}}" href="#">Close</a></p>
                                            <a class="popup-close" data-popup-close="popup-{{$projects->id}}" href="#">x</a>
                                           </div>
                                        </div>
                       @endforeach
                       @if(sizeof($projectsList) > 10)
                       <input type="hidden" id="totalItems" value="{{sizeof($projectsList)-10}}">
                        <div class="col-md-12 text-center" id="loadMore" >
                            <a href="javascript:void(0)" class="contact_but">View {{sizeof($projectsList)-10}} More Projects</a>  
                        </div>
                        @endif
                       @endif 
                     

</div>
</div> 
</div>

</section>
@include('layouts.footer')