<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/favicon.ico')}}" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>@if(isset($projectInfo) && $projectInfo->meta_tag !=''){{$projectInfo->meta_tag}}@endif</title>
            @if(isset($projectInfo)  && $projectInfo->og_title !='')
            {!!$projectInfo->og_title!!}
            @endif
            @if(isset($projectInfo)  && $projectInfo->og_image !='')
            {!!$projectInfo->og_image!!}
            @endif
            @if(isset($projectInfo)  && $projectInfo->og_type !='')
            {!!$projectInfo->og_type!!}
            @endif
            @if(isset($projectInfo)  && $projectInfo->og_site_name !='')
            {!!$projectInfo->og_site_name!!}
            @endif
            @if(isset($projectInfo)  && $projectInfo->og_url !='')
            {!!$projectInfo->og_url!!}
            @endif
        @if(isset($projectInfo)  && $projectInfo->meta_desc !='')
         <meta name="description"  content="{{$projectInfo->meta_desc}}"/>
        @endif
        @if(isset($projectInfo)  && $projectInfo->meta_keyword !='')
         <meta name="keywords" content="{{$projectInfo->meta_keyword}}" />
        @endif
        <link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('public/css/font-awesome.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('public/css/style.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('public/css/serach-page.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('public/css/tab.css')}}" type="text/css">
        <link href="{{asset('public/css/owl.carousel.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/owl.theme.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/jquery-ui.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('public/css/jquery.fancybox.css')}}">
 <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet"> -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
        <style>
            .owl-carousel .owl-wrapper {
                display: none;
                position: relative;
                -webkit-transform: translate3d(0px, 0px, 0px);
                height: 200px;
            }
           
            .owl-carousel .owl-item {padding: 0 5px;} 
            .floatRelative{position: relative;top:1386px;}
             @media screen and (max-width:988px){
           .floatRelative{position: relative;top:0px;}
            }
            #geocomplete { width: 200px}

            .map_canvas { 
                    height: 100%; 
                    width: 100%; 
                    position: absolute;
                    top: 0px; 
                    left: 0px; background-color: rgb(229, 227, 223);

                }
        </style>
        
    </head>
    <body>
        <header style="background:rgba(0,0,0,0.5);">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                <a class="navbar-brand logo" href="{{ URL::to('/') }}"><img src="{{asset('public/images/logo.png')}}" alt=""/></a> </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                  <li ><a href="{{ URL::to('/') }}">Home</a></li>
                                  <li class="{{ ( $selected_tab == 'about-us' ) ? 'active' : '' }}"><a href="{{ URL::to('/about-us') }}">About Us</a></li>
                                       <li class="dropdown menu-list {{ ( $selected_tab == 'land-pooling-policy' ) ? 'active' : '' }}">
      

         
       <a href="javascript:void(0)" class="dropbtn">Land Pooling Policy</a>

    <div class="drop-content">
     <a  href="{{ URL::to('/land-pooling-policy/') }}/">Land Pooling Policy</a>
    <a href="{{ URL::to('/land-pooling-policy-concept/') }}/">Land Pooling Concept </a>
    <a href="{{ URL::to('/l-zone-timeline/') }}/" >Land Pooling Timeline</a>
    </div>         
        </li>
                                  <li class="{{ ( $selected_tab == 'projects' ) ? 'active' : '' }}"><a href="{{ URL::to('/projects') }}">Projects</a></li>
                                  <li class="{{ ( $selected_tab == 'blogs' ) ? 'active' : '' }}"><a href="{{ URL::to('/blogs/') }}/">Blogs</a></li>
                                  <li class="{{ ( $selected_tab == 'contact-us' ) ? 'active' : '' }}"><a href="{{ URL::to('/contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <section class="seacrh-content container-fluid" style="background: #f7f7f7">
            <div class="row">
                <div class="search-details-banner">
                    
                    <img src="{{asset('public/upload/project_image/')}}/{{$projectInfo->project_image}}"/>
                    
                    <div class="container pos-rel">
                        <div class="pos-abs col-md-12">
                            <div class="col-md-4 search-details-titlebox">
                            <h1>@if(!empty($projectInfo)){{$projectInfo->name}} @endif</h1>
                          <h2 class="locationh2"><i class="fa-map-marker fa font22"></i> {{$loc}}</h2>
                          <p>RERA Regd NO: @if(!empty($projectInfo) && $projectInfo->rera_id != ''){{$projectInfo->rera_id}} @else NA @endif </p>
                         </div>
                        <div class="col-md-4 search-details-titlebox right">
                            <h3><i class="fa fa-rupee"></i>@if(!empty($projectDetail)) {!!$projectDetail->price!!}@endif <span class="font24 text-normal">Onwards</span></h3><h2 class="locationh2"> @if(!empty($projectInfo)){{$projectInfo->project_feature}} @endif @if(!empty($projectInfo)){{$projectInfo->project_type}} @endif</h2> @if(!empty($projectDetail) && $projectDetail->web_url) 
                            <p>@php $url=''; $urls = preg_replace("(^https?://)", "", $projectDetail->web_url );if(substr($urls, -1) == '/'){ $url = substr($urls, 0, -1);}else{ $url=$urls;} @endphp Website :<a class="website" href="{{$projectDetail->web_url}}" target="_blank"> {{$url}}</a></p> @endif

                        </div>
                        <div class="clearix"></div>
                        </div>                        
                    </div>
                </div>
                 <div class="clearix"></div>
                <div class="top-link" id="linkbar">
                    <div class="container">
                        <ul class="link">
                            <li class="active"><a href="#overview">Overflow</a></li>
                            <li><a href="#amenities">Amenities</a></li>
                            <li><a href="#gallery">Gallery</a></li>
                            @if(!empty($priceInfo))
                            <li><a href="#units">Units/Prices</a></li>
                            @endif
                            <li><a href="#location">Location</a></li>
                            @if($sizeOfpriceInfo == 1 )
                   
                            <li><a href="#payment">Payment Plan</a></li>
                            @endif
                            <li><a href="#developer">Developers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container details-cont content-details">
                <div class="col-md-9 col-sm-12 col-xs-12 com-marg">
                    <div id="overview" class="about-desc bgWhite clearfix " style="">
                        <h3><strong>Project Overview</strong></h3>
                        <hr style="">
                        <ul class="circleList row">
                            <li class="col-md-4 col-sm-4">
                                <div class="title"><i class="fa fa-key fvcolor" aria-hidden="true"></i> Possesion Date</div>
                                <hr style=" margin:0px">
                                <p class="project">@if( $projectInfo->possasion_date !=''){{$projectInfo->possasion_date}} @else  Not Available @endif</p>
                                </li>
                            <li class="col-md-4 col-sm-4">
                                <div class="title"><i class="fa fa-building-o fvcolor" aria-hidden="true"></i> No. of Towers</div>
                                <hr style=" margin:0px">
                                <p class="project">
                                    @if( $projectInfo->numb_towers !='')
                                    {{$projectInfo->numb_towers}}
                                    @else
                                    -
                                    @endif</p>
                            </li>
                            <li class="col-md-4 col-sm-4">
                                <div class="title"><i class="fa fa-calendar fvcolor" aria-hidden="true"></i> Project Type</div>
                                <hr style=" margin:0px">
                                <p class="project">{{$projectInfo->phase}} </p>
                            </li>
                        </ul>
                         @if(isset($projectDetail->description))
                         <div id="intro" class="intro">
                         @php
                            $end='';
                            $s='';
                            try{
                                $end=strpos("$projectDetail->description",'.',200)+1;
                                $s=substr("$projectDetail->description",0,$end);
                                $start=strripos($s, ".")+1;
                            }catch(Exception $e){
                                $s=substr("$projectDetail->description",0);
                                $start=strripos($s, ".")+1;
                            }
                            $start=strripos($s, ".")+1;
                         @endphp
                            <p style="display: block;text-align:justify; ">{!!$s!!} </p>
                            @if(!empty(substr("$projectDetail->description",$start+1)))
                            <div class="complete1" style="display: none;text-align:justify;">{!!substr("$projectDetail->description",$start)!!}</div>
                            <p class="more" onClick="showHideDivs(this, 1)"><a style="color:#174e91" href="javascript:void(0)">View More...</a></p>
                            @endif
                        </div>
                        @endif
                    </div>
                    <div id="amenities" class="about-desc bgWhite clearfix " style="">
                        <h3><strong>Amenities</strong></h3>
                        <hr>
                        <ul id="amin_list" class="circleList row">
                            @php $display =""; 
                                $complete2="";
                            @endphp
                            @if(!empty($amenities))
                            
                            @foreach($amenities as $key=>$amenity)
                            @if($key >=6)
                                @php $display ="none"; 
                                     $complete2 ="complete2";
                                @endphp
                            @endif
                           <li class="col-md-2 col-sm-2 col-xs-3 {{$complete2}}" style="display: {{$display}}" >
                            <center>
                                <img src="{{asset('public/images/amenities/')}}/{{$amenity}}.png"  title="{{$amenity}}" alt="{{$amenity}}"  class="img-responsive" />
                            </center>
                            <div class="title1 hidden-xs-down" align="center">{{$amenity}}</div>
                            </li>
                            @endforeach
                           @endif
                            @if(sizeof($amenities) == 0) 
                                                Not available
                                              @endif
                        </ul>
                         
                          @if(!empty($amenities) && count($amenities)>=6)

                        <p class="more" onClick="showHideDivs(this, 2)"><a href="javascript:void(0)">View More...</a></p>
                        @endif

                    </div>
                    <div id="gallery" class="bgWhite clearfix" style="margin-top:15px">
                        <h3><strong>Gallery</strong></h3>
                        <hr style="">
                        <ul class="tab">
                            <li class="active" rel="tab1">Projects</li>
                            <li rel="tab2">Siteplan</li>
                            <li rel="tab3">Floor Plan</li>
                            <li rel="tab4">Payment Plan</li>
                            <li rel="tab5">Brochure </li>
                        </ul>
                        <div class="tab_container">
                            <h3 class="d_active tab_drawer_heading" rel="tab1">Projects</h3>
                            <div id="tab1" class="tab_content"> 
                                <div id="owl-demo8" class="owl-carousel">
                                    @if(!empty($gallery))
                                    @foreach($gallery as $img)
                                    <div class="item"><a data-fancybox="proj_img" href="{{asset('public/upload/property_gallery/')}}/{{$img->img_name}}"> <img src="{{asset('public/upload/property_gallery/')}}/{{$img->img_name}}" style="width: 790px; height: 481px;"></a>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="item"><a data-fancybox="proj_img" href="{{asset('public/upload/project_image/')}}/{{$projectInfo->project_image}}"> <img src="{{asset('public/upload/project_image/')}}/{{$projectInfo->project_image}}" style="width: 790px; height: 481px;"></a>
                                    </div>
                                    @endif
                                   
                                </div>   
                            </div>

                            <!-- #tab1 -->

                           
                            <h3 class="tab_drawer_heading" rel="tab2">Siteplan</h3>
                            <div id="tab2" class="tab_content">
                                @if(!empty($projectDetail)) 
                                    @if($projectDetail->site_plan_image != '')
                                   <a href="{{asset('public/upload/project_image/site_plan/')}}/{{$projectDetail->site_plan_image}}" data-fancybox="site_plan"> <img src="{{asset('public/upload/project_image/site_plan/')}}/{{$projectDetail->site_plan_image}}" alt="Site Plan" class="thumbnail"></a>
                                    @else
                                      Image not Available
                                    @endif
                                @endif
                            </div>
                            
                            <h3 class="tab_drawer_heading" rel="tab3">Floor Plan</h3>
                            <div id="tab3" class="tab_content">
                                <div class="row">
                                    @if(!empty($floorPlan))
                                    @foreach($floorPlan as $floor_plan )
                                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 text-center "> <a href="{{asset('public/upload/floor_plans/')}}/{{$floor_plan->image}}" data-fancybox="gallery"  alt="{{$floor_plan->type}}">
                                            <img class="thumbnail" src="{{asset('public/upload/floor_plans/')}}/{{$floor_plan->image}}" alt="{{$floor_plan->type}}" style="width: 231px;height: 144px;"></a>
                                        <p><strong>{{$floor_plan->type}} </strong></p>
                                    </div>
                                   
                                    @endforeach
                                    @else
                                    Not available
                                    @endif
                                </div>
                            </div>

                            <h3 class="tab_drawer_heading" rel="tab4">Payment Plan</h3>
                            <div id="tab4" class="tab_content">
                                @if($projectDetail->booking_amount == '' && $projectDetail->terrace_comp_amt == '' && $projectDetail->possesion_amt =='')
                                    Not Available

                                @else
                                <p>{{$projectDetail->booking_amount}}</p>
                                <p>{{$projectDetail->terrace_comp_amt}}</p>
                                <p>{{$projectDetail->possesion_amt}}</p>
                                <p>{{$projectDetail->booking_payment_time}}</p>
                                @endif
                            </div>
                            <div id="tab5" class="tab_content">
                                @if(!empty($projectDetail) && $projectDetail->brochure) 
                                       <center><a href="{{asset('public/upload/brouchers')}}/{{$projectDetail->brochure}}" target="_blank" alt="download Broucher" title=" Broucher" ><img src="{{asset('public/images/pdf.gif')}}" /> </a></center>
                                      @else
                                      <img src="{{asset('public/images/coming-soon.jpg')}}" class="thumbnail" />
                              @endif
                              
                            </div>
                        </div>

                    </div>
                     @if(!empty($priceInfo))
                    @if($sizeOfpriceInfo > 1)
                    <div id="units" class="bgWhite clearfix" style="margin-top:15px">
                        <h3><strong>Units / Prices</strong></h3>
                        <hr align="left">
                       <div id="demo" class="col-md-12"  style="margin-top: 10px;">
                            <div id="owl-demo6" class="owl-carousel">
                               
                                @foreach($priceInfo as $prices)
                                 @if($prices->price_plan == '')
                                <div class="item" style="border:1px solid #999999; padding:10px; text-align:center;">
                                    <p style="font-size:14px; color:#000000"><font style="font-size:24px"><strong>{{$prices->accomodation_type}}</strong></font><br>
                                        {{$prices->unit_size}}</p>
                                    <h2 style="font-size:26px">₹ <strong>{{$prices->    total_price}}</strong></h2>
                                    
                                </div>
                               @endif
                              @endforeach
                            </div>
                        </div>
                    </div> 
                    @endif
                    @if($sizeOfpriceInfo == 1 )
                    <div id="payment" class="bgWhite clearfix" style="margin-top:15px">
                        <h3><strong>Price Breakup</strong></h3>
                        <hr style="">
                        <center id="price_breakeup">
                            <p>B.S.P : As Applicable</p>
                            @foreach($priceInfo as $prices)
                                 @if($prices->price_plan != '')
                            <a href="{{asset('public/upload/project_image/price_plan/')}}/{{$prices->price_plan}}" data-fancybox="price" alt="{{$prices->price_plan}}">
                                <img  src="{{asset('public/upload/project_image/price_plan/')}}/{{$prices->price_plan}}" height="465" style="width: 100%;">
                            </a>
                                 @endif
                            @endforeach

                            <!--<p class="complete3" style="display:none;">
                                <input type="image" src="{{asset('public/images/favista/494x465xparkwest,P20price,P201,281,29.jpg.pagespeed.ic.ujt_XLkP3v.jpg')}}" height="465"  style="width: 100%;">
                            </p>
                            <p class="more" onClick="showHideDivs(this, 3)"><a href="javascript:void(0)">View More...</a></p>-->
                        </center>
                    </div>
                    @endif
                    @endif
                    <div id="location" class="bgWhite clearfix" style="margin-top:15px">
                        <h3><strong>Location Map</strong></h3>
                        <hr style="width:100%" align="left">
                        <div id="location_map" class="col-md-9" style="height: 445px; position: relative; overflow: hidden;">
                              <input id="geocomplete" name="address" type="text" placeholder="Type in an address" value="@if(!empty($projectDetail)) {{$projectDetail->address}} @endif" size="90" />
                            <div class="map_canvas" >
                                
                            </div>
                        </div>
                        @if(!empty($nearByArr))
                        <div class="col-md-3">
                            <p style="color:#000" align="center"><strong>Explore Neighbourhood</strong></p>
                            <hr style="" align="center">
                            <div class="row" align="center" style="margin-left:0px;">
                               @if(in_array('School',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('school', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAAAS1BMVEX///8FBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgfBH/iAAAAGHRSTlMAECAwQExQYHCAkKCwwMjM0NPY4PD09vi9cuulAAABUElEQVRIx+3WwXKEIAwG4B9IQcS0bEMt7/+kPWx3RyVi3WPHHBzHzbeaCBmBK/5ZhPAKEhEJ51F4HE+i9dkJdAJSk/YHSKylBMl0Gt0h02nUgcQlmm4BQ2mggixtk0xcQ+1OQ+GcTQdqyPnZA0r/njBUsSsQp1wL39jHapUSrdS4vNsdCMeBDGAiT0rr1o85f96qcAzUXw3bpmT5Omi/ggCmw/fWIoAJIP6OnVUyv7cV3P+n9taktpp/2e4OYAJ3mLaggwg9U/YY4PXdfcR2ZonKhiVTJ5fGyrRmypxUGEnZsmYqK2wcxTVMLX8V2afhiLWv21YEBpDTvhqbrY4wwVYAruy6VFxzbRoAIQAus7ahYTkr14sFxggAZqypSbCpalvDVYJN/MgpOS6qp5hLUh8BscjiJ+NTrsLMH8xSc/L7fXLNxCOiNyJ3fYtc8XL8AC5oEmCGxIpSAAAAAElFTkSuQmCC" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">School</p>
                                    </a> </div>
                                @endif
                                @if(in_array('Hospital',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('hospital', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2BAMAAAB+a3fuAAAAIVBMVEX///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADdcGRXAAAACnRSTlMAIDBAcICQwNDgh45RHwAAAF5JREFUOMtjYCAXsK8CgQIMkQUglgGQYEaRA4kwjMqNBLmpoQgQiSrHGIoMBFDksIAhJ8dUjgwUUMOl2RgBLEbTy6gcbjlnYBIxQZEDiRiD5NjASSsBSQ4iUoC3tAYAmyZj2rUnKOkAAAAASUVORK5CYII=" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">Hospital</p>
                                    </a> </div>
                                @endif
                                @if(in_array('ATM',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('atm', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2BAMAAAB+a3fuAAAAKlBMVEX///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADmU0mKAAAADXRSTlMAECAwQGCAkKDA0ODw/BaCBgAAAL9JREFUOMtjYCAfRM7EBFMhUlJLXTBB5EKw3AyshnWCCOaDWOWkDYAEWwNWOfYEkFwCVjk2qBwrwn0B6HJsLTDneSRgyKGKDHk5RrA/DbDKMYODp4BubmG5CwITiNM3LQ0KMjHkWNLgwAFdDl+awC+nIsBiwOzA6IRNLteBdwLnBZZr2OTcFNgCWBOYUki1D58cPvsiDTgL2CcwT6Wmffj8gM8t+MJsqIT1IJHLnJaGDWROA8qll2GVSy/DW1YDAOwPiqzkZtR0AAAAAElFTkSuQmCC" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">ATMs</p>
                                    </a> </div>
                                @endif
                                @if(in_array('Market',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('shopping_mall', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAAASFBMVEX///8EBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwdvMef6AAAAF3RSTlMAECAwQFBYYHCAiJCgsMDEyNDY4PD19hUoiakAAAG0SURBVEjH5VbbcoUgDBQNlRRIS6XN//9pVaIDHI6OTqcPbR5klF3MjYWu+4vWj368ytE2sDf9NdLAwcJ1/0Z/KyzEWzTSt2gT3KLx1VrBiJ7idAHuaOIv8mjA0AlcgUY7wz9X+GDxOJEJHpiJEAFIUiCjNzUeAJGIObTg20hVImmGW9TwJnURp3Ycq3zYzdqyC7CmpfzEuq9DGo1r0oaUeKgTqaSO28RGC5B/RldnROY3OtqiK4wvIs6CwyL27a9c/JweOlJTEUxFc6le8WFTqxJQ0dJiqtHIEpy4U9HSJDQ6UoITZyXjG43zxJQq0cJDyCNoNXLPjfXFLWE3FUH2uwwlLQ3T0KA5zJcsaFJ8PpBACSAOOW39Bk1FkPaWdFGe8iT5uq0IU/6DRJMNkV6eKEJqEEl2gUwePzlmUnA9v8BswSxP55anYVzsibT2vFj8eKfdQlie7nWlofpRpd4E5epBGe4dyrG/RfMWTqztJZ1Z9y9tnE+2h1uMJ3dcHBO1DrbeighuOr159NX2X4+McNixy0ar5XfVEjqk2aCUq8oKEboxqsMrgpuP+xoxRm6q3e/ZN/pNFa4sqZ6mAAAAAElFTkSuQmCC" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">Supermarkets</p>
                                    </a> </div>
                                @endif
                                @if(in_array('Gym',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('gym', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAAAP1BMVEX///8EBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwdqYdmLAAAAFHRSTlMAECAwQFBgcICQoLDAxMjM0ODv8ID8bSsAAAD6SURBVEjH7ZXhDsIgDIRlMOg6dCC8/7M6YFOLSIgaTQz3Z8uxDyhcusOhq6sriesoZNRmmHxRppgzYpXySH30KvjGDUVM2LSMdNTXMs1qy8sJvb34DNufn8NGADzVsRMCjHRImjbMSDLkWdsmGR3zrbXdjSmAdgxAbdc555ggMhk2xzhEm2KayOebNGL/urbJB0x/H+MhwbUjyTFmeUyITWU/O5IzwbS2W1J4mrHt3oTg76Xk9UyG+sxSxxZzq+vaftYmcqxjR0Xq+kFTCJhLW8C8c6UGyF0ZY9aNayjQT9QHj6s9OsfK/XWYQihmlfsyhmUa+n+r6/90ATYdD4AzXHsLAAAAAElFTkSuQmCC" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">Gym</p>
                                    </a> </div>
                                @endif
                                @if(in_array('Entertainment',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('movie_theater', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAAAQlBMVEX///8FBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwi9d+A+AAAAFXRSTlMAECAwQFBgcICIkJigsMDM0ODk7/A77jpQAAABFUlEQVRIx+2WYW+EIAyGwSLXOejBCf//r25SnVziqcDicsneT03TB9C3gQpxvaRG1jDgpj4+5+AmVwqDnbNE25jxc2AjLpRx8LMAbh9H0RKBMxz0QXYqSe5iXAMy9CnltEBP3wpqF4tTjSOhXUrFZRM6wJ4C8KIQc2oOyjBqxZJd/gBLBuZYiQEZJs7bLRbsrlaZV5jLiu4Je9Aq/wobs6JHyyHrMc27wy7GNcMv2P0WmJlsdAfYVHOj9j95od1jLNT4Nz2JvPuBAUn035Pv3ZOV92TlrVz/bYqnh24X4xrd/r5d11xdKMU81MwlMgXCYtkUhJaHrwglTz4ExTkd9Hm7s2LlvT03T1rv+2w10HhKGpqm5C//tScg9d0oKwAAAABJRU5ErkJggg==" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">Entertainment</p>
                                    </a> </div>
                                @endif
                                @if(in_array('Cafe',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('cafe', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAAAM1BMVEX///8FBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgkR648AAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAQhJREFUSMftlVEPgyAMhCkiKrTQ//9rN9DNuQ3Xmj1sifdEYj6OwwOMOfULsqNEDp4wlwTURBzsFkPhong8gBnTUQQ5VrP3ZQQU5FjNTnkoXBrkGM5wcXIZdNjihIMSm5160mLVCViNDSVd6rRYHaDTYv0xbCrlYlBikK+xuqTdkhCLY1BigWBxlGPWJyyhYpB3kplz9GXo10rKz5sZ1yXKMYup055u52MeQXMp4DUbU/RgVFfQvU5fwda+qDCT7R4FDI3fsbvKh2I8zUex6Qe1iq1vjA1xgL0AriH7Jw/iRuIHcSOW9ONVLOjHq5YXYEIVBeRv/QDFDhJ+7sebxkwPUzixwJz6qi4mPhHVcEM3+wAAAABJRU5ErkJggg==" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">Cafe</p>
                                    </a> </div>
                                @endif
                                @if(in_array('Bus Stand',$nearByArr))
                                <div class="col-md-6 col-xs-3"> <a href="javascript:void(0);" onClick="exp_services('bus_station', this);"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAAAM1BMVEX///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADxgEwMAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAfFJREFUSMfNVkGWhSAMC1KhYIHe/7SzkO8XBHXem8V0iQTbJikAfxaGOEiNwGTeYNaQVAJTDQ6iKaz3GBtLDtSvUsgl2jlISph8taHI5JMrfFOG4eKGRelDBVQGG8z4sDadaza8PXc5hstSpmeYzf3KUt6QWvpuuu0NbPN92h6AjaKTkGgB+P7wbAHSqQANsRJgu1IWHaXQhN8AlKXlWgCk22ZSBiAt48wA9L4f+tn3DVkB8wArS82qJYTkHibU02sUl5OuMAdAzz0hueZ9VS3XX35X4ltY4F4j24Nz3AbAx9NKov7/I+IEAKUzJeYt7Ezu7qNOOFdd6ke8jWv0yTaKtgN7e1QeQo+mnzoCegw0PTGKX4Sas2tex+EdDr+BHWL64GkTmRjciWxrl1ulwhWmVdJomsS0kiuhHZZax4kFABkI2mUDwNYpqQ2sDrNRf8Sd6O1gtdTReK5q5Ra2i9GlWvygNP7aa5+N3xxMdgDs6BKjYgG4/fijimo9W4RjGXLoNPJW6PRPAEudK8YxT8yzePamKvHYEuN7kZz2LoXeouhsZvfk7MkNH9OrJ5JJsVfdni5PYs8mxeGgAOUxqj4IFBOYzK+NfwDbvTobK/sAuD5nwALApMmtw8kAkDCgJHufpzNMMvs0JNfFeHNVtV9/AO3nFJtVy8anAAAAAElFTkSuQmCC" class="img-responsive">
                                        <p class="intro_1 hidden-xs-down">Bus Stand</p>
                                    </a> </div>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                    
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="bformBox wrap sticky-scroll-box">
                        <h3 style="padding-top:5px; font-size: 20px; margin: 0 0 20px; font-weight: 600;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAMAAAAp4XiDAAAAPFBMVEX///8EBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwcEBwfwrhZTAAAAE3RSTlMAECAwQFBgcICQkqCwwNDc4Ojw/NYysAAAATtJREFUSMftls12hCAMRkHiD8VYJvf937ULnXPqKNHZtJvJTuRC+BISQvhH60REpLs9vy/Galb6O8BUAVRVFaBOV4BUqDltXylXqOISI9iwGxkMRocoMMeXsThDaRL5fMERckspGE5/DHCuXLTmYhmL5wdZmi4vp8fpoK2mQHe2uTpa6pnTFS85euphLGFukI10FH92kfkYsoyff9PxMIqf5/1BnQyP5BDp8Zo1E9CI8DMzYO+6soi5oTRZ9q4pEtRFNMgH+SB/jYyxukiN4x4pUKF9xxLUl2reGV59D6EAtq9+cZz8u//Fd3yz4RamN4lopIsZIiKSflc+9Ylh6+LLBsXCxSYCpqoLa8fphtrqb7+qmqWnsqtdNPIQJpa49UgAdLjUV6Cur4t6W9L8fMOk+2FIIiIi78a7ZT/EwRh5Qth1LQAAAABJRU5ErkJggg==" alt="Get A Call Back" width="30px"> Get a Call Back</h3>
                        @if(session('info')=='fail')
                         <div class="alert alert-danger">Message Not Send</div>
                         @endif
                         @if(session('info')=='success')
                          <div class="alert alert-success">Thank you for your Interest. We will contact you Soon.</div>
                         @endif
                        <form id="lead_form" action="{{ action('UserQueriesController@store') }}" method="post" >
                            @include('layouts.errors')
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                            <div class="formrow">
                                <input id="name" name="name" class="form-control input" placeholder="Name *" required="" type="text">
                            </div>
                            <div class="formrow">
                                <input id="email" name="email" class="form-control input" placeholder="Email *" required="" type="email">
                            </div>
                            <div class="formrow">

                                <input id="phone" name="pnumber" class="form-control input" placeholder="Mobile No*" required="" autocomplete="off" type="tel">
                                @if(!empty($builderInfo))
                                <input name="builder_id" value="{{$builderInfo->id}}" maxlength="50" required="" placeholder="Name*" class="com-marg" style="cursor:pointer;" autocomplete="off" tabindex="7" type="hidden">
                                @endif
                                <input id="project_id" name="project_id" value="{{$projectInfo->id}}" class="form-control input" placeholder="" required="" autocomplete="off" type="hidden">
                                <input  name="budget" value="@if(!empty($projectDetail)) {{$projectDetail->price}} @endif" class="form-control input" placeholder="" required="" autocomplete="off" type="hidden">
                                <input  name="msg" value="I am Interested for {{$projectInfo->name}} Project.. please contact me as soon as possible" class="form-control input" placeholder="" required="" autocomplete="off" type="hidden">
                            </div>
                            
                            <div class="text-center col-md-12">
                                <button id="sub_btn" type="submit" name="submit" class="contact_but col-md-8" style="float: none">Call Me</button>
                            </div>

                        </form>
                        <div class="clearfix"></div>
                        <p style="padding-top: 5px;font-size:11px">AGENT RERA REGD NO:  AAGT10058 </p>
                        <h6 style="padding: 10px 0;font-size:11px"><i class="fa fa-shield fvcolor" aria-hidden="true"></i> Your number is secure with us. We don't share your number with any third party.</h6>
                    </div>

                </div>
            </div>
             @if(!empty($builderInfo))
            <div class="container"> 
                <!-- Section Title -->
                <div class="col-md-12 bgWhite clearfix" style="margin:15px">
                    <h3><strong>Popular Projects</strong></h3>
                    <hr style="">
                    <div class="col-md-12"  style="margin-top: 10px;">
                        <div id="owl-demo7" class="owl-carousel">
                            @foreach($popularProjects as $popularProject)
                           <div class="item">
                                <a href="{{ URL::to($popularProject->url)}}/" alt="Embassy Grove"> <img src="{{asset('public/upload/project_image/')}}/{{$popularProject->project_image}}" class="thumbnail" style="width: 260px; height: 161px;">
                                    <div class="col-md-8 pro-price" align="left">
                                        <p>{{$popularProject->name}}</p>
                                    </div>
                                    <div class="col-md-4 pro-price" align="right">
                                        <p><i class="fa-rupee fa"></i>
                                                {{$popularProject->pro_price}}
                                       </p>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div> 
            </div>
            @endif
            @if(!empty($builderInfo))
            <section id="developer" class="clearfix">
                <div class="container">
                    <div class="col-md-12 bgWhite" style="margin:15px">
                        <h3><strong>{{$builderInfo->name}}</strong></h3>
                        <hr style="">
                        <div>
                            <p class="intro">{!!$builderInfo->description!!}</p>
                            <br>
                            <strong style="text-align:center;width: 100%;display: inline-block;"><a style="color:#174e91" href="{{ URL::to('/')}}/{{$builderInfo->url}}/">View More...</a></strong> </div>
                    </div>
                </div>
            </section>
            @endif
        </section>
        
<footer>
  <div class="footer1">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Top Cities Project</h4>
          <ul>
          <li><a href="{{ URL::to('/new-delhi/') }}/">New Delhi</a></li>
    <li><a href="{{ URL::to('/noida/') }}/">Noida</a></li>
    <li><a href="{{ URL::to('/greater-noida/') }}/">Greater Noida</a></li>
    <li><a href="{{ URL::to('/gurugram/') }}/">Gurugram</a></li>
    <li><a href="{{ URL::to('/bangalore/') }}/">Bangalore</a></li>
    <li><a href="{{ URL::to('/pune/') }}/">Pune</a></li>
    <li><a href="{{ URL::to('/mumbai/') }}/">Mumbai</a></li>

          </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Quick Links</h4>
    <ul>
        <li><a href="{{ URL::to('/') }}/">Home</a></li>
        <li><a href="{{ URL::to('/about-us/') }}/">About Us</a></li>
        <li><a href="https://www.inoutgreen.com/">In Out Green Interiors</a></li>
        <li><a href="{{ URL::to('/green-building-consultants/') }}/">Green Building Consultants</a></li>
        <li><a href="http://www.inoutgreenhomes.com/blogs/">Blogs</a></li>
        <li><a href="{{ URL::to('/contact-us/') }}/">Contact us</a></li>
        <li><a href="{{ URL::to('/we-are-hiring/') }}/">We Are Hiring</a></li>
    </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Recent Posts</h4>
           @foreach($footerPost as $newposts)
          <p>
            @if($newposts->getImageAttribute() != '')
            <img src="{{$newposts->getImageAttribute()}}"  style="width: 60px; height: 49px; "alt=""/>
            @else
               <img src="{{asset('public/images/no-image.png')}}"  style="width: 60px; height: 49px; "alt=""/>
            @endif
             <a href="{{ URL::to('/blogs/') }}/{{$newposts->slug}}/">{!!substr($newposts->title,0,50)!!}</a></p>
          @endforeach
         
          
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Contact Us</h4>
          <p>
   <i class="fa fa-map-marker"></i> <span>Delhi Office:<br/>
            {{substr($address->office_address,0,32)}}<br/>
            {{substr($address->office_address,33)}} </span>
     
       <br><i class="fa fa-map-marker"></i>  <span> Noida Office:<br/>
           {{substr($address->address,0,32)}}<br/>
            {{substr($address->address,33)}}</span>  </p>
          <p><i class="fa fa-phone"></i> <a href="tel:+91 {{$address->phone}}">+91 {{$address->phone}}</a></p>
          <p><i class="fa fa-envelope"></i> <a href="mailto:{{$address->email}}">{{$address->email}}</a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="footer2">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ft"> <span>Copyright &copy; 2018 <a href="{{ URL::to('/') }}">Delhi Smart Cities</a>. All Rights Reserved</span> </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ft">
<ul>
    <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
    <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
    <li><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
    <li><a href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
    <li><a href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
    
</ul>
</div>
      </div>
    </div>
  </div>
</footer>
        <script src="{{asset('public/js/jquery.min.js')}}"></script>
        <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/js/jquery-ui.js')}}"></script>
        <script src="{{asset('public/js/bootstrap-select.js')}}"></script>
        <!-- Import Owl carosel js-->
        <script src="{{asset('public/js/owl.carousel.js')}}"></script>
        <script src="{{asset('public/js/tab.js')}}"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsMVfio3e-dKTp1S4etAdOoJDZrqnWEfM&libraries=places"></script>

        <script src="{{asset('public/backend/js/jquery.geocomplete.js')}}"></script>
        <script src="{{asset('public/backend/js/logger.js')}}"></script>
        <script src="{{asset('public/js/jquery.fancybox.js')}}"></script>

        <!-- logo slider-->
        @php
             $lat = '';
            $long = '';
        @endphp
        @if(!empty($projectDetail)) 
            @php 

             $lat = $projectDetail->latitude; 
             $long = $projectDetail->longitude; 
            @endphp
        @endif


        <script>
      $(function(){


       var options = {
          map: ".map_canvas",
          details: "#info",
          location: ["{{$lat}}","{{$long}}"],
          types: ["geocode", "establishment"],

       };  
      $("#geocomplete").geocomplete(options);
   
      });
    </script>
        <script>
                                $(document).ready(function () {
                                    $('#Carousel').carousel({
                                        interval: 5000
                                    })
                                });
        </script>
        <script>
            $(window).on('load', function () {
                $('.selectpicker').selectpicker({
                    'selectedText': 'cat'
                });
            });
           
            $("#controlgroup").controlgroup();
            $().fancybox({
                selector : '[data-fancybox="gallery"]',
                
            });
             $().fancybox({
                selector : '[data-fancybox="price"]',
                
            });
             $().fancybox({
                selector : '[data-fancybox="site_paln"]',
                
            });
             $().fancybox({
                selector : '[data-fancybox="proj_img"]',
                
            });
        </script>
        <script>
            //            $(function(){
            //               $(".elipDots .__readMore").click(function(){
            //                   $(this).next(".descHidden").show("slow");
            //               },function(){
            //                   $(this).next(".descHidden").hide("slow");                   
            //               });               
            //           });
            // tabbed content
            http://www.entheosweb.com/tutorials/css/tabs.asp
                    $(".tab_content").hide();
            $(".tab_content:first").show();

            /* if in tab mode */
            $("ul.tabs li").click(function () {

                $(".tab_content").hide();
                var activeTab = $(this).attr("rel");
                $("#" + activeTab).fadeIn();

                $("ul.tabs li").removeClass("active");
                $(this).addClass("active");

                $(".tab_drawer_heading").removeClass("d_active");
                $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

                /*$(".tabs").css("margin-top", function(){ 
                 return ($(".tab_container").outerHeight() - $(".tabs").outerHeight() ) / 2;
                 });*/
            });
            $(".tab_container").css("min-height", function () {
                return $(".tabs").outerHeight() + 50;
            });
            /* if in drawer mode */
            $(".tab_drawer_heading").click(function () {

                $(".tab_content").hide();
                var d_activeTab = $(this).attr("rel");
                $("#" + d_activeTab).fadeIn();

                $(".tab_drawer_heading").removeClass("d_active");
                $(this).addClass("d_active");

                $("ul.tabs li").removeClass("active");
                $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
            });
            /* Extra class "tab_last" 
             to add border to bottom side
             of last tab 
             $('ul.tabs li').last().addClass("tab_last");*/
        </script>
        <script>
            function switch_tabs($obj) {
                $('.tabs-content-wrap').hide();
                $('.nav-tabs a').removeClass('active');
                var id = $obj.attr('href');
                $(id).show().css('display', 'block');
                $obj.addClass('active');
            }
            $('.nav-tabs a').click(function () {
                switch_tabs($(this));
                return false;
            });
            switch_tabs($('.nav-tabs a.active'));
        </script>
        <!-- builder /-->
        <script>
            $(document).ready(function () {
                $("#owl-demo6").owlCarousel({
                    navigation: true,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-grey'></i>",
                        "<i class='fa fa-chevron-right icon-grey'></i>"
                    ],
                    autoPlay: 8000,
                    items: 4,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [979, 3]
                });
                $(".owl-pagination").remove();

                $("#owl-demo7").owlCarousel({
                    navigation: true,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-grey'></i>",
                        "<i class='fa fa-chevron-right icon-grey'></i>"
                    ],
                    autoPlay: 8000,
                    items: 4,
                    itemsDesktop: [1199, 2],
                    itemsDesktopSmall: [979, 1]
                });
                $(".owl-pagination").remove();

                $("#owl-demo8").owlCarousel({
                    navigation: false,
                    navigationText: [
                        "<i class='fa fa-chevron-left icon-grey'></i>",
                        "<i class='fa fa-chevron-right icon-grey'></i>"
                    ],
                    autoPlay: 8000,
                    items: 1,
                    itemsDesktop: [1199, 2],
                    itemsDesktopSmall: [979, 1]
                });
                $(".owl-pagination").remove();
            });
        </script>
        <script>
            $(function () {
                $('ul.link li').on('click', function () {
                    $(this).parent().find('li.active-link').removeClass('active-link');
                    $(this).addClass('active-link');
                });
            });

            //                  $(".more").click(function(){
            //                      console.log(this);
            //                      if($(this).text() == 'View More...' || $(this).text() == 'More...'){
            //                          $(this).text("less...").siblings(".complete").show();
            //                          $('.complete').show();
            //                      }else if($(this).text() == 'less...'){
            //                          $(this).text("More...").siblings(".complete").show();
            //                          $('.complete').hide();
            //                      }                      
            //                  }); 

            function showHideDivs(obj, classId) {
                if ($(obj).text() == 'View More...' || $(obj).text() == 'More...') {
                    $(obj).text("View less...").siblings(".complete").show();
                    $('.complete' + classId).show();
                } else if ($(obj).text() == 'View less...') {
                    $(obj).text("View More...").siblings(".complete").show();
                    $('.complete' + classId).hide();
                }
            }

        </script>
        <script>
            $('a[href^="#"]').on('click', function (event) {
                var target = $(this.getAttribute('href'));
                if (target.length) {
                    event.preventDefault();
                    $('html, body').stop().animate({
                        scrollTop: target.offset().top
                    }, 1000);
                }
            });

            window.onscroll = function () {
                myfunction()
            };
            var linkbar = document.getElementById("linkbar");
            var sticky = linkbar.offsetTop;

            function myfunction() {
                if (window.pageYOffset >= sticky) {
                    linkbar.classList.add("sticky")
                } else {
                    linkbar.classList.remove("sticky")
                }
            }

        $(document).ready(function () {
                var top = $('.sticky-scroll-box').offset().top;
                $(window).scroll(function (event) {
                    var topid=$('.intro_1').offset();
                     var f=topid.top;
                      if ((window.matchMedia('(max-width: 988px)').matches)) {
                  $(".floatRelative").css("top", 0);
                 }
                 else{
                $(".floatRelative").css("top", f-590);
            }
                     console.log(f);
                    var y = $(this).scrollTop();
                    if (y >= 500 && y<=f+10) {
                        //$('.sticky-scroll-box').removeClass('floatRelative');
                        $('.sticky-scroll-box').removeAttr("style");
                        $('.sticky-scroll-box').addClass('fixed');
                    } else {
                        if(y>f+10){
                           // $('.sticky-scroll-box').addClass('floatRelative');
                             if ((window.matchMedia('(max-width: 988px)').matches)) {
                                  $(".floatRelative").css("top", 0);
                                     }
                                   else{
                  $('.sticky-scroll-box').css({ "position": "relative", "top": f-590 }) ;
              }
                        }else{
                            $('.sticky-scroll-box').removeAttr("style");
                           // $('.sticky-scroll-box').removeClass('floatRelative');
                        }
                        $('.sticky-scroll-box').removeClass('fixed');
                    }
        $('.sticky-scroll-box').width($('.sticky-scroll-box').parent().width());
                });
            });
        </script>
    </body>
</html>
