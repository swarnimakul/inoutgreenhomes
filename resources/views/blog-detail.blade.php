@include('layouts.master')
@include('layouts.header')
<section class="seacrh-content mar-top100 container-fluid">
  <div class="container project-content">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="row">
          
          <div class="divider"></div>
          
          <div class="clearfix">&nbsp;</div>
          @include('layouts.left-contact-form')
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-9">
        
      
     
        @if(!empty($post))
        <div class="project-cont">
          <div class="srpWrap">
           
          
                <h4 class="pro-title">{{$post->title}}<span class="fr font13">{{$post->created_at}}</h4>
                <p>{!!$post->description!!}</p>
                
              
          </div>
        </div>
       
        
        @endif
     
 
      </div>
    </div>
  </div>
  
  
<div class="clearfix com-marg"></div>               
</section>
 


@include('layouts.footer')
