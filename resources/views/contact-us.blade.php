@include('layouts.master')
<script type="text/javascript">

  function validateForm1()
  {
     var name1 = document.forms["myForm1"]["name1"].value;
    if (name1 == "") {
        alert("Name must be filled out");
        document.getElementById("name1").focus();
        return false;
    }

    
    
    var email1 = document.getElementById('email1');
  
    var filter1 = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter1.test(email1.value))
  {
    alert('Please provide a valid email address');
 document.getElementById("email1").focus();
    return false;
     }
     
   
  var pnumber1 = document.forms["myForm1"]["pnumber1"].value;
    var phoneno1 = /^\d{10}$/;
    if(pnumber1.match(phoneno1))
        {
         
     $a=document.getElementById('ran1').value;
          $b=document.getElementById('usercaptcha1').value;
           if($a!=$b){
            alert('Enter valid Captcha');
            return false;
           } 
           else{
            return true;
           }
        }
      else
        {
        alert("Enter Correct Phone number");
        document.getElementById("pnumber1").focus();
        return false;
        }
      
         
        
  }
 
</script>

<script type="text/javascript">

function captch1() {
    var x = document.getElementById("ran1");
    x.value = Math.floor((Math.random() * 10000) + 1);
}
function lettersOnly(textarea){
	var regex=/[^a-z .,0-9]/gi;
	var count=textarea.value.length;
	//alert(count);
	textarea.value=textarea.value.replace(regex,"");
	document.getElementById('comment2').innerHTML = "Characters left: " + (200 - count);
}
function CheckCaptcha(capt,ucapt){
 $a=document.getElementById('ran1').value;
          $b=document.getElementById('usercaptcha1').value;
           if($a!=$b){
            alert('Enter valid Captcha');
            return false;
           } 
           else{
            return true;
           }
  }
</script>

 
<style type="text/css">
  #ran1{
    background-image: url("{{url('/public/')}}/images/cat.png");
  }
</style>
@include('layouts.header')


<section>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="rera_regis">
          <h2 id="contact">Contact Us</h2>
         
        </div>
      </div>
</section>
<section class="about">
  <div class="container">
    <div class="row">
     @if(session('info')=='fail')
     <div class="alert alert-danger">Message Not Send</div>
     @endif
     @if(session('info')=='success')
      <div class="alert alert-success">Thank you for your Interest. We will contact you Soon.</div>
     @endif
   
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form"> 
         <form  method="post" action="" name="myForm1" onsubmit="return validateForm1();">
          {{ csrf_field() }}
          <div class="form-group">
          <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
       <input type="text" class="form-control from-input-text" name="name" placeholder="Name" id="name1" required="">
      </div>
       <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="email" class="form-control from-input-text" name="email" placeholder="Email" id="email1" required="">
      </div>
       <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <input type="tel" class="form-control from-input-text" name="pnumber" placeholder="Phone Number" id="pnumber1" pattern="^\d{10}$" required="" maxlength="10" oninvalid="this.setCustomValidity('Please Enter valid Phone number')" oninput="setCustomValidity('')">
        
      </div>
    </div>
    <!-- select -->
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <select name="budget" placeholder="Your Budge" required="">
              <option value="">Your Budget</option>
          <option value="20 Lacs" class="form-control"> 20 Lacs</option>
          <option value="20-30(lacs)">20-30(lacs)</option>
          <option value="30-50(lacs)">30-50(lacs)</option>
          <option value="50 lacs-1Cr">50 lacs-1Cr</option>
          <option value="1cr">&gt;1cr</option>
                </select>
              </div>


              <!-- textarea -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<textarea class="form-control" id="comment1" name="msg" placeholder="Message" onkeyup="lettersOnly(this);" required="" ></textarea><i id="comment2" style="color: red">  </i>
</div>
<!-- captcha -->
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
<input type="text" class="from-input-text" value="1447" name="captcha" id="ran1" readonly="" >


</div>
<!-- captcha enter -->
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
<input type="text" class="form-control from-input-text" name="usercaptcha" placeholder="Enter Captcha" id="usercaptcha1" required="" onfocusout ="CheckCaptcha()">
<!-- refresh -->
<a title="refresh captcha"  onclick="captch1()" id="clickcaptcha">  <i class="fa fa-refresh"></i></a>
</div>
            
<div class="form-group">

<!-- send message -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<input type="submit" id="contact-btn" class="btn btn-info btn-lg" name="sub1" value="Send Message"  >
</div>
</div>
      
      
          
  </form> 
  </div>     
 <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 ">
  <div class="address">
    <ul>
      <li>
       
        <p> <i class="fa fa-map-marker"></i>
            <b>Delhi Office:</b><br>
            SF 2, Dwarka Mor, Dwarka-110075 </p>
            <p style="margin-left: 33px;margin-bottom:20px"> <b>Noida Office:</b><br>
            FF-17, Sethi Arcade,<br>
            Sec-76, Noida - 201301 </p>
           
          
      </li>
      <li>

         <p> <i class="fa fa-phone"></i>
         <a href="tel:+91 8010-179-239">
       +91 8010-179-239</a>
     </p>
    
      </li>
      <li>
        <p><i class="fa fa-envelope"></i>
        <a href="mailto:inoutgreenhomes@gmail.com">inoutgreenhomes@gmail.com</a>
       </p>
      </li>
    </ul>
  </div>
 </div>
   
    </div>
  </div>
</section>
<!-- map -->
<section>
 <div class="fluid-container map">

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3504.1700288015145!2d77.38290245011851!3d28.564656582358463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cef4ac49e7813%3A0x5a73926542cc661a!2sSethi%20Arcade!5e0!3m2!1sen!2sin!4v1605537284612!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> 
 </div>
</section>
 <script type="text/javascript">
   window.onload = captch1();
 </script>
@include('layouts.footer')
