@include('layouts.master')
@include('layouts.header')
<section class="about page">
  <div class="container">
    <div class="row">
      {!! $pageData->content !!}
    </div>
  </div>
</section>



@include('layouts.footer')



