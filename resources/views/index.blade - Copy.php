<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name='viewport' content='width=device-width, initial-scale=1'>
<title></title>
<link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('public/css/style.css')}}" type="text/css">
<link href="{{asset('public/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/owl.theme.css')}}" rel="stylesheet">
    
<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<style>

</style>
</head>

<body>
<header>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand logo" href="#"><img src="{{asset('public/images/logo.png')}}" alt=""/></a> </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="">Home</a></li>
              <li><a href="#">About Us</a></li>
              <li><a href="#">Land Pooling Policy</a></li>
              <li><a href="#">Projects</a></li>
              <li><a href="#">Blogs</a></li>
              <li><a href="#">Contact Us</a></li>
            </ul>
          </div>
          <!--/.nav-collapse --> 
        </nav>
      </div>
    </div>
  </div>
</header>
<section class="banner"> <img src="{{asset('public/images/banner.jpg')}}" width="100%" alt=""/>
  <div class="overlay">
    <div class="container">
      <div class="row search-box">
        <h2>Find Your Property</h2>
        <div class="tab-box">
            <ul class="nav nav-tabs nav-fill com-marg" role="tablist">
                @foreach ($categories as $key=>$cat)
                @if($key == 0)
                 @php $href = "#all"; $class= "active";@endphp
                @endif
                @if($key == 1)
                 @php $href = "#active"; $class= "";@endphp
                @endif
                @if($key == 2)
                 @php $href = "#disable"; $class= "";@endphp
                @endif
                <li><a href="{{$href}}" onclick="searchByCat({{$cat->id}},'{{$cat->category}}')" class="{{$class}}">{{$cat->category}}</a></li>
                @endforeach
            </ul>
            <ul id="all" class="tabs-content-wrap res-table clearfix mrg-top10">
                <li>
                    <input placeholder="Enter a location, builder or Project" value="" type="text" class="search">
                    <div class="clearfix"></div>
                    <div class="city-link">
                         @foreach($locations as $loc) 
                            <a href="#" onclick="searchByLoc({{$loc->id}},'{{$loc->location}}')">{{$loc->location}}</a>
                         @endforeach
                        
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle city-link-dropdown-menu" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span> </button>
                            <ul class="dropdown-menu pull-right drop-menu" role="menu">
                                <li><a href="#">Hyderabad</a></li>
                                <li><a href="#">Bangalore</a></li>
                                <li><a href="#">Chennai</a></li>
                                <li><a href="#">Ahmedabad</a></li>
                                <li><a href="#">Jaipur</a></li>
                                <li><a href="#">Faridabad</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <ul id="active" class="tabs-content-wrap res-table clearfix mrg-top10">
                <li>
                   <input placeholder="Enter a location, builder or Project" value="" type="text" class="search">
                    <div class="clearfix"></div>
                    <div class="city-link">
                        <a href="#">Delhi</a>
                        <a href="#">Noida</a>
                        <a href="#">Gurgaon</a>
                        <a href="#">Ghaziabad</a>
                        <a href="#">Mumbai</a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle city-link-dropdown-menu" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span> </button>
                            <ul class="dropdown-menu pull-right drop-menu" role="menu">
                                <li><a href="#">Hyderabad</a></li>
                                <li><a href="#">Bangalore</a></li>
                                <li><a href="#">Chennai</a></li>
                                <li><a href="#">Ahmedabad</a></li>
                                <li><a href="#">Jaipur</a></li>
                                <li><a href="#">Faridabad</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <ul id="disable" class="tabs-content-wrap res-table clearfix mrg-top10">
                <li>
                    <input placeholder="Enter a location, builder or Project" value="" type="text" class="search">
                    <div class="clearfix"></div>
                    <div class="city-link">
                        <a href="#">Delhi</a>
                        <a href="#">Noida</a>
                        <a href="#">Gurgaon</a>
                        <a href="#">Ghaziabad</a>
                        <a href="#">Mumbai</a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle city-link-dropdown-menu" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span> </button>
                            <ul class="dropdown-menu pull-right drop-menu" role="menu">
                                <li><a href="#">Hyderabad</a></li>
                                <li><a href="#">Bangalore</a></li>
                                <li><a href="#">Chennai</a></li>
                                <li><a href="#">Ahmedabad</a></li>
                                <li><a href="#">Jaipur</a></li>
                                <li><a href="#">Faridabad</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div> 
      </div>
    </div>
  </div>
</section>
    <div class="clearfix"></div>
<section class="features">
  <div class="container">
    <div class="row">
      <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="homes"> <img src="{{asset('public/images/icon1.png')}}" alt=""/> <span>{{$worth_home}}<br>
          Worth homes sold</span> </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="homes"> <img src="{{asset('public/images/icon2.png')}}" alt=""/> <span>{{$happy_customer}}<br>
          Happy Customers</span> </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="homes"> <img src="{{asset('public/images/icon3.png')}}" alt=""/> <span>{{$relationship_manager}}<br>
          Relationship Managers</span> </div>
      </div>
      <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="homes"> <img src="{{asset('public/images/icon4.png')}}" alt=""/> <span>{{$active_listing}}<br>
          Active Listings</span> </div>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="rera_regis">
          <h2>RERA Registration</h2>
          <p>{{$rera_registration_text}}</p>
        </div>
      </div>
    </div>
    <div class="row" >
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="feat_pro" >
          <h2 id="location">Featured Projects</h2>
          <p>{{$featured_text}}</p> 
        <div id="demo">
              <div id="owl-demo" class="owl-carousel" >
              @foreach($projects as $project)
                       
                <div class="item">
                    <div class="iten-cont">
                    <img src="{{asset('public/upload/project_image/')}}/{{$project->project_image}}" alt=""/>
                    <div class="item-cont-top">
                    <div class="row">
                        <div class="col-md-7 col-sm-6 col-xs-12">
                        <h3 class="head-title">{{$project->name}}</h3>
                        <p><i class="fa-home fa"></i> 2BHK,3BHK,4BHK,5BHK</p>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                        <h4 class="price">3,30,0000.00 </h4>                         
                        </div>
                    </div>                          
                    </div>
                    <div class="item-cont-link"> <a href="#"> More Details</a></div>
                    <div class="item-cont-bottom">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-6 com-marg font10 no-rpad"><i class="fa fa-area-chart"></i> 4500 Sqft</div>
                        <div class="col-md-3 col-sm-6 col-xs-6 com-marg font10"><i class="fa-bed fa"></i> 2</div>
                        <div class="col-md-2 no-lpad col-sm-6 com-marg col-xs-6 font10"><i class="fa-bathtub fa"></i> 2</div>
                        <div class="col-md-3 col-sm-6 col-xs-6 com-marg font10"><i class="fa-car fa"></i> 1</div>
                    </div>  
                    </div>
                   
                    <div class="item-cont-overlay"></div>
                    </div> 
                </div>
                @endforeach 
            </div>
      </div>  
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home_cat">
  {!!$affHomeContent!!}
</section>
<section class="sch_tab">
  {!!$govtHousing!!}
</section>
<section class="top_build">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>Top Builders</h2>
        <p>{{$top_builder_text}}</p>
      </div>
    </div>
    <div class="row">
      <div class="tabs_wrapper">
        <div class="col-md-3 col-sm-12 col-lg-3">
          <ul class="tabs">
              @foreach($builders as $key=>$builder)
            @if($key==0)
            <li class="active" rel="tab{{$builder->id}}">{{$builder->name}}</li>
            @else
            <li  rel="tab{{$builder->id}}">{{$builder->name}}</li>
           @endif
            @endforeach
          </ul>
        </div>
         <div class="col-md-9 col-sm-12 col-lg-9">
          <div class="tab_container">
        @foreach($builders as $key=>$builder)
           @if($key==0)
            <h3 class="d_active tab_drawer_heading" rel="tab{{$builder->id}}">{{$builder->name}}</h3>
            @else
          <h3 class="tab_drawer_heading" rel="tab{{$builder->id}}">{{$builder->name}}</h3>
            @endif
            <div id="tab{{$builder->id}}" class="tab_content">
              <h4>{{$builder->name}}</h4>
               @foreach($builderProject as $projects)
              @if($builder->id==$projects->id)
       
        <div class="col-md-3 col-sm-6 col-lg-3">
                <div class="s1"><a href="">
                  <p>
                  @if($projects->project_image!='')
                  <img src="{{URL::to('')}}/public/upload/project_image/{{$projects->project_image}}" style="width:170px; height: 124px;" alt=""/>
                  @else
                  <img src="{{URL::to('')}}/public/upload/project_image/no-image.png" style="width:170px; height: 124px;" alt=""/>
                 
                  @endif

                    <span>{{$projects->name}}</span></p>
                  </a></div>
              </div>
              @endif
        @endforeach
       
              
            </div>
          
        @endforeach
        </div>
        </div>
        <!-- .tab_container --> 
      </div>
    </div>
  </div>
</section>
<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> <img src="{{asset('public/images/about.png')}}" class="about_img" alt=""/> </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> <span>About</span>
        <h2>Know About Delhi Smart Cities</h2>
        <p>We, the   DelhiSmartCities team, have extensively researched DDA land pooling Zone L/P/N/J for our customers and short-listed an array of projects with diverse residential projects offerings.DelhiSmartCities.com is the pioneer realty consultancy firm that markets properties proposed.</p>
        <p>We have extensively researched limitless opportunities available through this groundbreaking DDA Land Policy under Master Plan Delhi 2021. Our experts diligently assess customers’ requirements and short-list an array of projects out of diverse residential/commercial offerings. We have transformed the way a property research is done and take pride in providing seamless support service to our esteemed clients throughout their ownership journey.</p>
        <div class="read_more"><a href="">Read More <img src="{{asset('public/images/arrow.png')}}" alt=""/></a></div>
      </div>
    </div>
  </div>
</section>
<section class="value_add">
  <div class="val_add">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h2>Value Added Services</h2>
          <ul>
            <li class="v1"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m2"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m2"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m2"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m2"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m1"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
            <li class="v1 m2"><a href="">
              <p><img src="{{asset('public/images/1.png')}}" alt=""/></p>
              <span class="hidden-xs">Lorem Ipsum</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="nws_tab">
<div class="container">
<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
    <h3>Latest Real Estate News</h3>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img">
            <img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 com-marg">
    <h3>Trending News</h3>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
    <div class="lat_nws">
      <h5><a href="">Final hurdle on Dwarka e-way off, to be ready by </a></h5>
      <span>13 Jan 2018</span>
      <div class="nws_cont">
        <div class="nws_img"><img src="{{asset('public/images/news.jpg')}}" alt=""/></div>
        <p>New Delhi: The Northern Peripheral Road, the 24-km long 8-laned expressway awaiting completion since 2012, seems to be finally on the home stretch <a href="">Read More</a></p>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div class="t_soc"> <a class="twitter-timeline" href="https://twitter.com/dscdotcom?ref_src=twsrc%5Etfw">Tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
    </div>
    <div class="f_soc">
      <div class="fb-page" data-href="https://www.facebook.com/DelhiSmartCities" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/DelhiSmartCities" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/DelhiSmartCities">Facebook</a></blockquote>
      </div>
    </div>
    <div class="o_soc">
      <iframe src="https://www.youtube.com/embed/y27sdTc7Ruo?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
</div>
</section>
<section class="num_tab">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"> <span>WANT MORE INFORMATION ? FOR ANY EMERGENCY CONTACT US AT</span> <a href="">(+91) 8010-179-239</a> </div>
    </div>
  </div>
</section>
<footer>
  <div class="footer1">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Top Cities Project</h4>
          <ul>
          <li><a href="{{ URL::to('/new-delhi/') }}/">New Delhi</a></li>
          <li><a href="{{ URL::to('/noida/') }}/">Noida</a></li>
          <li><a href="{{ URL::to('/greater-noida/') }}/">Greater Noida</a></li>
          <li><a href="{{ URL::to('/gurugram/') }}/">Gurugram</a></li>
          <li><a href="{{ URL::to('/bangalore/') }}/">Bangalore</a></li>
          <li><a href="{{ URL::to('/pune/') }}/">Pune</a></li>
          <li><a href="{{ URL::to('/mumbai/') }}/">Mumbai</a></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Quick Links</h4>
          <ul>
        <li><a href="{{ URL::to('/') }}/">Home</a></li>
        <li><a href="{{ URL::to('/about-us/') }}/">About Us</a></li>
        <li><a href="https://www.inoutgreen.com/">In Out Green Interiors</a></li>
        <li><a href="{{ URL::to('/green-building-consultants/') }}/">Green Building Consultants</a></li>
        <li><a href="http://www.inoutgreenhomes.com/blogs/">Blogs</a></li>
        <li><a href="{{ URL::to('/contact-us/') }}/">Contact us</a></li>
        <li><a href="{{ URL::to('/we-are-hiring/') }}/">We Are Hiring</a></li>
    </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Recent Posts</h4>
          <p><img src="{{asset('public/images/news.jpg')}}" alt=""/> <a href="">New Development on Rera 2018</a></p>
          <p><img src="{{asset('public/images/news.jpg')}}" alt=""/> <a href="">New Development on Rera 2018</a></p>
          <p><img src="{{asset('public/images/news.jpg')}}" alt=""/> <a href="">New Development on Rera 2018</a></p>
          <p><img src="{{asset('public/images/news.jpg')}}" alt=""/> <a href="">New Development on Rera 2018</a></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ft">
          <h4>Contact Us</h4>
          <p><i class="fa fa-map-marker"></i> <span>Delhi Office:<br/>
            {{substr($address->office_address,0,32)}}<br>{{substr($address->office_address,33)}} </span> <span> Noida Office:<br/>
            {{substr($address->address,0,32)}}<br>{{substr($address->address,33)}}</span> </p>
          <p><i class="fa fa-phone"></i> <a href="">+{{$address->phone}}</a></p>
          <p><i class="fa fa-envelope"></i> <a href="">sales@delhismartcities.com</a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="footer2">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ft"> <span>Copyright &copy; 2018 Company Name. All Rights Reserved</span> </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ft">
          <ul>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
            <li><a href=""><i class="fa fa-skype"></i></a></li>
            <li><a href=""><i class="fa fa-youtube"></i></a></li>
            <li><a href=""><i class="fa fa-rss"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
    <script src="{{asset('public/js/jquery.min.js')}}"></script> 
<script src="{{asset('public/js/bootstrap.min.js')}}"></script> 
 <!-- Import Owl carosel js-->
     <script src="{{asset('public/js/owl.carousel.js')}}"></script>
<!-- logo slider--> 
<script>
$(document).ready(function() {
    $('#Carousel').carousel({
        interval: 5000
    })
});
</script> 
<!-- logo slider --> 
<!-- facebook --> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script> 
<!-- facebook --> 
<!-- builder --> 
<script>
 // tabbed content
    http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
        
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();        
        
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

      $(".tab_drawer_heading").removeClass("d_active");
      $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
      
    /*$(".tabs").css("margin-top", function(){ 
       return ($(".tab_container").outerHeight() - $(".tabs").outerHeight() ) / 2;
    });*/
    });
    $(".tab_container").css("min-height", function(){ 
      return $(".tabs").outerHeight() + 50;
    });
    /* if in drawer mode */
    $(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
      
      $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
      
      $("ul.tabs li").removeClass("active");
      $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
    
    
    /* Extra class "tab_last" 
       to add border to bottom side
       of last tab 
    $('ul.tabs li').last().addClass("tab_last");*/
    </script> 
        
        <script>
            function switch_tabs($obj){
            $('.tabs-content-wrap').hide();
            $('.nav-tabs a').removeClass('active');
            var id = $obj.attr('href');
            $(id).show().css('display', 'block');
            $obj.addClass('active');
            }
            $('.nav-tabs a').click(function(){
            switch_tabs($(this));
            return false;
            });
            switch_tabs($('.nav-tabs a.active'));
            </script>
         <!-- builder /-->
 

    <script>
    $(document).ready(function() {

      //Sort random function
      function random(owlSelector){
        owlSelector.children().sort(function(){
            return Math.round(Math.random()) - 0.5;
        }).each(function(){
          $(this).appendTo(owlSelector);
        });
      }

      $("#owl-demo").owlCarousel({
        navigation: true,
        pagination: false,
        navigationText: [
        "<i class='fa fa-chevron-left icon-white'></i>",
        "<i class='fa fa-chevron-right icon-white'></i>"
        ],
        //Call beforeInit callback, elem parameter point to $("#owl-demo")
        beforeInit : function(elem){
          random(elem);
        }

      });

    });
    </script>   
<script type="text/javascript">


function searchByLoc(id,lName){
     var imgId  = imgId;
     var Url = "{{url('/')}}/searchByLoc/"+id;
     $("#location").html(lName+ " Projects");
      $.ajax({
        url: Url,
        type: "get",
        dataType: "json",
        success: function(data){
               $("#owl-demo").html(data); 

        }
       });
      
}

function searchByCat(id,catName){
     var imgId  = imgId;
     var Url = "{{url('/')}}/searchByCat/"+id;
     $("#location").html(catName+ " Projects");
     
      $.ajax({
        url: Url,
        type: "get",
        dataType: "json",
        success: function(data){
          $("#owl-demo").html(data);
       }
      });     
  }

    </script>
</body>
</html>