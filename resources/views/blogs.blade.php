@include('layouts.master')
@include('layouts.header')
<section class="seacrh-content mar-top100 container-fluid">
  <div class="container project-content">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="row">
          
          <div class="divider"></div>
          
          <div class="clearfix">&nbsp;</div>
          @include('layouts.left-contact-form')
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-9">
        
      
     
        @if(!empty($posts))
        @foreach($posts as $post)
        <div class="project-cont">
          <div class="srpWrap">
           
                 
                <h4 class="pro-title"><img src="{{$post->img}}"/><a href="{{ URL::to('/blogs/') }}/{{$post->id}}/">{{$post->title}}</a><span class="fr font13">{{$post->created_at}}</h4>
                <p>{!!substr($post->description,0,100)!!}</p>
               
          </div>
        </div>
       
        @endforeach
        @endif
     
 
      </div>
    </div>
  </div>
  
  
<div class="clearfix com-marg"></div>               
</section>
 


@include('layouts.footer')
